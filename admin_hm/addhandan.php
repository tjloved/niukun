<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	发布喊单 - 直播管理中心
</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../../../assets/css/metro.css" rel="stylesheet" /><link href="../../../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../../../assets/css/style.css" rel="stylesheet" /><link href="../../../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../../../assets/css/style_default.css" rel="stylesheet" /><link href="../../../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../../../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../../../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../../../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../../../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../../../assets/uniform/css/uniform.default.css" />
<script src="../assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../js2/swfobject.min.js"></script>
<script type="text/javascript" src="../js2/web_socket.min.js"></script>
<script type="text/javascript" src="../js2/json2.min.js"></script>
<script type="text/javascript" src="../js/socket_config_xs.js"></script>
<script type="text/javascript" src="../js/socket_handan_xs.js"></script>
</head>
<!-- BEGIN BODY -->
<body class="fixed-top" onload="connect();">
	<!-- BEGIN HEADER -->
	 <?php include_once 'head.php'; ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	    <?php include_once 'left.php'; ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     喊单管理
                     <small>发布喊单</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">喊单管理</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">新增快讯</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
<!-- BEGIN VALIDATION STATES-->
                   <div class="portlet box grey">
                       <div class="portlet-title">
                           <h4><i class="icon-reorder"></i>填写资料</h4>
                           <div class="tools">
                               <a href="javascript:;" class="collapse"></a>
                               <a href="#portlet-config" data-toggle="modal" class="config"></a>
                               <a href="javascript:;" class="reload"></a>
                               <a href="javascript:;" class="remove"></a>
                           </div>
                       </div>
                       <div class="portlet-body form">
                           <!-- BEGIN FORM-->
  <?php
$handan=$res->fn_select("select t1.*,t2.flid from handanlist t1 left join pinpailist t2 on t1.shangpin=t2.pname where hid ='$_GET[hid]'");
?>

                           <form  id="form_sample_1" class="form-horizontal" method="post">
               
                               <div class="alert alert-error hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   填写信息有误，请检查修正后提交！
                               </div>
                               <div class="alert alert-success hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   信息填写正确!正在提交...
                               </div>

							   <div class="control-group">
                                   <label class="control-label">单 号:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="did" id="did" type="text" class="span6 m-wrap" placeholder="单号" value="<?=$handan[did]?$handan[did]:date('mdHis',time())?>"  required/>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">商 品:<span class="required">*</span></label>
                                   <div class="controls">
	<select class="span6 m-wrap" id="fenlei" name="fenlei" data-placeholder="请选择" tabindex="1" style="width:265px">
<option value="">选择商品类型...</option>
<?php  

$q_fenlei=$res->fn_sql("select * from fenleilist ");
while($fenlei=mysql_fetch_array($q_fenlei)){
?>        
<option value="<?=$fenlei[flid]?>" <?php if($handan[flid]==$fenlei[flid]){echo 'selected="selected"';}?>><?=$fenlei[name]?></option>
<?php } ?>									
	</select>
				
	<select class="span6 m-wrap" id="shangpin" name="shangpin" data-placeholder="请选择" tabindex="1" style="width:265px;margin-left: 10px;">
<option value="">选择商品...</option>
<?
$q_pinpai=$res->fn_sql("select * from pinpailist where flid = '$handan[flid]'");
while($pinpai=mysql_fetch_array($q_pinpai)){
?>        
<option value="<?=$pinpai[pname]?>" <?php if($handan[shangpin]==$pinpai[pname]){echo 'selected="selected"';}?>><?=$pinpai[pname]?></option>
<?php } ?>				
	</select>
									</div>                                   
                                       <img id="proLoading" style="margin:5px 0 0 180px; display:none;position:absolute;" src="../../../assets/img/loading.gif" />
                               </div>

						 <div class="control-group">
                                   <label class="control-label">类  型:<span class="required">*</span></label>
                                   <div class="controls">
	<select class="span6 m-wrap" id="leixing" name="leixing" data-placeholder="请选择" tabindex="1">
    <option value="买入" <?php if($handan[leixing]=='买入'){echo 'selected="selected"';}?>>买入</option>
    <option value="卖出" <?php if($handan[leixing]=='卖出'){echo 'selected="selected"';}?>>卖出</option>
	</select>
									</div>                                   
                                       <img id="proLoading" style="margin:5px 0 0 180px; display:none;position:absolute;" src="../../../assets/img/loading.gif" />
                               </div>

							<div class="control-group">
                                   <label class="control-label">仓位:<span class="required">*</span></label>
                                   <div class="controls">
<select name="cangwei" id="cangwei" type="text" class="span6 m-wrap" data-placeholder="请选择" tabindex="1" style="width:240px" >
	<option value="20%" <?php if($handan[cangwei]=='20%'){echo 'selected="selected"';}?>>20%</option>
	<option value="30%" <?php if($handan[cangwei]=='30%'){echo 'selected="selected"';}?>>30%</option>
	<option value="50%" <?php if($handan[cangwei]=='50%'){echo 'selected="selected"';}?>>50%</option>
	<option value="60%" <?php if($handan[cangwei]=='60%'){echo 'selected="selected"';}?>>60%</option>
	<option value="75%" <?php if($handan[cangwei]=='75%'){echo 'selected="selected"';}?>>75%</option>
</select>
	<!--<select class="span6 m-wrap" id="cwleixing" name="cwleixing" data-placeholder="请选择" tabindex="1" style="width:240px" >
	<option value="额定仓位"  <?php if($handan[cwleixing]=='额定仓位'){echo 'selected="selected"';}?>>额定仓位</option>
    <option value="减半仓位" <?php if($handan[cwleixing]=='减半仓位'){echo 'selected="selected"';}?>>减半仓位</option>
	</select>-->
									</div>                                   
                                       <img id="proLoading" style="margin:5px 0 0 180px; display:none;position:absolute;" src="../../../assets/img/loading.gif" />
                               </div>


                               <div class="control-group">
                                   <label class="control-label">开仓价:<span class="required">*</span></label>
                                   <div class="controls">
                                       <div class="input-prepend input-append">
                                           <span class="add-on" id="inbb">$</span><input class="m-wrap " id="kcjia" name="kcjia" type="text" value="<?=$handan[kcjia]?>" required onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')" onchange="if(!(/\d+(\.\d+)?$/.test(this.value))){alert('只能输入数字');this.value='';}"/><span class="add-on"><i id="inpricere" class="icon-refresh"></i></span>
                                       </div>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">止损价:<span class="required">*</span></label>
                                   <div class="controls">
                                       <div class="input-prepend input-append">
                                           <span class="add-on" id="stbb">$</span><input id="zsjia" name="zsjia" class="m-wrap " type="text" value="<?=$handan[zsjia]?>" required onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')" onchange="if(!(/\d+(\.\d+)?$/.test(this.value))){alert('只能输入数字');this.value='';}"/><span class="add-on"><i id="stpricere" class="icon-refresh"></i></span>
                                       </div>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">止盈价:<span class="required">*</span></label>
                                   <div class="controls">
                                       <div class="input-prepend input-append">
                                           <span class="add-on" id="tgbb">$</span><input id="zyjia" name="zyjia" class="m-wrap " type="text" value="<?=$handan[zyjia]?>" required onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')" onchange="if(!(/\d+(\.\d+)?$/.test(this.value))){alert('只能输入数字');this.value='';}"/><span class="add-on"><i id="tgpricere" class="icon-refresh"></i></span>
                                       </div>
                                   </div>
                               </div>
                         
                               <div class="control-group">

										<label class="control-label">分析师：<span class="required">*</span></label>

										<div class="controls">
<select class="span6 m-wrap" id="fenxishi" name="fenxishi" data-placeholder="请选择" tabindex="1">
<option value="">选择分析师...</option>
<?php  

$sql="select * from userlist where adminid =2 ";

if($u[quanxian]=='房间管理员'){
	$sql.=" and fid='$u[fid]'";
}


$q_fenxishi=$res->fn_sql($sql);


while($fenxishi=mysql_fetch_array($q_fenxishi)){
?>        
<option value="<?=$fenxishi[mid]?>" <?php if($handan[fenxishi]==$fenxishi[mid]){echo 'selected="selected"';}?>><?=$fenxishi[username]?></option>
<?php } ?>									
	</select>

										</div>

							</div>
                
                           <div class="control-group">

										<label class="control-label">房间：<span class="required">*</span></label>

										<div class="controls">
<select class="span6 m-wrap" id="fangjian" name="fangjian" data-placeholder="请选择" tabindex="1">
<option value="">选择房间...</option>
<?php  

$sql="select * from fangjianlist";

if($u[quanxian]=='房间管理员'){
	$sql.=" where fid='$u[fid]'";
}


$q_fangjian=$res->fn_sql($sql);


while($fangjian=mysql_fetch_array($q_fangjian)){
?>        
<option value="<?=$fangjian[fid]?>" <?php if($handan[fid]==$fangjian[fid]){echo 'selected="selected"';}?>><?=$fangjian[fname]?></option>
<?php } ?>									
	</select>

										</div>

							</div>
							
                               <div class="form-actions">
                                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <iframe src="" id="frameSend" style="display:none"></iframe>
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" class="btn green">OK</button>
                                       </div>
                                   </div>
                                   <button type="button" class="btn purple" onclick="form_submit()">确认发布</button>
                                   <button type="button" onclick="javascript:window.location.reload();" class="btn">重置</button>
                               </div>
                           </form>
                           <!-- END FORM-->
                       </div>
                   </div>
                  <!-- END VALIDATION STATES-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php include_once 'foot.php'; ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../../../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../../../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../../../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../../../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../../../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../../../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../../../assets/jquery-validation/dist/additional-methods.min.js"></script>
   <script src="../../../assets/js/app.js"></script>     

    <script src="../../../../Scripts/jquery.signalR-2.1.0.min.js"></script>
    <script src="../../../../signalr/hubs"></script>
	  <!--socoket start-->
<script type="text/javascript" src="../static/jquery.websocket.js"  media="all"></script>
   <script type="text/javascript" src="/static/websocket_config.js"  media="all"></script>

<!--socoket end-->
 <script>    
       function reloadThisPage() {
           window.location.reload();
       }
       jQuery(document).ready(function () {
           App.init();
		   
		   
		   $("#fenlei").change(function(){
			//   alert('ddd');
				var flid =$("#fenlei").val();
				 $.ajax({
					 url: "action.php?type=selectpinpai",  
					 type: "POST",
					 async: true,
					 data:{flid:flid},
					 dataType: "json",
					 error: function(){  
						// alert('Error loading XML document');  
					 },  
					 success: function(msg){//如果调用php成功  
						if(msg!=null){ 	
							$('#shangpin').find('option').remove();
							 for(i in msg){
								
								var str='<option value="'+msg[i].pname+'">'+msg[i].pname+'</option>';
								$('#shangpin').append(str);
							}
							
						}
					 }
				});  
		   });
       });
	   
	   function form_submit(){
		  var did=$("#did").val();
		  var fenlei=$("#fenlei").val();
		  var shangpin=$("#shangpin").val();
		  var leixing=$("#leixing").val();
		  var cangwei=$("#cangwei").val();
		  var kcjia=$("#kcjia").val();
		  var zsjia=$("#zsjia").val();
		  var zyjia=$("#zyjia").val();
		  var fenxishi=$("#fenxishi").val();
		  var fangjian=$("#fangjian").val();
	     $.ajax({
					 url: "action.php?type=addhandan&hid=<?=$_GET[hid]?>",  
					 type: "POST",
					 async: true,
					 data:{did:did,fenlei:fenlei,shangpin:shangpin,leixing:leixing,cangwei:cangwei,kcjia:kcjia,zsjia:zsjia,zyjia:zyjia,fenxishi:fenxishi,fangjian:fangjian},
					 error: function(){  
						// alert('Error loading XML document');  
					 },  
					 success: function(msg){//如果调用php成功  
						if(msg=='success'){ 	
							// $.wssend($.param( { sh_mid : 1,adminid:0,username:'交易提示',sh_content:'单号：'+did+' 价位：'+kcjia+' 详情点击查看',fid:fangjian } ) );
							$("#myModal2a").click();
                            $("#myModal2 p").html("操作成功！");
							//socket发送
							dataObj = {
								'type':'handan',
								'did': did,
								'kcjia': kcjia,
								'fenxishi':fenxishi,
								'rid': fangjian
							};
							ws.send(JSON.stringify(dataObj));
							
							setInterval(reloadThisPage, 3000);
						}else{
							alert(msg);
						}
					 }
				});  
	   
	   }
   </script>

   <!-- END JAVASCRIPTS -->   
</body>
</html>
