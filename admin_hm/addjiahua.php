<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	假话管理 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" />
<script src="../assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/swfobject/2.2/swfobject.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/web-socket-js/1.0.0/web_socket.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/json2/20150503/json2.min.js"></script>
<script type="text/javascript" src="../js/socket_config_xs.js"></script>
<script type="text/javascript" src="../js/socket_jiahua_xs.js"></script>
</head>
<!-- BEGIN BODY -->
<body class="fixed-top" onload="connect();">
	<!-- BEGIN HEADER -->
	  <?php include_once 'head.php'; ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	  <?php include_once 'left.php'; ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->
                  <h3 class="page-title">
                     假人管理
                     <small>假话列表</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">假人管理</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">假话列表</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
			    <div class="clearfix" style="margin-bottom:10px">
									<div class="btn-group pull-left">
                          			    <button class="btn  purple" id="fabuAll">全部发布</button>
									</div>
                                    <div class="btn-group pull-left">
                                   		 <button class="btn blue  " id="cancelAll">全部取消</button>
                                    </div>
						</div>
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>假人列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
                                      		  <th></th>
											<th>MID</th>
											<th class="hidden-480">姓名</th>
                                            <th class="hidden-480">用户组</th>
                                            <th class="hidden-480">房间</th>
										</tr>
									</thead>
									<tbody>
  <?php
if($u[fid]){
$sql_plus=" and t1.fid='$u[fid]'";
}

$sql="select t1.*,t2.adminname, t3.fname, t3.fid from userlist t1 left join admin_group t2 on t1.adminid = t2.adminid left join fangjianlist t3 on t1.fid=t3.fid where jiaren='2' $sql_plus order by fabu desc";
$q_users = $res->fn_sql($sql);
while($user = mysql_fetch_array($q_users)){

?>
	<tr class="odd gradeX" id="tr52">
	<td class="center" style="width:8px;"><input type="checkbox" data-fid="<?=$user[fid]?>" data-adminid="<?php echo $user['adminid']; ?>" data-uname="<?php echo $user['username']; ?>" class="group-checkable" name="mid"  value="<?=$user[mid]?>" <?php if($user[fabu]){?> checked="checked" <?php } ?> /></td>
	<td class="center hidden-480"><?=$user[mid]?></td>
	<td class="center hidden-480"><?=$user[username]?></td>
	<td class="center hidden-480"><?=$user[adminname]?></td>
	<td class="center hidden-480"><?=$user[fname]?></td>
             </tr>
  <?php } ?>
									</tbody>
								</table>
							</div>
						</div>
                        <!-- 假人列表 end-->
                         <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>假话列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
							 <div class="clearfix">
                            <form class="form-inline" >
									<div class="form-group">

									<label>请选择类型</label>
									<select id="leixing">
									<option value="">请选择类型</option>
									<option value="A类">A类</option>
									<option value="B类">B类</option>
									<option value="C类">C类</option>
									<option value="D类">D类</option>
									</select>
									</div>
									</form>
								</div>
														<table class="table table-striped table-hover table-bordered" id="sample_editable_2">
									<thead>

										<tr>
											<th></th>
											<th class="hidden-480">内容</th>
                                          <th class="hidden-480">类型</th>

										</tr>
									</thead>
									<tbody>
  <?php
$sql="select * from jiahualist ";
$q_jiahua = $res->fn_sql($sql);
while($jiahua = mysql_fetch_array($q_jiahua)){

?>
            <tr class="odd gradeX" id="tr52">
                                    <td class="center" style="width:8px;"><input type="checkbox" data-content="<?php echo $jiahua['content']; ?>" class="group-checkable" name="jhid" value="<?=$jiahua[jhid]?>" <?php if($jiahua[fabu]){?> checked="checked" <?php } ?>/></td>
                                <td class="center hidden-480"><?=$jiahua[content]?></td>
								<td class="center hidden-480"><?=$jiahua[jhtype]?></td>
             </tr>
  <?php } ?>
									</tbody>
								</table>
							</div>
						</div>
	<!--假话列表end-->
 <div class="portlet box light-grey">
		<div class="portlet-title">
			<h4><i class="icon-globe"></i>发话间隔</h4>
			<div class="tools">
				<a href="javascript:;" class="reload"></a>
			</div>
		</div>
		<div class="portlet-body">
		<?php $jianhua_jiange=$res->fn_select("select * from jiahua_jiange order by jgid desc limit 1");?>
							发话间隔：<select id="jiange" name="jiange">
<option value='1' <?php if($jianhua_jiange[value]==1){ ?> selected="selected" <?php }?> >1分</option>
<option value='2'<?php if($jianhua_jiange[value]==2){ ?> selected="selected" <?php }?> >2分</option>
<option value='3'<?php if($jianhua_jiange[value]==3){ ?> selected="selected" <?php }?> >3分</option>
<option value='4'<?php if($jianhua_jiange[value]==4){ ?> selected="selected" <?php }?> >4分</option>
<option value='5'<?php if($jianhua_jiange[value]==5){ ?> selected="selected" <?php }?> >5分</option>
<option value='6'<?php if($jianhua_jiange[value]==6){ ?> selected="selected" <?php }?> >6分</option>
<option value='7'<?php if($jianhua_jiange[value]==7){ ?> selected="selected" <?php }?> >7分</option>
<option value='8'<?php if($jianhua_jiange[value]==8){ ?> selected="selected" <?php }?> >8分</option>
<option value='9'<?php if($jianhua_jiange[value]==9){ ?> selected="selected" <?php }?> >9分</option>
<option value='10'<?php if($jianhua_jiange[value]==10){ ?> selected="selected" <?php }?> >10分</option>
</select>
							</div>
					</div>


          <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                   <input type="hidden" id="noHandanID" value=""/>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delHandan" class="btn red">确定</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>

                   <a href="#myModal3" role="button" id="myModal3a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel3">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" class="btn red">确定</button>
                                           <button data-dismiss="modal" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/breakpoints/breakpoints.js"></script>
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>
   <script>
       function reloadThisPage() {
           window.location.reload();
       }
       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("table_editable");
           App.init();
		   $('#sample_editable_2').DataTable( {
						 "aLengthMenu" : [10,20,50,100], //更改显示记录数选项
						 "iDisplayLength" : 10, //默认显示的记录数
			 });

			$("#fabuAll").click(function () {
					var mid=new Array();
					var jhid=new Array();
					var jiange=$("#jiange").val();
					$("input[type='checkbox'][name='mid']").each(function(){
						if($(this).attr('checked')){
							mid.push($(this).val());
						}
					})

					$("input[type='checkbox'][name='jhid']").each(function(){
						if($(this).attr('checked')){
							jhid.push($(this).val());
						}
					})
					if(mid.length==0 || jhid.length==0 ){
						  	$("#myModal2a").click();
                        	$("#myModal2 p").html("假人和假话不能为空!");
							return false;
					}

					var jhDataRow = [];
					var jhDataObj;
					$.each($('input[name=mid]:checked'), function (a, b) {
					 $.each($('input:checked[name=jhid]'), function (c, d) {
						 jhDataObj = {
							 'type': 'jhsay',
							 'rid': $(b).data('fid'),
							 'tid': 'all',
							 'aid': $(b).data('adminid'),
							 'umz': $(b).data('uname'),
							 'content': $(d).data('content')
						 }
						 jhDataRow.push(jhDataObj);
					 });
					});

					$.ajax({
             type: "post",
             url: "action.php?type=fabujiahua",
             data: {mid:mid,jhid:jhid,jiange:jiange},
             success: function (data) {
                 if (data == "success") {
                     $("#myModal2a").click();
                     $("#myModal2 p").html("提交成功!");
										 
										var jhTotal = jhDataRow.length - 1;
										 var setTime = setInterval(function () {
											if(jhTotal >=0){
												ws.send(JSON.stringify(jhDataRow[jhTotal]));
												jhTotal--;
											}else{
												clearTimeout(setTime);
											}
										}, parseInt(jiange) * 1000 * 60);
                 }
                 else {
                     $("#myModal2a").click();
              	   $("#myModal2 p").html("服务器内部错误!");
                 }
             },
             error: function (err) {
                 $("#myModal2a").click();
                 $("#myModal2 p").html("服务器内部错误!");
             }
         });
			});

			$("#cancelAll").click(function () {
					var mid=new Array();
					var jhid=new Array();
					$("input[type='checkbox'][name='mid']").each(function(){
						if($(this).attr('checked')){
							mid.push($(this).val());
						}
					})

					$("input[type='checkbox'][name='jhid']").each(function(){
						if($(this).attr('checked')){
							jhid.push($(this).val());
						}
					})
					if(mid.length==0 || jhid.length==0 ){
						  	$("#myModal2a").click();
                        	$("#myModal2 p").html("假人和假话不能为空!");
							return false;
					}

					$.ajax({
                       type: "post",
                       url: "action.php?type=canceljiahua",
                       data: {mid:mid,jhid:jhid},
                       success: function (data) {
                           if (data == "success") {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("提交成功!");
                               setInterval(reloadThisPage, 3000);
                           }
                           else {
                               $("#myModal2a").click();
                        	   $("#myModal2 p").html("服务器内部错误!");
                           }
                       },
                       error: function (err) {
                           $("#myModal2a").click();
                           $("#myModal2 p").html("服务器内部错误!");
                       }
                   });
			});

			$('#leixing').on( 'keyup click', function () {
				filterColumn();
			} );

       });
	   	function filterColumn () {
			var v=$("#leixing").val();
			$('#sample_editable_2').DataTable().column( 2 ).search(
				v,false,false
			).draw();
		}
   </script>
   <!-- END JAVASCRIPTS -->
</body>
</html>
