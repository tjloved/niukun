<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1"><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	新增直播室 - 直播管理中心
</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link href="../assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" rel="stylesheet" /><link href="../assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" /><link href="../assets/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet" />
    <script src="../ueditor/ueditor.config.js"></script>
    <script src="../ueditor/ueditor.all.min.js"></script>
</head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
     <?php 
	 include_once '../sys/conn.php'; 
	 include_once '../sys/mysql.class.php'; 
	 ?> 
    <!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
  <div class="row-fluid">
		<!-- BEGIN SIDEBAR -->
	 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content" style="margin-left: 0px; ">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     <?=$_GET[type]?>管理
                     <small>添加<?=$_GET[type]?></small>
                  </h3>
                  <ul class="breadcrumb">
                   
                     <li>
                        <a href="#"><?=$_GET[type]?>管理</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">添加<?=$_GET[type]?></a></li>
					  <li style="float:right;margin-right:20px"><a href="/news_list.php?typename=文件共享">返回列表>></a></li>
                  </ul>
				  
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
<!-- BEGIN VALIDATION STATES-->
                   <div class="portlet box grey">
                       <div class="portlet-title">
                           <h4><i class="icon-reorder"></i>表单</h4>
                           <div class="tools">
                               <a href="javascript:;" class="collapse"></a>
                               <a href="#portlet-config" data-toggle="modal" class="config"></a>
                               <a href="javascript:;" class="reload"></a>
                               <a href="javascript:;" class="remove"></a>
                           </div>
                       </div>
                       <div class="portlet-body form">
                           <!-- BEGIN FORM-->
    <?php
$_SESSION[litpic]='';
$news=$res->fn_select("select * from newslist where nid ='$_GET[nid]'");
$_SESSION[litpic]=$news[litpic];
?>
                           <form action="#" id="form_sample_1" class="form-horizontal">
                       <input type="hidden" name="typename" value="0"/>
                               <div class="alert alert-error hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   填写信息有误，请检查修正后提交！
                               </div>
                               <div class="alert alert-success hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   信息填写正确!正在提交...
                               </div>

                               <div class="alert alert-error hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   填写信息有误，请检查修正后提交！
                               </div>
                               <div class="alert alert-success hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   信息填写正确!正在提交...
                               </div>
                               <div class="control-group">
                                   <label class="control-label">标  题:<span class="required">*</span></label>
                                   <div class="controls">

										  <input name="title" id="title" type="text" class="span6 m-wrap"  value="<?=$news[title]?>"  />
                                     
									
									</div>                                   
                               </div>
                               
                               
                               <div class="control-group">
                                    <label class="control-label">URL/QQ：<span class="required">*</span></label>
                                    <div class="controls">
                                    <input name="description" id="description" type="text" class="span6 m-wrap"  value="<?=$news[description]?>"  />

                                    </div>
                                </div>
                                     
                               <div class="control-group">
                                   <label class="control-label">内  容:<span class="required">*</span></label>
                                   <div class="controls">
                                       <textarea id="newsinfo" style="width:708px;height:300px;" name="content"><?=$news[content]?></textarea>
                                   </div>
                               </div>
                 
                       <div class="control-group">
                                   <label class="control-label">房  间:<span class="required">*</span></label>
                                   <div class="controls">
                                   <select name="fangjian" id="fangjian">
<?php
$sqlxs='';
  if($u[fid]){
	   		$sqlxs.=" where fid='$u[fid]'";
	 }
$rs=$res->fn_sql("select * from fangjianlist $sqlxs");
while($fangjian=mysql_fetch_array($rs)){
?>
<option value="<?=$fangjian[fid]?>" <?php if($news[fid]==$fangjian[fid]){echo 'selected="selected"';} ?>><?=$fangjian[fname]?></option>
<?php } ?>
</select> 
                                   </div>
                               </div>
                                         
							   
							   
							   
							   
                               <div class="form-actions">
                                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <iframe src="" id="frameSend" style="display:none"></iframe>
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" class="btn green">OK</button>
                                       </div>
                                   </div>
                                   <button type="button" id="save" class="btn purple">保存</button>
                                   <button type="button" onclick="javascript:window.location.reload();" class="btn">重置</button>
                               </div>
                           </form>
                           <!-- END FORM-->
                       </div>
                   </div>
                  <!-- END VALIDATION STATES-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script src="../assets/jquery-tags-input/jquery.tagsinput.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <script src="../assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
   <script src="../assets/js/app.js"></script>     

   <script>
       var ue = UE.getEditor('newsinfo');
       function reloadThisPage() {
           window.location.reload();
       }

       function getUrlParam(name) {
           var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
           var r = window.location.search.substr(1).match(reg);  //匹配目标参数
           if (r != null) return unescape(r[2]); return null; //返回参数值
       }

       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("form_validation");
           App.init();

           function refrensepage() {
               window.location.reload();
           }


           $("#save").click(function () {
               var title = $('#title').val();
     		var description = $('#description').val();
			 var fangjian = $('#fangjian').val();
			  var position = $('#position').val();
               var content = ue.getContent();
			   var typename="<?=$_GET[type]?>";
 //      alert(title);   alert(description); alert(content);
          $.ajax({
                       url: "action.php?type=addnews&nid=<?=$_GET[nid]?>", type: "post",
                       data: {title:title,description:description,content:content,fangjian:fangjian,typename:typename,position:position},
                       success: function (data) {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("提交成功!");
                               setInterval(refrensepage, 3000);
                       },
                       error: function (err) {
                           $("#myModal2a").click();
                        $("#myModal2 p").html("服务器内部错误!");
                       }
                   });
   
           });
       });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>
