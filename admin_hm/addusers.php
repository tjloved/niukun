<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	新增会员 - 直播管理中心
</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../../../assets/css/metro.css" rel="stylesheet" /><link href="../../../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../../../assets/css/style.css" rel="stylesheet" /><link href="../../../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../../../assets/css/style_default.css" rel="stylesheet" /><link href="../../../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../../../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../../../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../../../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../../../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../../../assets/uniform/css/uniform.default.css" /><link href="../../../assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" rel="stylesheet" /><link href="../../../assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" /><link href="../../../assets/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
  <?php include_once 'head.php'; ?> 
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	  <?php include_once 'left.php'; ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     新增会员
                     <small>后台增加会员，如需帮助请点击--><a href="http://www.niukun.net/">南京牛昆喊单直播系统V2.3版</a></small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">会员管理</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">新增会员</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
<!-- BEGIN VALIDATION STATES-->
                   <div class="portlet box grey">
                       <div class="portlet-title">
                           <h4><i class="icon-reorder"></i>表单</h4>
                           <div class="tools">
                               <a href="javascript:;" class="collapse"></a>
                               <a href="#portlet-config" data-toggle="modal" class="config"></a>
                               <a href="javascript:;" class="reload"></a>
                               <a href="javascript:;" class="remove"></a>
                           </div>
                       </div>
                       <div class="portlet-body form">
                           <!-- BEGIN FORM-->
 <?php
$user=$res->fn_select("select * from userlist where mid ='$_GET[mid]'");
?>
                           <form action="action.php?type=adduser&mid=<?=$user[mid]?>#" id="form_sample_1" class="form-horizontal" method="post">
                               <div class="alert alert-error hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   填写信息有误，请检查修正后提交！
                               </div>
                               <div class="alert alert-success hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   信息填写正确!正在提交...
                               </div>
                               <div class="control-group">
                                   <label class="control-label">用户名:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="username" id="username" type="text" class="span6 m-wrap" placeholder="可由数字、字母、中文及下划线组成" value="<?=$user[username]?>"  <?=$_GET[mid]?'readonly':'';?> />
										</div>                                   
                                       <img id="proLoading" style="margin:5px 0 0 180px; display:none;position:absolute;" src="../../../assets/img/loading.gif" />
                               </div>
							  
                               <div class="control-group">
                                   <label class="control-label">密  码:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="password" id="password" type="password" class="span6 m-wrap" placeholder="密码长度6-16位" value="<?=$user[password]?>" onfocus="this.type='password'"/>
                                   </div>
                               </div>
                               <div class="control-group" >
                                   <label class="control-label">QQ:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="qq" id="QQ"  class="span6 m-wrap" placeholder="输入QQ号码" value="<?=$user[qq]?>"/>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">E-Mail:</label>
                                   <div class="controls">
                                       <div class="input-icon left">
												<i class="icon-envelope"></i>
												<input class="m-wrap " name="email" id="email" type="text" placeholder="如：lover@169money.com" value="<?=$user[email]?>"/>    
											</div>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">手机:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="telephone" id="tel"  class="span6 m-wrap" placeholder="输入手机号码" value="<?=$user[telephone]?>"/>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">用户组:</label>
                                   <div class="controls">
                                     <select name="adminid">
<?php
 $rs=$res->fn_sql("select * from admin_group where status='1' ");
	while($group=mysql_fetch_array($rs)){
?>
<option value="<?=$group[adminid]?>" <?php if($user[adminid]==$group[adminid])echo "selected='selected'";?>><?=$group[adminname]?></option>
<?php } ?>
</select>  
                                   </div>
                               </div>
                                <div class="control-group">
                                   <label class="control-label">上线:</label>
                                   <div class="controls">
                                     <select name="tuijianmid">
									 <option value="">请选择。。。</option>
<?php
 $rs=$res->fn_sql("select * from userlist where adminid<14 and jiaren=0");
	while($shangxian=mysql_fetch_array($rs)){
?>
<option value="<?=$shangxian[mid]?>" <?php if($user[tuijianmid]==$shangxian[mid])echo "selected='selected'";?>><?=$shangxian[username]?></option>
<?php } ?>
</select>  
                                   </div>
                               </div>
							   
                               <div class="control-group">
                                   <label class="control-label">房间：<span class="required">*</span></label>
                                   <div class="controls">
                                     
                                           <select name="fangjian">
<?php 
$sqlxs='';
  if($u[fid]){
	   		$sqlxs.=" where fid='$u[fid]'";
	 }
$rs=$res->fn_sql("select * from fangjianlist $sqlxs");
while($fangjian=mysql_fetch_array($rs)){
?>
<option value="<?=$fangjian[fid]?>" <?php if($user[fid]==$fangjian[fid])echo "selected='selected'";?>><?=$fangjian[fname]?></option>
<?php } ?>
</select>    
                                 
                                   </div>
                               </div>
                                          <div class="control-group">
                                   <label class="control-label">备注:</label>
                                   <div class="controls">
                                       <input name="beizhu" id="beizhu"  class="span6 m-wrap" placeholder="备注" value="<?=$user[beizhu]?>"/>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">推广链接：<span class="required">*</span></label>
                                   <div class="controls">
             http://<?php echo $_SERVER['HTTP_HOST'];?>/index.php?_=<?=$user[mid]?>
                                   </div>
                               </div>
                                
                               <div class="form-actions">
                                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <iframe src="" id="frameSend" style="display:none"></iframe>
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" class="btn green">OK</button>
                                       </div>
                                   </div>
                                   <button type="submit" id="save" class="btn purple">保存</button>
                                   <button type="reset" onclick="javascript:window.location.reload();" class="btn">重置</button>
                               </div>
                           </form>
                           <!-- END FORM-->
                       </div>
                   </div>
                  <!-- END VALIDATION STATES-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../../../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../../../assets/breakpoints/breakpoints.js"></script>      
   <script src="../../../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../../../assets/js/jquery.blockui.js"></script>
   <script src="../../../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../../../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../../../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../../../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script src="../../../assets/jquery-tags-input/jquery.tagsinput.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <script src="../../../assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../../../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../../../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../../../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../../../assets/jquery-validation/dist/additional-methods.min.js"></script>
   <script src="../../../assets/js/app.js"></script>     
<script>jQuery(document).ready(function () {
           App.init();
       });
</script>
</body>
</html>
