<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	管理员设置 - 直播管理中心
</title><meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	 <?php include_once 'head.php';?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	    <?php include_once 'left.php';?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->
                  <h3 class="page-title">
                     管理员设置
                     <small>新增管理员与管理员设置</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">系统设置</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">管理员设置</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet box grey">
                        <div class="portlet-title">
                            <h4><i class="icon-reorder"></i>填写资料</h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="#" id="form_sample_1" class="form-horizontal">
            <input type="hidden" id="aid"  value=""/>
                                <div class="alert alert-error hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    填写信息有误，请检查修正后提交！
                                </div>
                                <div class="alert alert-success hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    信息填写正确!正在提交...
                                </div>
                                <div class="control-group">
                                    <label class="control-label">用户名:<span class="required">*</span></label>
                                    <div class="controls">
                                        <input name="name" id="username" type="text" class="span6 m-wrap" placeholder="管理账号，数字、字母、中文及下划线" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">密  码:<span class="required">*</span></label>
                                    <div class="controls">
                                        <input name="password" id="password" type="password" class="span6 m-wrap" placeholder="6-16位密码" />
                                    </div>
                                </div>
       <div class="control-group">
                                    <label class="control-label">权限:<span class="required">*</span></label>
                                    <div class="controls">
                                     <div class="danger-toggle-button" id="dtb">
                                    <select  id="quanxian" name="quanxian" data-placeholder="请选择" tabindex="1" onChange="romeChange()">
<option value="超级管理员">选择权限...</option>
<option value="超级管理员" >超级管理员</option>
<option value="代理商" >代理商</option>
<option value="房间管理员" >房间管理员</option>
<option value="假话管理员" >假话管理员</option>	
</select>
 <script  type="text/javascript">
 	 var quanxian=document.getElementById('quanxian');
	  function romeChange(){
		   var v =quanxian.value;
			if(v=='房间管理员'||v=='代理商'){
			 var fangjian=document.getElementById('fangjian_choice');
			fangjian.style.display='block';
			 }
    }
	  </script>
</div>
                                    </div>
                                </div>
       <div class="control-group" id="fangjian_choice" style="display:none">
                                   <label class="control-label">房间：<span class="required">*</span></label>
                                   <div class="controls">
                                       <div class="danger-toggle-button" id="dtb">
                                           <select name="fangjian" id="fangjian">
                                           <option value="">请选择房间...</option>
<?php 
$rs=$res->fn_sql("select * from fangjianlist");
while($fangjian=mysql_fetch_array($rs)){
?>
<option value="<?=$fangjian[fid]?>"><?=$fangjian[fname]?></option>
<?php } ?>
</select>    
                                       </div>
                                   </div>
                               </div>
                                <div class="form-actions">
                                    <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                    <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                        <div class="modal-header">
                                            <iframe src="" id="frameSend" style="display: none"></iframe>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h3 id="myModalLabel2">系统提示</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p>Body goes here...</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn green">OK</button>
                                        </div>
                                    </div>
                                    <button type="button" id="save" class="btn purple">保存</button>
                                    <button type="button" id="edit" style="display:none" class="btn purple">保存</button>
                                    <button type="button" onclick="javascript:window.location.reload();" class="btn">重置</button>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>管理员列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>ID</th>
											<th class="hidden-480">账号</th>
											<th class="hidden-480">密码(MD5)</th>
											<th class="hidden-480">权限</th>          
                                            <th class="hidden-480">功能</th>
										</tr>
									</thead>
									<tbody>
<?php 
$sqlxs='';
  if($u[fid]){
	   		$sqlxs.=" where t1.fid='$u[fid]'";
	 }
$q_admin=$res->fn_sql("select t1.* ,t2.fname from adminlist t1 left join fangjianlist t2 on t1.fid=t2.fid $sqlxs");
while($admin=mysql_fetch_array($q_admin)){
?> 
                                                <tr class="odd gradeX" id="tr19">
                                                    <td class="center hidden-480"><?=$admin[aid]?></td>
                                                    <td class="center hidden-480"><?=$admin[username]?></td>
                                                    <td class="center hidden-480"><?=md5($admin[password])?></td>
                                                    <td class="center hidden-480"><?=$admin[quanxian]?>
	                                                    <font color="#FF0000"><?=$admin[quanxian]=='房间管理员'||$admin[quanxian]=='代理商'?'('.$admin[fname].')':''?></font>
                                                    </td>
                                                    <td class="center hidden-480">
                                                        <div class="btn-group">
                                                            <button class="btn red dropdown-toggle" data-toggle="dropdown" style="margin-bottom: 0px;">操作<i class="icon-angle-down"></i></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="editManager('<?=$admin[aid]?>','<?=$admin[username]?>','<?=$admin[password]?>','<?=$admin[quanxian]?>','<?=$admin[fid]?>')">编辑</a></li>
                                                                <li><a href="#" onclick="delManager('<?=$admin[aid]?>','<?=$admin[username]?>')">删除</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
      </tr>
     <?php }?> 
									</tbody>
								</table>
							</div>
						</div>
                   <a href="#myModal3" role="button" id="myModal3a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                        <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h3 id="myModalLabel3">系统提示</h3>
                            </div>
                            <div class="modal-body">
                                <p>Body goes here...</p>
                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" id="delManager" class="btn red">删除</button>
                                <button data-dismiss="modal" id="close" class="btn green">取消</button>
                            </div>
                        </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	 <?php include_once 'foot.php';?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>     
   <script>
       function reloadThisPage()
       {
           location.reload();
       }
       function delManager(id, name) {
           $("#delManager").show();
           $("#close").html("取消");
           $("#aid").val(id);
           $("#myModal3a").click();
           $("#myModal3 p").html("是否删除管理员：" + name + "?");
       }
       function editManager(aid,username,password,quanxian,fid)
       {
		
           $("#aid").val(aid);
           $("#username").val(username);
		    $("#quanxian").val(quanxian);
           $("#password").val(password);
		   $("#fangjian").val(fid);
		   if(quanxian=='房间管理员'){
			   $('#fangjian_choice').show();
			  }
       }
       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("table_editable");
           App.init();
           $("#save").click(function () {
			  
			   var aid = $("#aid").val();
               var username = $("#username").val();
               var password = $("#password").val();
			   var fangjian = $("#fangjian").val();
			   var quanxian=$("#quanxian").val();
               if(username != "" && password != "")
               {
                   $.ajax({
                       type: "post",
                       url: "action.php?type=addadmin",
                       data: {aid:aid,username:username,password:password,fangjian:fangjian,quanxian:quanxian},
                     
                       success: function (data) {
                           if (data=="success") {
                               location.reload();
                           }
                           else {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("操作失败！");
                           }
                       },
                       error: function (err) {
                           $("#myModal2a").click();
                           $("#myModal2 p").html("服务器内部错误！");
                       }
                   });
               }
           });
           $("#edit").click(function () {
			  
               var username = $("#username").val();
               var password = $("#password").val();
               if (username != "" && password != "") {
                   $.ajax({
                       type: "post",
                       url: "Default.aspx/updateManager",
                       data: "{'password':'" + password + "','id':'" + $("#noManagerID").val() + "'}",
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       success: function (data) {
                           if (data.d == "1") {
                               //成功
                               $("#myModal2a").click();
                               $("#myModal2 p").html("密码修改成功！");
                               setInterval(reloadThisPage,2000);
                           }
                           else {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("操作失败！");
                           }
                       },
                       error: function (err) {
                           $("#myModal2a").click();
                           $("#myModal2 p").html("服务器内部错误！");
                       }
                   });
               }
           });
           $("#delManager").click(function () {
               var id = $("#aid").val();
               $.ajax({
                   type: "get",
                   url: "/sys/delete.php",
                   data: {table:'adminlist',field:'aid',id:id},
                 
                   beforeSend: function () {
                       $(".modal-backdrop").fadeOut();
                   },
                   success: function (data) {
               
                           $("#delManager").hide();
                           $("#close").html("确定");
                           $("#tr" + id).hide();
						     setInterval(reloadThisPage,2000);
        
                   },
                   error: function (err) { }
               });
           });
       });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>
