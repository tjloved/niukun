<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1"><title>
	房间管理 - 直播管理中心
</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../../../assets/css/metro.css" rel="stylesheet" /><link href="../../../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../../../assets/css/style.css" rel="stylesheet" /><link href="../../../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../../../assets/css/style_default.css" rel="stylesheet" /><link href="../../../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../../../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../../../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../../../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../../../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../../../assets/uniform/css/uniform.default.css" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	 <?php include_once 'head.php'; ?> 
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	     <?php include_once 'left.php'; ?> 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     房间管理
                     <small>直播室列表，可编辑</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">直播室</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">直播室列表</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>直播室列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">工具 <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">打印</a></li>
											
											<li><a href="#">导出Excel</a></li>
										</ul>
									</div>
								</div>
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th>房间号</th>
											<th class="hidden-480">房间名称</th>
												<th class="hidden-480">密码</th>
											<th class="hidden-480">链接</th>
										
                                            <th class="hidden-480">操作</th>
										</tr>
									</thead>
									<tbody>
   
<?php

$sql="select * from fangjianlist";

if($u[fid]){
	$sql.=" where fid = $u[fid]";
}
$q_fangjian = $res->fn_sql($sql);
while($fangjian = mysql_fetch_array($q_fangjian)){

?>
         <tr class="odd gradeX" id='tr1'>
                     <td><?=$fangjian[fid]?></td>
                                                    <td class="center hidden-480"><?=$fangjian[fname]?></td>
													  <td class="center hidden-480"><?=$fangjian[password]?></td>
                                                    <td class="center hidden-480">http://<?php  echo $_SERVER['HTTP_HOST']."/index.php?fid=".$fangjian[fid]; ?><?=$fangjian[password]?'&pwd='.$fangjian[password]:''?></td>
                                                    <td class="center hidden-480"><a href="addfangjian.php?id=<?=$fangjian[id]?>">编辑</a>/<a href="/sys/delete.php?table=fangjianlist&field=fid&id=<?=$fangjian[fid]?>&url=/admin_hn/am_fangjian.php">删除</a></td>
                                                </tr>
                                            
    <?php } ?>								
									</tbody>
								</table>
							</div>
						</div>
                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                   <input type="hidden" id="noHandanID" value=""/>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delHandan" class="btn red">删除</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../../../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../../../assets/breakpoints/breakpoints.js"></script>      
   <script src="../../../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../../../assets/js/jquery.blockui.js"></script>
   <script src="../../../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../../../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../../../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../../../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../../../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../../../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../../../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../../../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../../../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../../../assets/js/app.js"></script>     
   <script>

       function deletem(id) {
           if (confirm("确定删除此条私信？")) {
               $.ajax({
                   type: "post",
                   url: "Default.aspx/Delsixin",
                   data: "{'id':'" + id + "'}",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   beforeSend: function () {
                       $(".modal-backdrop").fadeOut();
                   },
                   success: function (data) {
                       if (data.d == "1") {
                           location.reload();
                       }
                       else {
                       }
                   },
                   error: function (err) { }
               });
           }
       }
       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("table_editable");

           App.init();
       });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>
