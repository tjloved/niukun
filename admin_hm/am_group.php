<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	群组设置 - 直播管理中心
</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link href="../assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" rel="stylesheet" />
<link href="../assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
      <?php include_once 'head.php'; ?>
    <!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
  <div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	    <?php include_once 'left.php'; ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     系统设置
                    <small>用户组设置</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">系统设置</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">用户组设置</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet box grey">
                        <div class="portlet-title">
                            <h4><i class="icon-reorder"></i>填写资料</h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
      <form action="#"  id="form_sample_1" class="form-horizontal" enctype="multipart/form-data" method="post">
                                <div class="alert alert-error hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    填写信息有误，请检查修正后提交！
                                </div>
                                <div class="alert alert-success hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    信息填写正确!正在提交...
                                </div>
                                <div class="control-group">
                                    <label class="control-label">用户名:<span class="required">*</span></label>
                                    <div class="controls">
                                        <input name="name" id="adminname" type="text" class="span6 m-wrap" placeholder="请填写用户组名称" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">勋章:<span class="required">*</span></label>
                                    <div class="controls">
           <img id='xunzhang' src="" alt="" style="display:none"/>                            
     
<input name="upfile" type="file">
<input type="submit" name="submit" value="提交" />

                             <!--图片上传-->
         <!--   <div class="fileupload fileupload-new" data-provides="fileupload">

                                           <div class="fileupload-new thumbnail" style="width: 40px; height: 40px;">

      
                                               <img id='xunzhang' src="" alt="" style="display:none"/>

                                           </div>

                                           <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 250px; max-height: 40px; line-height: 20px;"></div>

                                           <div>

                                               <span class="btn btn-file"><span class="fileupload-new">选择文件</span>

                                                   <span class="fileupload-exists">重选</span>

                                                   <input type="file" class="default" id="fileupload"  name="files"/></span>

                                               <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">移除</a>

                                           </div>

                                       </div>-->
              <!--/.end图片上传-->                         
                                       
                                    </div>
                                </div>
           <div class="control-group">
                                    <label class="control-label">是否发言:<span class="required">*</span></label>
                                    <div class="controls">
                                    
            <div class="danger-toggle-button">
       <input type="checkbox" id="fayan" class="toggle" />
       </div>
                                    </div>
                                </div>
                        <div class="control-group">
                                    <label class="control-label">是否显示:<span class="required">*</span></label>
                                    <div class="controls">
                                    
            <div class="danger-toggle-button">
       <input type="checkbox" id="zhuangtai" class="toggle" />
       </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                    <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                        <div class="modal-header">
                                            <iframe src="" id="frameSend" style="display: none"></iframe>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h3 id="myModalLabel2">系统提示</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p>Body goes here...</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn green">OK</button>
                                        </div>
                                    </div>
                                    <button type="button" id="save" class="btn purple">保存</button>
                                    <button type="button" id="edit" style="display:none" class="btn purple">保存</button>
                                    <button type="button" onclick="javascript:window.location.reload();" class="btn">重置</button>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>管理员列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">工具 <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">打印</a></li>
											
											<li><a href="#">导出Excel</a></li>
										</ul>
									</div>
								</div>
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>	
									    <th>ID</th>
											<th class="hidden-480">用户组名称</th>
											<th class="hidden-480">勋章图标</th>
                                            					<th class="hidden-480">是否显示</th>
                                                                <th class="hidden-480">是否发言</th>
                                            <th class="hidden-480">功能</th>
										</tr>
									</thead>
									<tbody>
<?php
$q_group=$res->fn_sql("select * from (select * from admin_group  ) t order by status desc,adminid asc");
while($group=mysql_fetch_array($q_group)){
	++$key;
?>
                                      <tr class="odd gradeX" id="tr20">
                                                    
                            <td class="center hidden-480"><?=$key?></td>
                                                    <td class="center hidden-480"><?=$group[adminname]?></td>
                                                    <td class="center hidden-480"><img src="/images/Level/User<?=$group[adminid]?>.png" width="40px" height="40px"/>
</td>

  <td class="center hidden-480">
  <?php if($group[status]){ ?>
  <font >打开</font>
  <?php } else {?>
    <font color="#FF0000">关闭</font>
  <?php } ?>
</td>
  
  <td class="center hidden-480">
  <?php if($group[fayan]){ ?>
  <font >可以发言</font>
  <?php } else {?>
    <font color="#FF0000">禁止发言</font>
  <?php } ?>
</td>
                                               <td class="center hidden-480">
                                                        <div class="btn-group">
                                                            <button class="btn red dropdown-toggle" data-toggle="dropdown" style="margin-bottom: 0px;">操作<i class="icon-angle-down"></i></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="editManager('<?=$group[adminid]?>','<?=$group[adminname]?>','<?=$group[status]?>','<?=$group[fayan]?>')">编辑</a></li>
                                      </ul>
                                                        </div>
                                                    </td>
                                                </tr>
     <?php } ?>                                                                         	
                                        							
								  </tbody>
								</table>
							</div>
						</div>
                   <a href="#myModal3" role="button" id="myModal3a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                   <input type="hidden" id="noManagerID"  value=""/>
                                   <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel3">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delManager" class="btn red">删除</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php include_once 'foot.php'; ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
     <script src="../assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>     
   <script>

       function reloadThisPage()
       {
           location.reload();
       }

       function delManager(id, name) {
           $("#delManager").show();
           $("#close").html("取消");
           $("#noManagerID").val(id);
           $("#myModal3a").click();
           $("#myModal3 p").html("是否删除管理员：" + name + "?");
       }

       function editManager(adminid,adminname,status,fayan)
       {    $("#form_sample_1").attr('action','/sys/xunzhang.php?gid='+adminid);
           $("#noManagerID").val(adminid);
           $("#adminname").val(adminname);
        //   $("#adminname").attr("readonly","true");
           $("#xunzhang").attr('src','../images/Level/User'+adminid+'.png');
		   $("#xunzhang").show();
		   if(status==1){
			   	$("#zhuangtai").removeAttr("checked");
			  $("#zhuangtai").click();  
		$("#zhuangtai").attr("checked","checked");
			
			}else{
			 $("#zhuangtai").attr("checked","checked");
			 $("#zhuangtai").click();
			 	$("#zhuangtai").removeAttr("checked");
			}
			
			  if(fayan==1){
			   	$("#fayan").removeAttr("checked");
			  $("#fayan").click();  
		$("#fayan").attr("checked","checked");
			
			}else{
			 $("#fayan").attr("checked","checked");
			 $("#fayan").click();
			 	$("#fayan").removeAttr("checked");
			}

       }

       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("table_editable");
           App.init();

          $("#save").click(function () {
		
              var adminname = $("#adminname").val();
              var adminid = $("#noManagerID").val();
			 var status,fayan;
			     if ($("#zhuangtai").attr("checked") == "checked") {
                   status = "1";//是
               }
               else {
                   status = "0";
               }
			   
			   if ($("#fayan").attr("checked") == "checked") {
                   fayan = "1";//是
               }
               else {
                   fayan = "0";
               }

               if(adminname != "" && adminid != "")
               {
                   $.ajax({
                       type: "post",
                       url: "action.php?type=addgroup",
                       data:{adminname:adminname,adminid:adminid,status:status,fayan:fayan},
                     success: function (data) {
                           if (data == "success") {
                               //成功
                               location.reload();
                           }
                           else {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("操作失败！");
                           }
                       },
                       error: function (err) {
                           $("#myModal2a").click();
                           $("#myModal2 p").html("服务器内部错误！");
                       }
                   });
               }
           });

         
 
		  
    //初始化，主要是设置上传参数，以及事件处理方法(回调函数)
   /* $('#fileupload').fileupload({
        autoUpload: true,//是否自动上传
        url: "/images/Level/",//上传地址
        dataType: 'json',
        done: function (e, data) {//设置文件上传完毕事件的回调函数
            $.each(data.result.files, function (index, file) {//
                $('<p/>').text(file.name).appendTo('#files');
            });
        },
        progressall: function (e, data) {//设置上传进度事件的回调函数
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    }); */
		   
       });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>

