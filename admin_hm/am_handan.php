<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	喊单管理 - 直播管理中心
</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" />
<script src="../assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/swfobject/2.2/swfobject.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/web-socket-js/1.0.0/web_socket.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/json2/20150503/json2.min.js"></script>
<script type="text/javascript" src="../js/socket_config_xs.js"></script>
<script type="text/javascript" src="../js/socket_pingcang_xs.js"></script>
</head>
<!-- BEGIN BODY -->
<body class="fixed-top" onload="connect();">
	<!-- BEGIN HEADER -->
	    <?php include_once 'head.php';?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	    <?php include_once 'left.php';?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     喊单管理
                     <small>喊单历史记录列表</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">喊单管理</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">新增快讯</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>历史喊单列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">工具 <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">打印</a></li>
											
											<li><a href="#">导出Excel</a></li>
										</ul>
									</div>
								</div>
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											
											<th class="hidden-480">排序</th>
											<th class="hidden-480">单号</th>
											<th class="hidden-480">开仓时间</th>
											<th class="hidden-480">类型</th>
                                            <th class="hidden-480">仓位</th>
                                            <th class="hidden-480">商品</th>
                                            <th class="hidden-480">开仓价</th>
                                            <th class="hidden-480">止损价</th>
                                            <th class="hidden-480">止盈价</th>
                                            <th class="hidden-480">平仓时间</th>
											<th class="hidden-480">平仓价</th>
											<th class="hidden-480">获利点数</th>
											<th class="hidden-480">备注</th>
											<th class="hidden-480">分析师</th> 
											<th class="hidden-480">房间</th> 
											<th class="hidden-480">状态</th> 
                                            <th class="hidden-480">功能</th>
										</tr>
									</thead>
									<tbody>
       <?php
$sql_plus='';
if($u[quanxian]=='房间管理员'){
	$sql_plus=" where t1.fid='$u[fid]'";
}
  
$q_handan =$res->fn_sql("select t1.*,t2.fname,t3.username  from handanlist t1 left join fangjianlist t2 on t1.fid=t2.fid left join userlist t3 on t1.fenxishi=t3.mid order by hid desc");
while($handan =mysql_fetch_array($q_handan)){
	++$key;
?>          
                                                <tr class="odd gradeX" id="tr160">
                                                <td class="center hidden-480"><?=$key?></td>
                                                    <td class="center hidden-480"><?=$handan[did]?></td>
                                                    <td class="center hidden-480"><?=date('m-d H:i',$handan[kctime])?></td>
													    <td class="center hidden-480"><?=$handan[leixing]?></td>
                                                    <td class="center hidden-480"><?=$handan[cangwei]?></td>
                                                
                                                    <td class="center hidden-480"><?=$handan[shangpin]?></td>
                                                    <td class="center hidden-480"><?=$handan[kcjia]?></td>
                                                    <td class="center hidden-480"><?=$handan[zsjia]?></td>
                                                    <td class="center hidden-480"><?=$handan[zyjia]?></td>
													  <td class="center hidden-480"><?=date('m-d H:i',$handan[pctime])?></td>
													<td class="center hidden-480"><?=$handan[pcjia]?></td>
													<td class="center hidden-480"><?=$handan[hldian]?></td>
													<td class="center hidden-480"><?=$handan[beizhu]?></td>
													<td class="center hidden-480"><?=$handan[username]?></td>
													<td class="center hidden-480"><?=$handan[fname]?></td>
                                                    <td class="center hidden-480" id="st_160" style="cursor: pointer;"><span class="label label-success"><?=$handan[status]?'<font color="red">已平仓</font>':'未平仓'?></span></td>
                                       
                                                    <td class="center hidden-480">
                                                        <div class="btn-group">
                                                            <button class="btn red dropdown-toggle" data-toggle="dropdown" style="margin-bottom: 0px;">操作<i class="icon-angle-down"></i></button>
                                                            <ul class="dropdown-menu">
                                                                
 <li><a href="javascript:void(0)" onclick="editHandan('<?=$handan[did]?>','<?=$handan[fid]?>')">平仓</a></li> 
  <li><a href="addhandan.php?hid=<?=$handan[hid]?>">修改</a></li> 
   <li><a href="/sys/delete.php?table=handanlist&field=hid&id=<?=$handan[hid]?>&url=/admin_hn/am_handan.php">删除</a></li> 
           </ul>
       </div>
           </td>
        </tr>
  <?php
}
?>							
									</tbody>
								</table>
							</div>
						</div>
                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                   <input type="hidden" id="did" value=""/>
				   <input type="hidden" id="fid" value=""/>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delHandan" class="btn red">删除</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>

                   <a href="#myModal3" role="button" id="myModal3a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel3">填写平仓价</h3>
                                       </div>
                                       <div class="modal-body">
                                           <div class="control-group">
                                               <label class="control-label">平仓价:<span class="required">*</span></label>
                                               <div class="controls">
                                                   <div class="input-prepend input-append">
                                                       <span class="add-on" id="tgbb">$</span><input id="pcjia" name="pcjia" class="m-wrap " type="text" value="" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')" onchange="if(!(/\d+(\.\d+)?$/.test(this.value))){alert('只能输入数字');this.value='';}"/><span class="add-on"><i id="tgpricere" class="icon-refresh"></i></span>
                                                   </div>
                                               </div>
                                           </div>
										    <div class="control-group">
                                               <label class="control-label">获利点:<span class="required">*</span></label>
                                               <div class="controls">
                                                   <div class="input-prepend input-append">
                                                       <span class="add-on" id="tgbb">$</span><input id="huolidian" name="huolidian" class="m-wrap " type="text" value="" onkeyup="if(isNaN(value))execCommand('undo')" onafterpaste="if(isNaN(value))execCommand('undo')" onchange="if(!(/\d+(\.\d+)?$/.test(this.value))){alert('只能输入数字');this.value='';}"/><span class="add-on"><i id="tgpricere" class="icon-refresh"></i></span>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="hd_close" class="btn red">平仓</button>
                                           <button data-dismiss="modal" id="hd_c_close" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php include_once 'foot.php';?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>    
  
   <script>

       function delHandan(id,number)
       {
           $("#delHandan").show();
           $("#close").html("取消");
           $("#noHandanID").val(id);
           $("#myModal2a").click();
           $("#myModal2 p").html("是否删除编号为：" + number + "的操作建议?");
       }
       
	   function editHandan(did,fid)
       {

           $("#did").val(did);
		   $("#fid").val(fid);
           $("#myModal3a").click();
        
       }

       function Close(id)
       {
           $("#hd_close").show();
           $("#noHandanID").val(id);
           $("#myModal3a").click();
           $("#myModal3 p").html("请填写当前的平仓价格");
       }

       

       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("table_editable");
           App.init();


           $("#delHandan").click(function () {
               var id = $("did").val();
               $.ajax({
                   type: "post",
                   url: "Default.aspx/DelHandan",
                   data: "{'id':'" + id + "'}",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   beforeSend:function(){
                       $(".modal-backdrop").fadeOut();
                   },
                   success: function (data) {
                       if (data.d == "1")
                       {
					  
                           //成功
                           //$("#myModal2a").click();
                           //$("#myModal2 p").html("操作成功！");
                           $("#delHandan").hide();
                           $("#close").html("确定");
                           $("#tr" + id).hide();
                       }
                       else {

                       }
                   },
                   error: function (err) { }
               });
           });

           $("#hd_close").click(function () {
               var did = $("#did").val();
			    var fid = $("#fid").val();
               var pcjia = $("#pcjia").val();
			   var huolidian = $("#huolidian").val();
               $.ajax({
                   type: "post",
                   url: "action.php?type=pingcang",
                   data: {did:did,fid:fid,pcjia:pcjia,huolidian:huolidian},    
                   success: function (data) {
                       if (data == "success") {
						 //$.wssend($.param( { sh_mid : 1,adminid:0,username:'交易提示',sh_content:'单号：'+did+' 已平仓',fid:fid } ) );
                           //成功
                           //$("#myModal2a").click();
                           //$("#myModal2 p").html("操作成功！");
                           $("#hd_close").hide();
                           $("#close").html("确定");
                           //$("#tr" + id).hide();
                           ///$("#st_" + id).html("<span class=\"label label-success\">已平仓</span> ");
						   //socket发送
							dataObj = {
								'type': 'pingcang',
								'did': did,
								'rid': fid
							};
							ws.send(JSON.stringify(dataObj));
							
							location.reload();
                       }
                       else {

                       }
                   },
                   error: function (err) { }
               });
           });
       });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>
