<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	机器人列表 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<?php include_once 'head.php'; ?> 
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	    	  <?php include_once 'left.php'; ?> 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     假人管理
                     <small>假人列表</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">假人管理</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">假人列表</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>机器人列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
                                   <div class="control-group pull-left" >
 									<a href="addjiaren.php" class="btn purple">添加假人</a>
									<form method="post" action="am_jiaren_exls.php" enctype="multipart/form-data">
                                    <h3>导入Excel表：</h3><input  type="file" name="file_stu" /> <input type="submit"  value="导入" />
                                    </form>
									<a href="upFile/jiarenshili.xls" class="btn purple"><span style="color:red;font-size:18px;">下载批量导入示例.xls</a></span><br>
										</div>
								</div>
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>ID</th>
											<th class="hidden-480">用户名</th>
       						                <th class="hidden-480">房间</th>                                
											<th class="hidden-480">密码</th>
                                            <th class="hidden-480">等级</th>
                                            <th class="hidden-480">操作</th>
										</tr>
									</thead>
									<tbody>
                                        
<?php  $sqlxs=''; 
   if($u[fid]){
	   		$sqlxs.=" and t1.fid='$u[fid]'";
	 }
$sql="select * from userlist t1 left join admin_group t2 on t1.adminid=t2.adminid left join fangjianlist t3 on t1.fid=t3.fid  where ( jiaren='2'or jiaren='1' ) $sqlxs order by jiaren desc,t1.adminid asc";
$q_users = $res->fn_sql($sql);
while($user = mysql_fetch_array($q_users)){
?>                              
<tr class="odd gradeX" id="tr2743">
	<td>
	<input type="checkbox" class="checkboxes" value="<?=$user[mid]?>" username="<?=$user[username]?>" adminid="<?=$user[adminid]?>" flowers="<?=$user[flowers]?>" jiaren="<?=$user[jiaren]?>" fid="<?=$user[fid]?>" /></td>
	<td class="center hidden-480"><?=$user[mid]?></td>
	<td class="center hidden-480"><?=$user[username]?><?php if($user[jiaren]=='2'){?> <font color="#FF0000">(已上线)</font><?php }else{?><font color="#ccc">(已下线)</font><?php } ?></td>
<td class="center hidden-480"><?=$user[fname]?></td><td class="center hidden-480"><?=$user[password]?></td>
                                                    <td class="center hidden-480"><?=$user[adminname]?></td>
                                                    <td class="center hidden-480">
                                                        <div class="btn-group">
                                                            <button class="btn red dropdown-toggle" data-toggle="dropdown" style="margin-bottom: 0px;">操作<i class="icon-angle-down"></i></button> 
                                                <ul class="dropdown-menu">
                                                            <li><a href="addjiaren.php?mid=<?=$user[mid]?>">资料修改</a></li>
                                                                 <li><a href="/sys/delete.php?table=userlist&field=mid&id=<?=$user[mid]?>&url=/admin_hn/am_jiaren.php">删除</a></li>
                                                      </ul>
                                                        </div>
                                                         <a href="addjiahua.php?mid=<?=$user[mid]?>">假话管理</a> 
                                                    </td>
                                                </tr>
         <?php } ?>							
									</tbody>
								</table>
							</div>
							<div>
							<input type="button" value="全选" onclick="quanxuan()" style=""/>
							<input type="button" value="批量上线" onclick="piliangshangxian()" style=""/>
							<input type="button" value="批量下线" onclick="piliangxiaxian()" style=""/>
							<input type="button" name="piliangshanchu" value="批量删除" style=" float:right;width:96px; height:24px" onclick="piliangshanchu()"/>
							</div>
						</div>
                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                    <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                         <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                         <h3 id="myModalLabel2">系统提示</h3>
                         </div>
                             <div class="modal-body">
                                <p>操作成功！Loading...</p>
                             </div>
                             <div class="modal-footer">
                                 <button data-dismiss="modal" class="btn green">OK</button>
                             </div>
                     </div>   
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
         </div>
		</div>
	</div>
	<?php include_once 'foot.php' ?>
   <script src="../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>  
    <script src="../Scripts/jquery.signalR-2.1.0.min.js"></script>
    <script src="../signalr/hubs"></script>
   <script>
           function reloadThisPage() {
           window.location.reload();
       }
       jQuery(document).ready(function () {
           App.setPage("table_editable");
           App.init();
       });
	  function quanxuan(){
				$("tbody .checkboxes").each(function(){
				  if($(this).attr('checked')){
					 $(this).removeAttr('checked');
					 $(this).parent().removeClass("checked");
				 }else{
				 	 $(this).attr('checked','checked');
					 $(this).parent().addClass("checked");
				 }
			});
	   }
	   function piliangshangxian(){
			var mid=new Array();
			var jiaren=new Array()
			$("input[class='checkboxes']").each(function(){
			     if($(this).parent().attr('class')=='checked'){
					mid.push($(this).val());
					jiaren.push({mid:$(this).val(),username:$(this).attr('username'),adminid:$(this).attr('adminid'),flowers:$(this).attr('flowers'),jiaren:$(this).attr('jiaren'),fid:$(this).attr('fid')});
				 }
			});
			
                   $.ajax({
                       type: "post",
                       url: "action.php?type=jiarenpiliang",
                       data: {mid:mid,piliangshangxian:1},
                       success: function (data) {
                           if (data= "success") {
							$.wssend($.param( {jiaren:jiaren,piliangshangxian:1}) ); 
												 
                               $("#myModal2a").click();
                               $("#myModal2 p").html("提交成功!");
                               setInterval(reloadThisPage, 3000);
                           }
                           else {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("提交失败");
                           }
                       },
                       error: function (err) {
                           $("#myModal2a").click();
                           $("#myModal2 p").html("服务器内部错误!");
                       }
                   });
	   }
	      function piliangxiaxian(){
			var mid=new Array();
			var jiaren=new Array()
			$("input[class='checkboxes']").each(function(){
			     if($(this).parent().attr('class')=='checked'){
					mid.push($(this).val());
					jiaren.push({mid:$(this).val(),username:$(this).attr('username'),adminid:$(this).attr('adminid'),flowers:$(this).attr('flowers'),jiaren:$(this).attr('jiaren'),fid:$(this).attr('fid')});
				 }
			});
                $.ajax({
                       type: "post",
                       url: "action.php?type=jiarenpiliang",
                       data: {mid:mid,piliangxiaxian:1},
                       success: function (data) {
                           if (data= "success") {
                           $.wssend($.param( {jiaren:jiaren,piliangxiaxian:1}) );
                               $("#myModal2a").click();
                               $("#myModal2 p").html("提交成功!");
                               setInterval(reloadThisPage, 3000);
                           }
                           else {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("提交失败");
                           }
                       },
                       error: function (err) {
                           $("#myModal2a").click();
                           $("#myModal2 p").html("服务器内部错误!");
                       }
                   });
	   }
	    function piliangshanchu(){
			var mid=new Array();
			$("input[class='checkboxes']").each(function(){
			     if($(this).parent().attr('class')=='checked'){
					mid.push($(this).val());
				 }
			});
			
                   $.ajax({
                       type: "post",
                       url: "action.php?type=userpiliang",
                       data: {mid:mid,piliangshanchu:1},
                       success: function (data) {
                           if (data= "success") {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("提交成功!");
                               setInterval(reloadThisPage, 3000);
                           }
                           else {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("提交失败");
                           }
                       },
                       error: function (err) {
                           $("#myModal2a").click();
                           $("#myModal2 p").html("服务器内部错误!");
                       }
                   });
    	
	   }
   </script>
   <!-- END JAVASCRIPTS -->  
  <script type="text/javascript">
$('.group-checkable').click(function(){
$('tbody .checkboxes').click();
});
  </script>   
</body>
</html>
