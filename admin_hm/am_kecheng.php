<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	课程管理 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../../../assets/css/metro.css" rel="stylesheet" /><link href="../../../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../../../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../../../assets/css/style.css" rel="stylesheet" /><link href="../../../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../../../assets/css/style_default.css" rel="stylesheet" /><link href="../../../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../../../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../../../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../../../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../../../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../../../assets/uniform/css/uniform.default.css" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
  <?php include_once 'head.php'; ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	  <?php include_once 'left.php'; ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     课程管理
                     <small>普通课程表</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">课程管理</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">普通课程表</a>
                        
                     </li>
                    
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN VALIDATION STATES-->

                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
						  <div style="margin:20px;background-color: rgb(56, 52, 54);
width: 300px;
height: 46px;
line-height: 50px;">
            
     <font  style="color:#F00;"> 当前房间：</font>
	 
	 <select name="fangjian" id="fangjian" onChange="romeChange()">
      <?php 
	
	      $sqlxs='';
   if($u[fid]){
	   		$sqlxs.=" where fid='$u[fid]'";
	 }  
	  $q_fangjian=$res->fn_sql("select * from fangjianlist $sqlxs");
	 while($fangjian=mysql_fetch_array($q_fangjian)){
	  ?>
      <option value="<?=$fangjian[fid]?>" <?php if($_GET[fid]==$fangjian[fid]) echo 'selected="selected"'; ?>><?=$fangjian[fname]?></option>
      <?php }?>
      </select>
      <script  type="text/javascript">
 	 var fangjian=document.getElementById('fangjian');
	 var fid =fangjian.value;

	  function romeChange(){
		   var fid =fangjian.value;
		  window.location='am_kecheng.php?fid='+fid;
    }
	  </script>
      
      </div>
	    <?php
	if($_GET[fid]){
		$fid=$_GET[fid];
	}else{
		$q_f=$res->fn_select("select * from fangjianlist $sqlxs");
		$fid=$q_f[fid];
	}
	?>
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>普通课程表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<form action="action.php?type=addkecheng&fid=<?=$fid?>"  method="post">
								<div class="clearfix">
									<div class="btn-group pull-right">
									<!--	<button class="btn dropdown-toggle" data-toggle="dropdown">工具 <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">打印</a></li>
											
											<li><a href="#">导出Excel</a></li>
										</ul>-->
									</div>
								</div>
								<style>
								 table select{ width:124px}
								</style>
							
								<table class="table table-striped table-hover table-bordered" id="">
									<thead>
										<tr>
										
											<th>ID</th>
											<th class="hidden-480">时间</th>
											<th class="hidden-480">周一</th>
         									<th class="hidden-480">周二</th>                     
                                            <th class="hidden-480">周三</th>
											<th class="hidden-480">周四</th>
         									<th class="hidden-480">周五</th>                     
                                            <th class="hidden-480">周六</th>
											 <th class="hidden-480">操作</th>
										</tr>
									</thead>
									<tbody id="tb_kecheng">
<?php 
$jiaoshis=$res->fn_rows("select * from jiaoshilist where fid='$fid'");

$kechengs=$res->fn_rows("select * from kechenglist where fid='$fid' order by orderid asc");

if($kechengs){
foreach($kechengs as $key=> $kecheng ){

?>
			<tr >
				<td><?=($key+1)?></td>
				<td>
				<input type="text" name="starttime[]" style="width:80px" value="<?=$kecheng[starttime]?>"/>~<input type="text" name="endtime[]" style="width:80px" value="<?=$kecheng[endtime]?>"/>
				</td>
				<td>
				<select name="zhouyi[]">
				<?php
				foreach($jiaoshis as $jiaoshi){
				?>
				<option value="<?=$jiaoshi[jname]?>" <?php if($jiaoshi[jname]==$kecheng[zhouyi])echo 'selected="selected"'; ?>><?=$jiaoshi[jname]?></option>
				<?php } ?>
				</select>
				</td>
				<td>
					<select name="zhouer[]">
						<?php
						foreach($jiaoshis as $jiaoshi){
						?>
						<option value="<?=$jiaoshi[jname]?>" <?php if($jiaoshi[jname]==$kecheng[zhouer])echo 'selected="selected"'; ?>><?=$jiaoshi[jname]?></option>
						<?php } ?>

					</select>
				</td>
				<td>
					<select name="zhousan[]">
						<?php
						foreach($jiaoshis as $jiaoshi){
						?>
						<option value="<?=$jiaoshi[jname]?>" <?php if($jiaoshi[jname]==$kecheng[zhousan])echo 'selected="selected"'; ?>><?=$jiaoshi[jname]?></option>
						<?php } ?>
					</select>
				</td> 
                
				<td>
					<select name="zhousi[]">
						<?php
						foreach($jiaoshis as $jiaoshi){
						?>
						<option value="<?=$jiaoshi[jname]?>" <?php if($jiaoshi[jname]==$kecheng[zhousi])echo 'selected="selected"'; ?>><?=$jiaoshi[jname]?></option>
						<?php } ?>
					</select>
				</td> 				   
				
				<td>
					<select name="zhouwu[]">
						<?php
						foreach($jiaoshis as $jiaoshi){
						?>
						<option value="<?=$jiaoshi[jname]?>" <?php if($jiaoshi[jname]==$kecheng[zhouwu])echo 'selected="selected"'; ?>><?=$jiaoshi[jname]?></option>
						<?php } ?>
					</select>
				</td> 

				<td>
					<select name="zhouliu[]">
						<?php
						foreach($jiaoshis as $jiaoshi){
						?>
						<option value="<?=$jiaoshi[jname]?>" <?php if($jiaoshi[jname]==$zhouliu[zhouwu])echo 'selected="selected"'; ?>><?=$jiaoshi[jname]?></option>
						<?php } ?>
					</select>
				</td> 
				  <td>
					<div class="btn-group">
						<button class="btn red dropdown-toggle" data-toggle="dropdown" style="margin-bottom: 0px;">操作<i class="icon-angle-down"></i></button>
						<ul class="dropdown-menu">
							<li><a href="#" onclick="addRow()">添加</a></li>
							<li>
							<a href="/sys/delete.php?table=kechenglist&field=kid&id=<?=$kecheng[kid]?>&url=am_kecheng.php">删除</a>                                                   
							
							</li>
						</ul>
					</div>
				</td>
</tr>
<?php } }else{?>      
<tr id="norow"> <td colspan="9"> <input type="button" name="btn" value="添加" onclick="addRow('norow')"/></td></tr>
<?php } ?>

                                        							
									</tbody>
								</table>

							<input type="submit" name="submit" value="确认提交"/>
							</div>
								</form>
						</div>
                   <a href="#myModal3" role="button" id="myModal3a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                   <input type="hidden" id="noManagerID"  value=""/>
                                   <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel3">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delManager" class="btn red">删除</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../../../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../../../assets/breakpoints/breakpoints.js"></script>      
   <script src="../../../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../../../assets/js/jquery.blockui.js"></script>
   <script src="../../../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../../../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../../../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../../../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../../../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../../../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../../../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../../../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../../../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../../../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../../../assets/js/app.js"></script>     
   <script>

       function reloadThisPage() {
           location.reload();
       }


       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("table_editable");
           App.init();
        
       });

		var jiaoshi='<?php $str=''; foreach($jiaoshis as $jiaoshi){ $str.="<option value=\"$jiaoshi[jname]\">$jiaoshi[jname]</option>";} echo $str;?>';

    function addRow(flag){
var tr_count=$('table tbody tr').length;
	var str='<tr ><td>'+(tr_count+1)+'</td><td ><input type="text" name="starttime[]" style="width:80px"/>~<input type="text" name="endtime[]" style="width:80px"/></td><td ><select name="zhouyi[]">'+jiaoshi+'</select></td><td ><select name="zhouer[]">'+jiaoshi+'</select></td><td ><select name="zhousan[]">'+jiaoshi+'</select></td><td ><select name="zhousi[]">'+jiaoshi+'</select></td><td ><select name="zhouwu[]">'+jiaoshi+'</select></td><td ><select name="zhouliu[]">'+jiaoshi+'</select></td><td ><div class="btn-group"><button class="btn red dropdown-toggle" data-toggle="dropdown" style="margin-bottom: 0px;">操作<i class="icon-angle-down"></i></button><ul class="dropdown-menu"><li><a href="#" onclick="addRow()">添加</a></li><li><a href="/sys/delete.php?table=kechenglist&field=kid&id=&url=am_kecheng.php">删除</a>  </li></ul></div></tr>';
	if(flag=="norow"){
		$('#norow').remove();
	}
	$('#tb_kecheng').append(str);
	
	}
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>
