<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	留言管理 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap_3.0.min.css" rel="stylesheet"><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" media="all" href="../assets/bootstrap-daterangepicker/daterangepicker-bs3.css"/>

</head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<?php include_once 'head.php'; ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	  <?php include_once 'left.php'; ?> 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     聊天记录管理
                     	<small>直播系统聚合统计面板</small>
                  </h3>
                    <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">聊天记录管理</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>聊天记录</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body" style="padding:10px 10px 30px">

					<form id="form_sample_1" class="form-horizontal" action="" method='get'>		
									<div class="control-group pullleft">

										发送人:</span>
										<input type="text" name="username"  class="span1" value="<?=$_GET['username']?>"/>
										<span>接收人:</span>
										<input type="text" name="tousername"  class="span1" value="<?=$_GET['tousername']?>"/>
										<span>内容:</span>
										<input type="text" name="content"   class="span3" value="<?=$_GET['content']?>"/><br/><br/>
										<span>发送时间</span>
										<input type="text"  name="reservation" id="reservationtime" class=" span4" placeholder="请选择时间....." value="<?=$_GET['reservation']?>"/>
										<span>房间:</span>
									    <select name="fangjian" id="fangjian">
											<?php if(!$u[fid]){	?>
								<option value="">全部</option>
									<?php }?>
										<?php
											if($u[fid]){
											$sqlxs.=" where fid='$u[fid]'";
											}
										$rs=$res->fn_sql("select * from fangjianlist $sqlxs");
										while($fangjian=mysql_fetch_array($rs)){
										?>
										<option value="<?=$fangjian[fid]?>" <?php if($_GET[fangjian]==$fangjian[fid]){echo 'selected="selected"';} ?>><?=$fangjian[fname]?></option>
										<?php } ?>
										</select> 
										 <input type="submit" id="save" onClick="$('#form_sample_1').attr('action','')" class="btn purple" value="搜索"/>
										
										<input type="submit" id="daochu" onClick="$('#form_sample_1').attr('action','daochu-liaotian.php')" class="btn purple" value="导出"/>

									</div>
							</form>	
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th style="width:8px;"></th>
											<th>ID</th>
											 <th class="hidden-480">发送人</th>
											  <th class="hidden-480">发送时间</th>
											 <th class="hidden-480">接收人</th>
											<th class="hidden-480">房间</th>
											<th class="hidden-480">内容</th>
										</tr>
									</thead>
									<tbody id="liuyanlist">
     <?php 
	$sqlxs="";
	if($u[fid]){
		$sqlxs.=" and t1.fid='$u[fid]'";
	}
	if($_GET[username]){
		$sqlxs.=" and t1.username='$_GET[username]'";
	}
	if($_GET[tousername]){
		$sqlxs.=" and t1.tousername='$_GET[tousername]'";
	}
	if($_GET[content]){
		
		$sqlxs.=" and t1.content  like '%$_GET[content]%'";
	}
	if($_GET['reservation']){
		$reservation=explode("-",$_GET['reservation']);
		$start_time=strtotime($reservation[0]);
		$end_time=strtotime($reservation[1]);
		$sqlxs.=" and shtime>'$start_time' and shtime<'$end_time'";
	}
	if($_GET[fangjian]){
		
		$sqlxs.=" and t1.fid ='$_GET[fangjian]'";
	}
	   $rs=$res->fn_rows("select t1.*,t2.fname from liaotianlist t1 left join fangjianlist t2 on t1.fid=t2.fid where 1=1  $sqlxs   order by t1.lid desc limit 300");
	if($_GET[minganlid]){
		$sqlxs.=" and ( t1.mid='$_GET[minganmid]' or t1.tomid='$_GET[minganmid]') and ( t1.mid='$_GET[mingantomid]' or t1.tomid='$_GET[mingantomid]')";
		 $rs1=$res->fn_rows("select t1.*,t2.fname from liaotianlist t1 left join fangjianlist t2 on t1.fid=t2.fid where 1=1  $sqlxs and t1.lid<=$_GET[minganlid]  order by t1.lid desc limit 5");
		 $rs2=$res->fn_rows("select * from (select t1.*,t2.fname from liaotianlist t1 left join fangjianlist t2 on t1.fid=t2.fid where 1=1  $sqlxs and t1.lid>$_GET[minganlid]  order by t1.lid asc limit 5) t order by t.lid desc ");
		$rs=array_merge($rs2, $rs1);
	}
  foreach($rs as $liaotian){
	 ?>                                   
             <tr class="odd gradeX" id="tr52">
                           <td>
                                 <input type="checkbox" class="checkboxes" name="liaotian" value="<?=$liaotian[lid]?>" /></td>
                                                    <td class="center hidden-480"><?=$liaotian[lid]?></td>
                                                    <td class="center hidden-480"><?=$liaotian[username]?></td>
													<td class="center hidden-480"><?=date('Y-m-d H:i:s',$liaotian[shtime])?></td>
                                                    <td class="center hidden-480"><?=$liaotian[tousername]?$liaotian[tousername]:'全部'?></td>
                                                 	<td class="center hidden-480"><?=$liaotian[fname]?></td>
                                                    <td class="center hidden-480"><?=$liaotian[content]?></td>
                                                   <!-- <td class="center hidden-480" ><input  type="button" value="删除"   lid="<?=$liaotian[lid]?>" onclick="delliaotian(this)"/></td>-->
      </tr>
        <?php } ?> 								
									</tbody>
								</table>
      <div>             <input type="button" onclick="selectall()" name="quanxuan" value="全选" style="width:80px; height:24px; float:left; margin-right:5px"/>
<input type="button" onclick="delall()" name="delall" value="批量删除" style="width:80px; height:24px; float:right; margin-right:30px"/>
</div>
							</div>
						</div>
                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delHandan" class="btn red">确定</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>

                   <a href="#myModal3" role="button" id="myModal3a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel3">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" class="btn red">确定</button>
                                           <button data-dismiss="modal" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/moment.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>     
   <script>
function selectall(){
	$("input[name='liaotian']").each(function(){
			  if($(this).attr('checked')){
					 $(this).removeAttr('checked');
					 $(this).parent().removeClass("checked");
				 }else{
				 	 $(this).attr('checked','checked');
					 $(this).parent().addClass("checked");
				 }
	});
}
       jQuery(document).ready(function () {
          App.setPage("table_editable");
           App.init();
			$('#reservationtime').daterangepicker({
					timePicker: true,
					timePicker12Hour : false, //是否使用12小时制来显示时间  
					
					format: 'YYYY/MM/DD HH:mm',
					 ranges : {  
						//'最近1小时': [moment().subtract('hours',1), moment()],  
						'今日': [moment().startOf('day'), moment()],  
						'昨日': [moment().subtract('days', 1).startOf('day'), moment().subtract('days', 1).endOf('day')],  
						'最近7日': [moment().subtract('days', 6).startOf('day'), moment()],  
					},
					locale : {  
						applyLabel : '确定',  
						cancelLabel : '取消',  
						fromLabel : '起始时间',  
						toLabel : '结束时间',  
						customRangeLabel : '自定义',  
						daysOfWeek : [ '日', '一', '二', '三', '四', '五', '六' ],  
						monthNames : [ '一月', '二月', '三月', '四月', '五月', '六月',  
								'七月', '八月', '九月', '十月', '十一月', '十二月' ],  
						firstDay : 1  
					}  
				},
				function(start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
				});
       });
function delall(){
	var liaotians=new Array();
	$("input[name='liaotian']").each(function(){
		if($(this).attr("checked")){
			  liaotians.push($(this).val());
			   $(this).parent().parent().remove(); 
		}
   
	});
	 $.ajax({
         url: "/admin_hm/action.php?type=delall",
         type: "post",
         data:{lids:liaotians},
         error: function(){ 
         },  
         success: function(data,status){//如果调用php成功  
			if(data=="true"){ 	
				alert("操作成功");
			}else{
			}
         }
     });
}
</script>
</body>
</html>
