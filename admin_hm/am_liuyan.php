<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	留言管理 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" />
<script src="../assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/swfobject/2.2/swfobject.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/web-socket-js/1.0.0/web_socket.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/json2/20150503/json2.min.js"></script>
<script type="text/javascript" src="../js/socket_config_xs.js"></script>
<script type="text/javascript" src="../js/socket_liuyan_xs.js"></script>
</head>
<!-- BEGIN BODY -->
<body class="fixed-top"  onload="connect();">
	<!-- BEGIN HEADER -->
	<?php include_once 'head.php'; ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	  <?php include_once 'left.php'; ?> 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     留言管理
                     <small>留言管理实际为聊天内容审核，当房间基础设置中关闭自动审核后，需要在本页面人工审核。<a href="http://www.niukun.net/">南京牛昆喊单直播系统V2.3版</a></small>
                  </h3>
                    <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">留言管理</a></li>
                    </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>留言管理</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body" style="padding:10px 10px 30px">

					<form id="form_sample_1" class="form-horizontal" action="" method='get'>		
									<div class="control-group pullleft">

										<span>发送人:</span>
										<input type="text" name="username"  class="span1" value="<?=$_GET['username']?>"/>
										<span>接收人:</span>
										<input type="text" name="tousername"  class="span1" value="<?=$_GET['tousername']?>"/>
										<span>内容:</span>
										<input type="text" name="content"   class="span3" value="<?=$_GET['content']?>"/><br/><br/>
										<span>发送时间</span>
										<input type="text"  name="reservation" id="reservationtime" class=" span4" placeholder="请选择时间....." value="<?=$_GET['reservation']?>"/>
										<span>房间:</span>
									    <select name="fangjian" id="fangjian">
											<?php if(!$u[fid]){	?>
								<option value="">全部</option>
									<?php }?>
										<?php
											if($u[fid]){
											$sqlxs.=" where fid='$u[fid]'";
											}
										$rs=$res->fn_sql("select * from fangjianlist $sqlxs");
										while($fangjian=mysql_fetch_array($rs)){
										?>
										<option value="<?=$fangjian[fid]?>" <?php if($_GET[fangjian]==$fangjian[fid]){echo 'selected="selected"';} ?>><?=$fangjian[fname]?></option>
										<?php } ?>
										</select> 
										 <input type="submit" id="save" onClick="$('#form_sample_1').attr('action','')" class="btn purple" value="搜索"/>
										<input type="submit" id="daochu" onClick="$('#form_sample_1').attr('action','daochu-liaotian.php')" class="btn purple" value="导出"/>
									</div>
							</form>	
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th style="width:8px;"></th>
											<th>ID</th>
											<th class="hidden-480">发送人</th>
											<th class="hidden-480">接收人</th>
											<th class="hidden-480">房间</th>
											<th class="hidden-480">内容</th>
                                            <th class="hidden-480">操作</th>
										</tr>
									</thead>
									<tbody id="liuyanlist">
    
             <tr class="odd gradeX" id="tr52">
                 <td></td>
                        <td class="center hidden-480"></td>            
                        <td class="center hidden-480"></td>
                        <td class="center hidden-480"></td>
                        <td class="center hidden-480"></td>
                        <td class="center hidden-480"></td>
                        <td class="center hidden-480"></td>
                                                  
      </tr>
							
									</tbody>
								</table>
      <div>             <input type="button" onclick="selectall()" name="quanxuan" value="全选" style="width:80px; height:24px; float:left; margin-right:5px"/>
<input type="button" onclick=#" name="delall" value="批量删除" style="width:80px; height:24px; float:right; margin-right:30px"/>
<input type="button" onclick="#" name="fabuall" value="批量审核" style="width:80px; height:24px; float:right; margin-right:30px"/>
</div>
							</div>
						</div>
                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delHandan" class="btn red">确定</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>

                   <a href="#myModal3" role="button" id="myModal3a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel3">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" class="btn red">确定</button>
                                           <button data-dismiss="modal" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER --><?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
  	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>   
   <script>
/*function selectall(){
	$("input[name='liaotian']").each(function(){
			  if($(this).attr('checked')){
					 $(this).removeAttr('checked');
					 $(this).parent().removeClass("checked");
				 }else{
				 	 $(this).attr('checked','checked');
					 $(this).parent().addClass("checked");
				 }
	});
}
   </script>
   <!-- END JAVASCRIPTS -->  
   <script>
/*var wait=10;
function   formatDate(now)   { 
          var   year=now.getYear();   
          var   month=now.getMonth()+1;   
          var   date=now.getDate();   
          var   hour=now.getHours();   
          var   minute=now.getMinutes();   
          var   second=now.getSeconds();
          return   hour+":"+minute;   
}

function addli2(){
	var v=$("tbody[id='liuyanlist'] tr:last-child").attr('id');
	if(!v){
		//window.location='index.php';
		v='';
	}
	var fid='<?=$u[fid]?>';
	try{
	$.ajax({
		   url: "/admin_hm/action.php?type=selectliaotian",
         type: "POST",
         data:{time:v,fid:fid},
		     timeout:8000,   
         dataType: "json",
		 cache:false,
  success: function(msg){//msg为返回的数据，在这里做数据绑定
		if(msg==null){
			setTimeout(addli2(),1000);
			return false;
		}else if(msg=="zero"){ 
                      addli2();
                 }          
              else{  
          for(i in msg){
			 var   d=new Date(parseInt(msg[i].time)*1000);   
             var shijian=formatDate(d);
			 var str='<tr class="odd gradeX" id="'+msg[i].time+'"> <td> <input type="checkbox" class="checkboxes" name="liaotian" value="'+msg[i].lid+'" /></td> <td class="center hidden-480">'+msg[i].lid+'</td> <td class="center hidden-480">'+msg[i].content+'</td><td class="center hidden-480">'+msg[i].username+'</td> <td class="center hidden-480">'+shijian+'</td> <td class="center hidden-480" ><input type="button"  id="'+msg[i].lid+'" mid="'+msg[i].mid+'"  username="'+msg[i].username+'" adminid="'+msg[i].adminid+'" content=\''+msg[i].content+'\'  fid="'+msg[i].fid+'" id="'+msg[i].lid+'" onclick="fabu(this);return false;" name="" value="发布"/></td>  <td class="center hidden-480" ><input type="button"  id="'+msg[i].lid+'" onclick="delliaotian(this);return false;" name="" value="删除"/></td> </tr>';
		     $("#liuyanlist").append(str);
			  }
			  addli2();    
			  }
		  },
		    error:function(XMLHttpRequest,textStatus,errorThrown){
						   addli2();    
            }        
	});	
	}catch(e){
		alert("[超时，请刷新]");
		 addli2(); 
		}
}

$(document).ready(function(){ 
	//addli2();
});

function fabu(obj){
	var mid=$(obj).attr('mid');
	var adminid=$(obj).attr('adminid');
	var username=$(obj).attr('username');
	var tousername=$(obj).attr('tousername');
	var tomid=$(obj).attr('tomid');
	tomid = tomid != 0 && tomid ?tomid:'all';
	var content=$(obj).parents('tr').find('.xs_content').html();
	var fid=$(obj).attr('fid');
	 $.ajax({
         url: "/admin_hm/action.php?type=fabu",  
         type: "POST",
         data:{lid:obj.id},
         //dataType: "json",
         error: function(){  
           // alert('Error loading XML document');  
         },  
         success: function(data,status){//如果调用php成功  
			if(data=="true"){ 	
				//alert("提交成功");
				//$.wssend($.param( { sh_mid : mid,adminid:adminid,username:username,sh_content:content,fid:fid } ) );	//socket发送信息
				var sayObj;
				sayObj = {
				'type': 'shenhe',
				'uid': mid,
				'lid': obj.id,
				'umz': username,
				'tid': tomid,
				'tmz': tousername,
				'aid': adminid,
				'zmd': 0,
				'rid': fid,
				'content': content
			}
			ws.send(JSON.stringify(sayObj));

			}else{
			}
         }
     });  
	 	$(obj).parent().parent().remove();
}

function delliaotian(obj){

	 $.ajax({
         url: "/admin_hm/action.php?type=delliaotian",  
         type: "get",
         data:{lid:obj.id},
         //dataType: "json",
         error: function(){  
           // alert('Error loading XML document');  
         },  
         success: function(data,status){//如果调用php成功  
			if(data=="true"){ 	
				//alert("提交成功");
			}else{
			}
         }
     });  
	 	$(obj).parent().parent().remove();
}

function delall(){
	var liaotians=new Array();
	$("input[name='liaotian']").each(function(){
		if($(this).attr("checked")){
			  liaotians.push($(this).val());
			   $(this).parent().parent().remove(); 
		}
   
	});
	 $.ajax({
         url: "/admin_hm/action.php?type=delall",  
         type: "post",
         data:{lids:liaotians},
         //dataType: "json",
         error: function(){  
           // alert('Error loading XML document');  
         },  
         success: function(data,status){//如果调用php成功  
			if(data=="true"){ 	
				alert("操作成功");
			}else{
			}
         }
     });  

}

function fabuall(){
	var liaotians=new Array();
	$("input[name='liaotian']").each(function(){
		if($(this).attr("checked")){
				$(this).parent().parent().find('input[value="发布"]').click();
			   liaotians.push($(this).val());
			   $(this).parent().parent().remove(); 
		}
	});
	 $.ajax({
         url: "/admin_hm/action.php?type=fabuall",  
         type: "post",
         data:{lids:liaotians},
         //dataType: "json",
         error: function(){  
           // alert('Error loading XML document');  
         },  
         success: function(data,status){//如果调用php成功  
			if(data=="true"){ 	
				//alert("提交成功");
			}else{
			}
         }
     });  
 }

function selectall(){
	$("input[name='liaotian']").each(function(){
		$(this).attr("checked","true");
	});
}


</script> 
  <script type="text/javascript">
/*$('.group-checkable').click(function(){
$('tbody .checkboxes').click();
});
  </script> 
  <script type="text/javascript">
	/*$(function(){ 
		$("#liuyanlist").click(function(){
			$.ajax({
				   type: "POST",
				   url: "/admin_hm/action.php?type=selectliaotian",
				   success: function(msg){
					 $("#liuyanlist").text(msg);
				   }
			});
		});
	}); 
	//ajax请求所有未审核留言
	var ajaxFun;
	ajaxFun = function () {
		$.get('action.php?type=sh_list', {'fid': <?=$u['fid']?>}, function (data) {
			$('#liuyanlist').html(data);
		});
	}
	setInterval('ajaxFun()', 5000);
  </script> 
</body>
</html>
