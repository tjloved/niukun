<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	基础设置 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link href="../assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" rel="stylesheet" /><link href="../assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
<script src="../assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/swfobject/2.2/swfobject.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/web-socket-js/1.0.0/web_socket.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/json2/20150503/json2.min.js"></script>
<script type="text/javascript" src="../js/socket_config_xs.js"></script>
<script type="text/javascript" src="../js/socket_handan_xs.js"></script>
</head>
<!-- BEGIN BODY -->
<body class="fixed-top" onload="connect();">
	<!-- BEGIN HEADER -->
	    <?php include_once 'head.php'; ?> 
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	     <?php include_once 'left.php'; ?> 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     基础设置
                     <small>房间全局控制面板</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">系统设置</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">基础控制</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
<!-- BEGIN VALIDATION STATES-->
                   <div class="portlet box grey">
                       <div class="portlet-title">
                           <h4><i class="icon-reorder"></i>表单</h4>
                           <div class="tools">
                               <a href="javascript:;" class="collapse"></a>
                               <a href="#portlet-config" data-toggle="modal" class="config"></a>
                               <a href="javascript:;" class="reload"></a>
                               <a href="javascript:;" class="remove"></a>
                           </div>
                       </div>
                       <div class="portlet-body form">
                          <div style="margin:20px;background-color: rgb(56, 52, 54);width: 300px;height: 46px;line-height: 50px;">
                            <font  style="color:#F00;"> 当前房间：</font>
							<select name="fangjian" id="fangjian" onChange="romeChange()">
      <?php $sqlxs=''; if($u[fid]){ $sqlxs.=" where fid='$u[fid]'"; }  
	  $q_fangjian=$res->fn_sql("select * from fangjianlist $sqlxs");
	 while($fangjian=mysql_fetch_array($q_fangjian)){
	  ?>
      <option value="<?=$fangjian[fid]?>" <?php if($_GET[fid]==$fangjian[fid]) echo 'selected="selected"'; ?>><?=$fangjian[fname]?></option>
      <?php }?>
                            </select>
      <script  type="text/javascript">
 	 var fangjian=document.getElementById('fangjian');
	 var fid =fangjian.value;
	  function romeChange(){
		   var fid =fangjian.value;
		  window.location='am_peizhi.php?fid='+fid;
    }
	  </script>
      </div>
<!-- BEGIN FORM-->
    <?php
	$fid = 0;
	if($_GET[fid]){
		$fid=$_GET[fid];
	}else{
		$q_f=$res->fn_select("select * from fangjianlist $sqlxs");
		$fid=$q_f[fid];
	}
    $peizhi=$res->fn_select("select * from configlist where fid='$fid' ");	
	?>
	<script type="text/javascript">
		var rid = <?php echo $fid; ?>
	</script>
                           <div id="form_sample_1" class="form-horizontal">
                               <div class="alert alert-error hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   填写信息有误，请检查修正后提交！
                               </div>
                               <div class="alert alert-success hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   信息填写正确!正在提交...
                               </div>
                               <div class="control-group">
                                   <label class="control-label">网站标题:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="hdType" id="title" type="text" class="span6 m-wrap" placeholder="请输入网站标题" value="<?=$peizhi['wzbt']?>" />
										</div>                                   
                                       <img id="proLoading" style="margin:5px 0 0 180px; display:none;position:absolute;" src="../assets/img/loading.gif" />
                               </div>
                               <div class="control-group">
                                   <label class="control-label">关键词:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="name" id="keywords" type="text" class="span6 m-wrap" placeholder="请输入网站关键词" value="<?=$peizhi['wzgjz']?> "/>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">描  述:<span class="required">*</span></label>
                                   <div class="controls">
                                       <textarea name="description" id="description" style="height:89px;" class="span6 m-wrap" placeholder="请输入网站描述内容" ><?=$peizhi['wzms']?></textarea>
                                   </div>
                               </div>
                             
                               <div class="control-group">
                                   <label class="control-label">在线人数:</label>
                                   <div class="controls">
                                       <div class="input-prepend ">
                                           <span class="add-on"><i class="icon-comment"></i></span><input class="m-wrap " id="people" name="people" type="text" value="<?=$peizhi['zxrs']?>"/>
                                       </div>
                                   </div>
                               </div>
							   <div class="control-group">
                                   <label class="control-label">直播停止请关闭：</label>
                                   <div class="controls">
                                       <div class="danger-toggle-button">
                 <input type="checkbox" id="rauthor2" class="toggle" <?php if($peizhi['sjxz'])echo 'checked="checked"'; ?>/>				  
                                       </div><span style="line-height:34px;"><font color="red">勾选为开始直播，留空则无直播。</font></span>
                                   </div>
                               </div>

							   <div class="control-group">
                                   <label class="control-label">播放宣传视频：</label>
                                   <div class="controls">
										<input type="text" name="videoadd" id="videoadd"  class="m-wra tags" style="width:500px;" value="<?=$peizhi['videoadd']?>"/>
                                   </div><span style="line-height:34px;"><font color="red">当无直播时，请关闭上方的直播开关，并填写视频HTML源码。</font></span>
                               </div>
                           
                       <div class="control-group">
                                   <label class="control-label">YY频道ID:</label>
                                   <div class="controls">
								      <input type="text" name="yy" id="yy"  class=" m-wrap" placeholder="" value="<?=$peizhi['yyh']?>"/>
									   <span style="line-height:34px;"><font color="red">填写后，用于电脑看YY直播。</font></span>
                                   </div>
                               </div>
                      <div class="control-group">
                                   <label class="control-label">知牛频道ID:</label>
                                   <div class="controls">
                      <input type="text" name="yy2" id="yy2"  class=" m-wrap" placeholder="" value="<?=$peizhi['yyh2']?>"/>
					  <span style="line-height:34px;"><font color="red">填写后，用于手机推流看直播。</font></span>
                                   </div>
                               </div>
					<div class="control-group">
                                   <label class="control-label">游客试看时长:</label>
                                   <div class="controls">
                      <input type="text" name="zcjg" id="zcjg"  class=" m-wrap" placeholder="" value="<?=$peizhi['zcjg']?>"/>
                      <span style="line-height:34px"><font color="red">填写数字，时间单位：分钟</font></span>
                                   </div>
                               </div>
                               <div class="control-group">
                                   <label class="control-label">消息自动审核：</label>
                                   <div class="controls">
                                       <div class="danger-toggle-button">
                 <input type="checkbox" id="rauthor" class="toggle" <?php if($peizhi['sfsh'])echo 'checked="checked"'; ?>/>
                                       </div>
                                   </div>
                               </div>
		      <div class="control-group">
                                   <label class="control-label">屏蔽字段:</label>
                                   <div class="controls">
                                   
                                          <input class="span6 m-wrap " id="pbzd" name="pbzd" data-role="tagsinput" type="text" value="<?=$peizhi['pbzd']?>"/><span style="line-height:34px">请用“|”分割</span>
								  </div>        
                </div>
				<div class="control-group">
                                   <label class="control-label">上传文件类型:</label>
                                   <div class="controls">
                                       <input id="tags_2" type="text" class="m-wra tags" style="width:500px;" value="jpg,png,gif,bmp,jpeg" />
                                   </div>
                               </div>
			 <div class="control-group">
                                   <label class="control-label">LOGO上传:</label>
                                   <div class="controls">
                                       <div class="input-prepend input-append">
											<form action="/sys/logo.php?fid=<?=$fid?>" enctype="multipart/form-data" method="post" name="upform">
											<input name="upfile" type="file">
											<input type="submit" name="submit" value="提交" />
											</form>
										</div>
                                   </div>
            </div>
				<div class="control-group">
				   <label class="control-label">进场图片上传:</label>
					<div class="controls">
					   <div class="input-prepend input-append"><form action="/sys/logo_min.php?fid=<?=$fid?>" enctype="multipart/form-data" method="post" name="upform">
						  <input name="upfile" type="file">
						  <input type="submit" name="submit" value="提交" />
						  </form>
						 <span style="line-height:34px"><font color="red">直播间弹窗广告图,800*350PX。</font></span>
					</div>
				   </div>
				</div>
          </div>
        </div>
                               <div class="form-actions">
                                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <iframe src="" id="frameSend" style="display:none"></iframe>
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" class="btn green">OK</button>
                                       </div>
                                   </div>
                                   <button type="button" id="save" class="btn purple">保存</button>
                                   <button type="button" onclick="javascript:window.location.reload();" class="btn">重置</button>
                               </div>
                           </div>
                       </div>
                   </div>
                  <!-- END VALIDATION STATES-->
               </div>
            </div>
         </div>
		</div>
	</div>
<?php include_once 'foot.php' ?>
      
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <script src="../assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="../assets/js/app.js"></script>     
   <script>
       function reloadThisPage() {
           window.location.reload();
       }
       jQuery(document).ready(function () {
           // initiate layout and plugins
          App.setPage("form_validation");
           App.init();
           $("#save").click(function () {
               var title = $("#title").val();
               var keywords = $("#keywords").val();
               var description = $("#description").val();
               var people = $("#people").val();
			   if ($("#rauthor2").attr("checked") == "checked") {
                   rauthor2 = "1";//是
               }
               else {
                   rauthor2 = "0";
               }
			   var videoadd = $("#videoadd").val();
         		var yy = $("#yy").val();
				var yy2 = $("#yy2").val();
				var zcjg = $("#zcjg").val();
				var pbzd = $("#pbzd").val();
               //注册会员发言是否审核 
               if ($("#rauthor").attr("checked") == "checked") {
                   rauthor = "1";//是
               }
               else {
                   rauthor = "0";
               }
                       $.ajax({
                           type: "post",
                           url: "action.php?type=peizhi&fid="+fid,
                           data:{title:title,keywords:keywords,description:description,people:people,rauthor2:rauthor2,videoadd:videoadd,yy:yy,yy2:yy2,zcjg:zcjg,rauthor:rauthor,pbzd:pbzd},
                           success: function (data) {
                               if (data == "success") {
									if("0" == rauthor2){
										var dataObj = {
											'type': 'force',
											'mid': 0,
											'rid': rid
										};
										ws.send(JSON.stringify(dataObj));
									}
                                   $("#myModal2a").click();
                                   $("#myModal2 p").html("提交成功!");
         setInterval(reloadThisPage, 3000);
                               }else {
								     $("#myModal2a").click();
                               $("#myModal2 p").html("服务器内部错误!");
								   }
                           },
                           error: function (err) {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("服务器内部错误!");
                           }
                       });
           });
       });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>
