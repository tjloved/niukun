<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	品牌管理 - 直播管理中心
</title><meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link href="../assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<?php include_once 'head.php' ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	    <?php include_once 'left.php' ?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     喊单管理
                     <small>在这里编辑品牌信息</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">喊单管理</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">品牌管理</a>
                      
                     </li>
          
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet box grey">
                        <div class="portlet-title">
                            <h4><i class="icon-reorder"></i>品牌信息</h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form action="#" id="form_sample_2" class="form-horizontal">
                                <div class="alert alert-error hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    填写信息有误，请检查修正后提交！
                                </div>
                                <div class="alert alert-success hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    信息填写正确!正在提交...
                                </div>
                                
                                
                                <div class="control-group">
                                   <label class="control-label">品牌名称:<span class="required">*</span></label>
                                   <div class="controls">
                        <input type="text" id="pname" value=""/>
                                   </div>
                               </div>
               <div class="control-group" id="fangjian_choice">
                                   <label class="control-label">分类：<span class="required">*</span></label>
                                   <div class="controls">
                                       <div class="danger-toggle-button" id="dtb">
                                           <select name="fenlei" id="fenlei">
                                           <option value="">请选择分类...</option>
<?php 



$rs=$res->fn_sql("select * from fenleilist ");
while($fenlei=mysql_fetch_array($rs)){
?>
<option value="<?=$fenlei[flid]?>"><?=$fenlei[name]?></option>
<?php } ?>
</select>    
                                       </div>
                                   </div>
                               </div>
                                <input type="hidden" id="pid" value=""/>
                                
                                <div class="form-actions">
                                    <a href="#myModal3" role="button" id="myModal3a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                    <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h3 id="myModalLabel3">系统提示</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p>操作成功！Loading...</p>
                                        </div>
                                        
                                        
                                        <div class="modal-footer">
                                            <button data-dismiss="modal" class="btn green">OK</button>
                                        </div>
                                    </div>
                                    <button type="button" id="save" class="btn purple">保存</button>
                                    <button type="button" onclick="javascript:window.location.reload();" class="btn">重置</button>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>品牌列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">工具 <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">打印</a></li>
											<li><a href="#">保存为PDF</a></li>
											<li><a href="#">导出Excel</a></li>
										</ul>
									</div>
								</div>
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>

											<th>ID</th>
											<th class="hidden-480">品牌名称</th>
											<th class="hidden-480">分类</th>									
                                            <th class="hidden-480">功能</th>
										</tr>
									</thead>
									<tbody>
<?php  $q_pinpai=$res->fn_sql("select t1.*,t2.name from pinpailist t1 left join fenleilist t2 on t1.flid=t2.flid  $sql_plus");

while($pinpai=mysql_fetch_array($q_pinpai)){
?>           


                                                <tr class="odd gradeX" id='tr4'>
                                                    
                                                    <td class="center hidden-480"><?=$pinpai[pid]?></td>
                                                    
                                                    
                                                    <td class="center hidden-480"><?=$pinpai[pname]?></td>
                                                                     <td class="center hidden-480"><?=$pinpai[name]?></td>
                                                    <td class="center hidden-480">
                                                        <div class="btn-group">
                                                            <button class="btn red dropdown-toggle" data-toggle="dropdown" style="margin-bottom: 0px;">操作<i class="icon-angle-down"></i></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#" onclick="editManager('<?=$pinpai[pid]?>','<?=$pinpai[pname]?>','<?=$pinpai[flid]?>')">编辑</a></li>
                                                                <li><a href="/sys/delete.php?table=pinpailist&field=pid&id=<?=$pinpai[pid]?>">删除</a></li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
         <?php } ?>                             	
                                        							
									</tbody>
								</table>
							</div>
						</div>
                   <a href="#myModal3" role="button" id="A1" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                   <input type="hidden" id="noManagerID"  value=""/>
                                   <div id="Div1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="H1">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delManager" class="btn red">删除</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
                </div>
                <input type="hidden" id="updateID" value="0" />
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../../../../Scripts/jquery-1.10.2.min.js"></script>
    <script src="../../../../Scripts/jquery-migrate-1.1.1.js"></script> 
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <script src="../assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
   <script src="../assets/js/app.js"></script>   
    
    <script src="../../../../Scripts/jquery.signalR-2.1.0.min.js"></script>
    <script src="../../../../signalr/hubs"></script>  
   <script>

       function reloadThisPage() {
           window.location.reload();
       }

       function editManager(pid,pname,flid) {
           $("#pid").val(pid);
           $("#pname").val(pname);
		   $("#fenlei").val(flid);
       }

       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("form_validation");
           App.init();

           $("#save").click(function () {
           
                   var pid = $("#pid").val();
                 
                   var pname = $("#pname").val();
				   var flid = $("#fenlei").val();
					
                   $.ajax({
                       type: "post",
                       url: "action.php?type=addpinpai",
                       data: {pid:pid,pname:pname,flid:flid},
                     
                       success: function (data) {
                           if (data= "success") {
                             
                               $("#myModal3a").click();
                               $("#myModal2 p").html("提交成功!");
                               setInterval(reloadThisPage, 3000);
                           }
                           else {
                               $("#myModal3a").click();
                               $("#myModal2 p").html("提交失败");
                           }
                       },
                       error: function (err) {
                           $("#myModal3a").click();
                           $("#myModal2 p").html("服务器内部错误!");
                       }
                   });
    
           });
       });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>
