<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	实盘管理 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<?php include_once 'head.php'; ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	 	<?php include_once 'left.php'; ?> 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     实盘管理
                     <small>实盘列表</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">实盘管理</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">实盘列表</a>
                     
                     </li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <iframe src='/admin_hn/am_shipan.php' style="width:800px; height:600px" ></iframe>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>     
   <script>

       function showRate(id,rate,level)
       {
           if (level <= 5)
           {
               $("#imgLevel").attr("src", "../../../../stylesheets/level/" + level + ".png");
           }
           else {
               if(level == 0)
               {
                   level = 1;
                   $("#imgLevel").attr("src", "../../../../stylesheets/level/1.png");
               }
               else {
                   $("#imgLevel").attr("src", "../../../../stylesheets/level/" + level + ".gif");
               }
           }
           $("#level").val(level);
           $("#myModal3a").click();
           $("#noUserID").val(id);
           $("#rate").val(rate);
       }

       function delUser(id, name) {
           $("#delUser").show();
           $("#close").html("取消");
           $("#noUserID").val(id);
           $("#myModal2a").click();
           $("#myModal2 p").html("是否删除用户：" + name + "?");
       }

       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("table_editable");
           App.init();

           $("#set_rate").click(function () {
               var id = $("#noUserID").val();
               var jifen = $("#rate").val();
               var level = $("#level").find("option:selected").val();
               $.ajax({
                   type: "post",
                   url: "Default.aspx/updateRate",
                   data: "{'id':'" + id + "','jifen':'"+jifen+"'}",
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   beforeSend: function () {
                       $(".modal-backdrop").fadeOut();
                   },
                   success: function (data) {
                       if (data.d == "1") {
                           //成功
                           //$("#myModal2a").click();
                           //$("#myModal2 p").html("操作成功！");
                           $("#sp" + id).html(jifen);
                           if (level <= 5) {
                               $("#img" + id).attr("src", "../../../../stylesheets/level/" + level + ".png");
                           }
                           else {
                               if (level == 0) {
                                   level = 1;
                                   $("#img" + id).attr("src", "../../../../stylesheets/level/1.png");
                               }
                               else {
                                   $("#img" + id).attr("src", "../../../../stylesheets/level/" + level + ".gif");
                               }
                           }
                       }
                       else {

                       }
                   },
                   error: function (err) { }
               });
           });

        
       });
   </script>
   <!-- END JAVASCRIPTS -->   
   <script type="text/javascript">
/*$('.group-checkable').toggle(function(){$('tbody .checkboxes').click();},function(){$('tbody .checkboxes').click();});*/
  </script>
</body>
</html>
