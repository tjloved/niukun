<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	投票设置 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" />
<link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" />
<link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link href="../assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" rel="stylesheet" /><link href="../assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" /><link href="../assets/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet" /></head>
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	    <?php include_once 'head.php'; ?> 
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	     <?php include_once 'left.php'; ?> 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     系统设置
                     <small>投票设置</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">系统设置</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">投票设置</a>
                     </li>
                    
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
<!-- BEGIN VALIDATION STATES-->
                   <div class="portlet box grey">
                       <div class="portlet-title">
                           <h4><i class="icon-reorder"></i>表单</h4>
                           <div class="tools">
                               <a href="javascript:;" class="collapse"></a>
                               <a href="#portlet-config" data-toggle="modal" class="config"></a>
                               <a href="javascript:;" class="reload"></a>
                               <a href="javascript:;" class="remove"></a>
                           </div>
                       </div>
                       <div class="portlet-body form">
            <div style="margin:20px;background-color: rgb(56, 52, 54);
width: 300px;
height: 46px;
line-height: 50px;">
            
     <font  style="color:#F00;"> 当前房间：</font><select name="fangjian" id="fangjian" onChange="romeChange()">
      <?php 
	
	      $sqlxs='';
   if($u[fid]){
	   		$sqlxs.=" where fid='$u[fid]'";
	 }  
	  $q_fangjian=$res->fn_sql("select * from fangjianlist $sqlxs");
	 while($fangjian=mysql_fetch_array($q_fangjian)){
	  ?>
      <option value="<?=$fangjian[fid]?>" <?php if($_GET[fid]==$fangjian[fid]) echo 'selected="selected"'; ?>><?=$fangjian[fname]?></option>
      <?php }?>
      </select>
      <script  type="text/javascript">
 	 var fangjian=document.getElementById('fangjian');
	 var fid =fangjian.value;

	  function romeChange(){
		   var fid =fangjian.value;
		  window.location='am_toupiao.php?fid='+fid;
    }
	  </script>
      
      </div>
                           <!-- BEGIN FORM-->
<?php 
	if($_GET[fid]){
		$fid=$_GET[fid];
	}else{
		
		$q_f=$res->fn_select("select * from fangjianlist $sqlxs");
		$fid=$q_f[fid];
	}

    $toupiao=$res->fn_select("select * from toupiao_count where fid ='$fid'");
?>

                           <div id="form_sample_1" class="form-horizontal">
                               <div class="alert alert-error hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   填写信息有误，请检查修正后提交！
                               </div>
                               <div class="alert alert-success hide">
                                   <button class="close" data-dismiss="alert"></button>
                                   信息填写正确!正在提交...
                               </div>
                               
                               <div class="control-group">
                                   <label class="control-label">主题:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="title" id="title" type="text" class="span6 m-wrap" placeholder="" value="<?=$toupiao[title]?>" />
										</div>
                               </div>
                               
                               
                               <div class="control-group">
                                   <label class="control-label">好:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="op1" id="op1" type="text" class="span6 m-wrap" placeholder="" value="<?=$toupiao[op1]?>" />
										人</div>
                               </div>
 					  <div class="control-group">
                                   <label class="control-label">中:<span class="required">*</span></label>
                                   <div class="controls">
                                       <input name="op2" id="op2" type="text" class="span6 m-wrap" placeholder="" value="<?=$toupiao[op2]?>" />
								人</div>
                       </div>
                       <div class="control-group">
                                   <label class="control-label">差:<span class="required">*</span></label>
                                  <div class="controls">
                                       <input name="op3" id="op3" type="text" class="span6 m-wrap" placeholder="" value="<?=$toupiao[op3]?>" />
										人</div>
                       </div>
                       
                                              
   
                             <div class="form-actions">
                                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <iframe src="" id="frameSend" style="display:none"></iframe>
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" class="btn green">OK</button>
                                       </div>
                                   </div>
                                   <button type="button" id="save" class="btn purple">保存</button>
                                   <button type="button" onclick="javascript:window.location.reload();" class="btn">重置</button>
                               </div>
                           </div><!--form/end-->

                        
                           <!-- END FORM-->
                       </div>
                   </div>
                  <!-- END VALIDATION STATES-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="../assets/js/jquery-1.8.3.min.js"></script>    
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script src="../assets/jquery-tags-input/jquery.tagsinput.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
    <script src="../assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
   <script src="../assets/js/app.js"></script>     

   <script>
       function reloadThisPage() {
           window.location.reload();
       }


       jQuery(document).ready(function () {
           // initiate layout and plugins
           App.setPage("form_validation");
           App.init();

      

           $("#save").click(function () {
			   var title=$("#title").val();
               var  op1= $("#op1").val();
               var op2 = $("#op2").val();
			   var op3 = $("#op3").val();
               var fangjian = $("#fangjian").val();
               
         	
               //注册会员发言是否审核 
         
             
             
                       $.ajax({
                           
                           url: "action.php?type=toupiao",
                           data:{title:title,op1:op1,op2:op2,op3:op3,fangjian:fangjian},
						   type: "post",
                           success: function (data) {
                               if (data == "success") {
                                   $("#myModal2a").click();
                                   $("#myModal2 p").html("提交成功!");
          setInterval(refrensepage, 3000);
                               }else {
								     $("#myModal2a").click();
                               $("#myModal2 p").html("服务器内部错误!");
								   }
                           },
                           error: function (err) {
                               $("#myModal2a").click();
                               $("#myModal2 p").html("服务器内部错误!");
                           }
                       });
                  
         
           });
       });
   </script>
   <!-- END JAVASCRIPTS -->   
</body>
</html>
