<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	会员管理 - 直播管理中心
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link href="../assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" /><link href="../assets/css/timepicker.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /><link rel="stylesheet" type="text/css" href="../assets/chosen-bootstrap/chosen/chosen.css" /><link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" />
<script src="../assets/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/swfobject/2.2/swfobject.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/web-socket-js/1.0.0/web_socket.min.js"></script>
<script type="text/javascript" src="//cdn.bootcss.com/json2/20150503/json2.min.js"></script>
<script type="text/javascript" src="../js/socket_config_xs.js"></script>
<script type="text/javascript" src="../js/socket_handan_xs.js"></script>
</head>
<!-- BEGIN BODY -->
<body class="fixed-top" onload="connect();">
	<!-- BEGIN HEADER -->
	<?php include_once 'head.php'; ?>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	 	<?php include_once 'left.php'; ?> 
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color" style="display:none"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>
                  <!-- END BEGIN STYLE CUSTOMIZER -->     
                  <h3 class="page-title">
                     会员管理
                     <small>会员列表</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="#">管理中心</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">会员管理</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">会员列表</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                   <!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>会员列表</h4>
								<div class="tools">
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">工具 <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">打印</a></li>
											
											<li><a href="am_users_xls.php">导出Excel</a></li>
										</ul>
									</div>
								</div>
 <?php
if($_GET[tuijianmid]){
	$datou=$res->fn_select("select * from userlist where mid  = '$_GET[tuijianmid]'");
	echo "<font color='red'>".$datou[username]."</font>的下线列表<br>";
}
?>
<form action="action.php?type=userpiliang" method="post">
								<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>ID</th>
											<th class="hidden-480">用户名</th>
                                            <th class="hidden-480">房间</th>
                                            <th class="hidden-480">手机号</th>
                                            <th class="hidden-480">上线</th>
                                            <th class="hidden-480">下限个数</th>
                                            <th class="hidden-480">qq</th>
                                            <th class="hidden-480">用户组</th>
											<th class="hidden-480">注册时间</th>
											<th class="hidden-480">IP</th>
											<th class="hidden-480">登录次数</th>
											<th class="hidden-480">备注</th>
                                            <th class="hidden-480">功能</th>
										</tr>
									</thead>
									<tbody>  
<?php
if($_GET[tuijianmid]){
	$sqlxs=" and t1.tuijianmid = '$_GET[tuijianmid]'";
}   if($u[fid]){
	   		$sqlxs.=" and t1.fid='$u[fid]'";
	 }
$sql="select t1.*,t2.adminname,t3.fname from userlist t1 left join admin_group t2 on t1.adminid=t2.adminid left join fangjianlist t3 on t1.fid=t3.fid where t1.adminid <>14 and jiaren=0  $sqlxs order by mid desc";
$q_users = $res->fn_sql($sql);
while($user = mysql_fetch_array($q_users)){
?>
                                                <tr class="odd gradeX" id="tr12270">
                                                    <td><input type="checkbox" class="checkboxes" name="mid[]" value="<?=$user[mid]?>" /></td>
                                                    <td class="center hidden-480"><?=$user[mid]?></td>
                                                    <td class="center hidden-480"><?=$user[username]?></td>
                                                    <td class="center hidden-480"><?=$user[fname]?></td>
                                                    <td class="center hidden-480"><?=$user[telephone]?></td>
                                                    <td class="center hidden-480"><?php
if($user[tuijianmid]){
	$shangxian=$res->fn_select("select * from userlist where mid = '$user[tuijianmid]'");
	echo "<a href=\"addusers.php?mid=".$shangxian[mid]."\" >".$shangxian[username]."</a>";
}else{
	echo "<font color='#333'>无上线</font>";
}
?></td>
                                                    <td class="center hidden-480"><?php
echo @$xiaxian_num=$res->fn_num("select * from userlist where tuijianmid = '$user[mid]'");
if($xiaxian_num!=0){
	echo "<a href='am_users.php?tuijianmid=".$user[mid]."'> &nbsp;&nbsp;查看</a>";
}
?>
</td>
                                                    <td class="center hidden-480"><?=$user[qq]?></td>
                                                    <td class="center hidden-480"><?=$user[adminname]?></td>
													<td class="center hidden-480"><?=date('Y-m-d H:i:s',$user[regtime])?></td>
													<td class="center hidden-480"><?=$user[ip]?></td>
													<td class="center hidden-480"><?=$user[login_count]?></td>
													<td class="center hidden-480"><?=$user[beizhu]?></td>
                                                    <td class="center hidden-480">
                                                        <div class="btn-group">
                                                            <button class="btn red dropdown-toggle" data-toggle="dropdown" style="margin-bottom: 0px;">操作<i class="icon-angle-down"></i></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="addusers.php?mid=<?=$user[mid]?>">修改</a></li>
																<li><a href="javascript:;" data-rid="<?php echo $user['fid']; ?>" data-mid="<?php echo $user['mid']; ?>" btn-del-user>删除</a></li>
															</ul>
                                                        </div>
                                                    </td>
                                                </tr>
<?php
}
?>          								
									</tbody>
								</table>
<div style="margin-top:8px">
<input type="button" value="全选" onclick="quanxuan()" style="float:left; height:24px;margin-right:20px"/>
<input type="submit" name="piliangshenhe" value="批量审核" style=" float:left;width:96px; height:24px"/>
<select name="fangjian" id="fangjian" style=" float:left; margin-left:20px" >
<?php
$q_fangjian=$res->fn_sql("select * from fangjianlist");
while($fangjian=mysql_fetch_array($q_fangjian)){
?>
<option value="<?=$fangjian[fid]?>"><?=$fangjian[fname]?></option> 
<?php }?>
</select>
<input type="submit" name="piliangfangjian" value="批量移入房间" style=" float:left;width:96px; height:24px"/>

<input type="submit" name="piliangshanchu" value="批量删除" style=" float:right;width:96px; height:24px"/>
<input type="button" name="daochu" value="导出EXCEL" style=" width:96px; height:24px" onclick="window.location='excel.php';"/>
</div>
</form>
							</div>
						</div>
                   <a href="#myModal2" role="button" id="myModal2a" style="display: none" class="btn btn-danger" data-toggle="modal">Alert</a>
                   <input type="hidden" id="noUserID" value=""/>
                                   <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                           <h3 id="myModalLabel2">系统提示</h3>
                                       </div>
                                       <div class="modal-body">
                                           <p>Body goes here...</p>
                                       </div>
                                       <div class="modal-footer">
                                           <button data-dismiss="modal" id="delUser" class="btn red">删除</button>
                                           <button data-dismiss="modal" id="close" class="btn green">取消</button>
                                       </div>
                                   </div>
						<!-- END SAMPLE TABLE PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<?php include_once 'foot.php' ?>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->    
   <script src="../assets/breakpoints/breakpoints.js"></script>      
   <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="../assets/js/jquery.blockui.js"></script>
   <script src="../assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="assets/js/excanvas.js"></script>
   <script src="assets/js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="../assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="../assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.js"></script>
   <script type="text/javascript" src="../assets/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
   <script src="../assets/js/app.js"></script>     
   <script>
	    function quanxuan(){
				$("tbody .checkboxes").each(function(){
				  if($(this).attr('checked')){
					 $(this).removeAttr('checked');
					 $(this).parent().removeClass("checked");
				 }else{
				 	 $(this).attr('checked','checked');
					 $(this).parent().addClass("checked");
				 }
			});
	   }
       function showRate(id,rate,level)
       {
           if (level <= 5)
           {
               $("#imgLevel").attr("src", "../../../../stylesheets/level/" + level + ".png");
           }
           else {
               if(level == 0)
               {
                   level = 1;
                   $("#imgLevel").attr("src", "../../../../stylesheets/level/1.png");
               }
               else {
                   $("#imgLevel").attr("src", "../../../../stylesheets/level/" + level + ".gif");
               }
           }
           $("#level").val(level);
           $("#myModal3a").click();
           $("#noUserID").val(id);
           $("#rate").val(rate);
       }

       function delUser(id, name) {
           $("#delUser").show();
           $("#close").html("取消");
           $("#noUserID").val(id);
           $("#myModal2a").click();
           $("#myModal2 p").html("是否删除用户：" + name + "?");
       }

       $(document).ready(function () {
           // initiate layout and plugins
           App.setPage("table_editable");
           App.init();
		
		//删除操作重写
          $('[btn-del-user]').click(function () {
			  var mid, rid;
			  mid = $(this).data('mid');
			  mid = mid ? parseInt(mid) : 0;
			  rid = $(this).data('rid');
			  rid = rid ? parseInt(rid) : 0;
			  if(mid > 0){
				  $.post('delusers.php', {mid: mid}, function (data) {
					if(data.status > 0){
						//socket发送
						var dataObj = {
							'type': 'force',
							'mid': mid,
							'rid': rid
						};
						ws.send(JSON.stringify(dataObj));
					}
					alert(data.info);
					window.location.href='./am_users.php';
				}, 'json');
			  }
		  });
       });
   </script>
   <!-- END JAVASCRIPTS -->   
   <script type="text/javascript">
$('.group-checkable').click(function(){
quanxuan();
});
  </script>
</body>
</html>
