	<div class="footer">
		2014 &copy; <?=$_SERVER['HTTP_HOST']?>
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
    
    <script type="text/javascript">
	function setMenu(){
		var Menu_main,Menu_child;
		var menu_parent=document.getElementsByClassName('page-title')		
		 Menu_main =menu_parent[0].childNodes[0].nodeValue.replace(/(^\s*)|(\s*$)/g,'');
		Menu_child=menu_parent[0].childNodes[1].innerHTML.replace(/(^\s*)|(\s*$)/g,'');
		var lis=document.getElementById('left_menu').getElementsByTagName("li");
		for(var i=0;i<lis.length;i++){	
			lis[i].setAttribute("class","has-sub");
			if(lis[i].innerHTML.indexOf(Menu_main)>-1){
				lis[i].setAttribute("class","has-sub open start active");
				lis[i].getElementsByTagName("ul")[0].style.display='block';
				lis[i].getElementsByTagName("a")[0].getElementsByTagName("span")[1].setAttribute("class","selected");
			}
		}
		
	}
	setMenu();
	</script>