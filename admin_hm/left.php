	<div class="page-sidebar nav-collapse " id="left_menu">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
			<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
			<?php if($u[quanxian]=='超级管理员'){ ?>
				<li>
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<form class="sidebar-search" style="display:none">
						<div class="input-box">
							<a href="javascript:;" class="remove"></a>
							<input type="text" style="height:27px;" placeholder="搜索..." />				
							<input type="button" class="submit" value=" " />
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="has-sub">
					<a href="javascript:;">
					<i class="icon-home"></i> 
					<span class="title">管理中心</span>
					<span class="arrow open"></span>
					</a>
					<ul class="sub">
						<li><a href="am_liuyan.php">留言审核</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="am_peizhi.php">
					<i class="icon-cogs"></i> 
					<span class="title">系统设置</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub">
					 <li ><a href="am_peizhi.php">基础设置</a></li>
                     <li ><a href="am_group.php">用户组设置</a></li>
                     <li ><a href="am_toupiao.php">投票设置</a></li>
                     <li ><a href="am_admin.php">管理员设置</a></li>
                     <li ><a href="am_qq.php">客服QQ设置</a></li>
                     <li ><a href="am_huandeng.php">图片设置</a></li>
					 <li ><a href="am_ipban.php">IP限制</a></li>
					 <li ><a href="am_duanxin.php">短信接口设置</a></li>
					</ul>
				</li>
                <li class="has-sub ">
					<a href="am_users.php">
					<i class="icon-group"></i> 
					<span class="title">会员管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addusers.php">新增会员</a></li>
						<li><a href="am_users.php">会员列表</a></li>
						<li><a href="am_liaotian.php">聊天记录管理</a></li>
					</ul>
				</li>
                  <li class="has-sub ">
					<a href="am_news.php">
					<i class="icon-book"></i> 
					<span class="title">单页管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
                        <li ><a href="addnews.php">单页发布</a></li>
						<li ><a href="am_news.php">单页列表</a></li>
					</ul>
				</li>
                   <li class="has-sub ">
					<a href="addhandan.php">
					<i class="icon-bullhorn"></i>
					<span class="title">喊单管理</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub" >						
					<li><a href="addhandan.php">发布喊单</a></li>
                  <li><a href="am_handan.php">喊单列表</a></li>	
                  <li><a href="am_pinpai.php">品牌列表</a></li>
				   <li><a href="am_fenlei.php">分类列表</a></li>
					</ul>
			</li>
		       <li class="has-sub ">
					<a href="am_kecheng.php">
					<i class="icon-calendar"></i> 
					<span class="title">课程管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="am_kecheng.php">普通课程表</a></li>
						<li ><a href="am_jiaoshi.php">教师管理</a></li>
					</ul>
				</li>

			
                <li class="has-sub">
					<a href="am_fangjian.php">
					<i class="icon-home"></i> 
					<span class="title">房间管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="addfangjian.php">新增房间</a></li>
                        <li ><a href="am_fangjian.php">房间列表</a></li>
					</ul>
				</li>
                <li class="has-sub ">
					<a href="am_jiaren.php">
					<i class="icon-github-alt"></i> 
					<span class="title">假人管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="am_jiaren.php">假人列表</a></li>
              		 	 <li > <a href="am_jiahua.php">假话列表</a></li>
                 		 <li > <a href="addjiahua.php">发布假话</a></li>
					</ul>
				</li>

				<li class="has-sub ">
					<a href="am_zhuanjia.php">
					<i class="icon-group"></i> 
					<span class="title">专家管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addzhuanjia.php">添加专家</a></li>
						<li><a href="am_zhuanjia.php">专家列表</a></li>
					</ul>
				</li>
				
				<li class="has-sub ">
					<a href="am_video.php?type=精彩回放">
					<i class="icon-reorder"></i> 
					<span class="title">精彩回放管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addvideo.php?type=精彩回放">添加精彩回放</a></li>
						<li><a href="am_video.php?type=精彩回放">精彩回放列表</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="am_news2.php?type=市场评论">
					<i class="icon-shopping-cart"></i> 
					<span class="title">市场评论管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addnews2.php?type=市场评论">添加市场评论</a></li>
						<li><a href="am_news2.php?type=市场评论">市场评论列表</a></li>
					</ul>
				</li>
			
				<li class="has-sub ">
					<a href="am_news2.php?type=文件共享">
					<i class="icon-folder-close"></i> 
					<span class="title">文件共享管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addnews2.php?type=文件共享">添加文件共享</a></li>
						<li><a href="am_news2.php?type=文件共享">文件共享列表</a></li>
					</ul>
				</li>
				      <li class="has-sub ">
					<a href="am_wenda.php">
					<i class="icon-folder-close"></i> 
					<span class="title">问答管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addwenda.php">发布问答</a></li>
						<li><a href="am_wenda.php">问答列表</a></li>
					</ul>
				</li>
			 <li class="has-sub">
					<a href="am_mingan.php">
					<i class="icon-comments"></i> 
					<span class="title">监控预警管理</span>
					<span class="arrow"></span>
					</a>
				</li>
                <li class="">
					<a href="/sys/off.php">
					<i class="icon-off"></i> 
					<span class="title">退出登录</span>
					</a>
				</li>
				<?php } ?><!--假话管理员开始-->
				<?php if($u[quanxian]=='假话管理员'){?>
                <li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-github-alt"></i> 
					<span class="title">假人管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
                  
						<li ><a href="am_jiaren.php">假人列表</a></li>
              		 	 <li > <a href="am_jiahua.php">假话列表</a></li>
                 		 <li > <a href="addjiahua.php">发布假话</a></li>
					</ul>
				</li>
				<?php }?><!--假话管理员结束-->
						<!--房间管理员-->
				<?php if($u[quanxian]=='房间管理员'){?>
<li class="start active">
					<a href="javascript:;">
					<i class="icon-home"></i> 
					<span class="title">管理中心</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub">
						<li><a href="am_liuyan.php">留言审核</a></li>
					</ul>
				</li>
                <li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-group"></i> 
					<span class="title">会员管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addusers.php">新增会员</a></li>
						<li><a href="am_users.php">会员列表</a></li>
						<li><a href="am_liaotian.php">聊天记录管理</a></li>
					</ul>
				</li>
                
                  <li class="has-sub ">
					<a href="am_news.php">
					<i class="icon-book"></i> 
					<span class="title">单页管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
                        <li ><a href="addnews.php">单页发布</a></li>
						<li ><a href="am_news.php">单页列表</a></li>
					</ul>
				</li>

		       <li class="has-sub ">
					<a href="am_kecheng.php">
					<i class="icon-calendar"></i> 
					<span class="title">课程管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="am_kecheng.php">普通课程表</a></li>
						<li > <a href="am_jiaoshi.php">教师管理</a></li>
					</ul>
				</li>
				    <li class="has-sub ">
					<a href="am_fangjian.php">
					<i class="icon-home"></i> 
					<span class="title">房间管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
                        <li ><a href="am_fangjian.php">房间列表</a></li>
					</ul>
				</li>
                
                <li class="has-sub ">
					<a href="am_jiaren.php">
					<i class="icon-github-alt"></i> 
					<span class="title">假人管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
                <li ><a href="am_jiaren.php">假人列表</a></li>
              		 	 <li > <a href="am_jiahua.php">假话列表</a></li>
                 		 <li > <a href="addjiahua.php">发布假话</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="am_zhuanjia.php">
					<i class="icon-group"></i> 
					<span class="title">专家管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addzhuanjia.php">添加专家</a></li>
						<li><a href="am_zhuanjia.php">专家列表</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="am_news2.php?type=精彩回放">
					<i class="icon-reorder"></i> 
					<span class="title">精彩回放管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addnews2.php?type=精彩回放">添加精彩回放</a></li>
						<li><a href="am_news2.php?type=精彩回放">精彩回放列表</a></li>
					</ul>
				</li>
				
				   <li class="has-sub ">
					<a href="am_wenda.php">
					<i class="icon-folder-close"></i> 
					<span class="title">问答管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addwenda.php">发布问答</a></li>
						<li><a href="am_wenda.php">问答列表</a></li>
					</ul>
				</li>
			    <li class="has-sub">
					<a href="am_mingan.php">
					<i class="icon-comments"></i> 
					<span class="title">监控预警管理</span>
					<span class="arrow"></span>
					</a>
				</li>
				<?php }?>

<!--代理商开始-->
<?php if($u[quanxian]=='代理商'){?>
				<li class="start active">
					<a href="am_liuyan.php">
					<i class="icon-home"></i> 
					<span class="title">管理中心</span>
					<span class="arrow"></span>
					</a>
					<ul class="sub">
						<li><a href="am_liuyan.php">留言审核</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="am_peizhi.php">
					<i class="icon-cogs"></i> 
					<span class="title">系统设置</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
					 <li ><a href="am_peizhi.php">基础设置</a></li>
                     <li ><a href="am_qq.php">客服QQ设置</a></li>
					</ul>
				</li>
                <li class="has-sub ">
					<a href="am_users.php">
					<i class="icon-group"></i> 
					<span class="title">会员管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addusers.php">新增会员</a></li>
						<li><a href="am_users.php">会员列表</a></li>
						<li><a href="am_liaotian.php">聊天记录管理</a></li>
					</ul>
				</li>
                
                  <li class="has-sub ">
					<a href="am_news.php">
					<i class="icon-book"></i> 
					<span class="title">单页管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
                        <li ><a href="addnews.php">单页发布</a></li>
						<li ><a href="am_news.php">单页列表</a></li>
					</ul>
				</li>

		       <li class="has-sub ">
					<a href="am_kecheng.php">
					<i class="icon-calendar"></i> 
					<span class="title">课程管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="am_kecheng.php">普通课程表</a></li>
						  <li > <a href="am_jiaoshi.php">教师管理</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="am_news2.php?type=精彩回放">
					<i class="icon-reorder"></i> 
					<span class="title">精彩回放管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addnews2.php?type=精彩回放">添加精彩回放</a></li>
						<li><a href="am_news2.php?type=精彩回放">精彩回放列表</a></li>
					</ul>
				</li>
				    <li class="has-sub ">
					<a href="am_fangjian.php">
					<i class="icon-home"></i> 
					<span class="title">房间管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
                        <li ><a href="am_fangjian.php">房间列表</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="am_zhuanjia.php">
					<i class="icon-group"></i> 
					<span class="title">专家管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addzhuanjia.php">添加专家</a></li>
						<li><a href="am_zhuanjia.php">专家列表</a></li>
					</ul>
				</li>
                <li class="has-sub ">
					<a href="am_jiaren.php">
					<i class="icon-github-alt"></i> 
					<span class="title">假人管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
                <li ><a href="am_jiaren.php">假人列表</a></li>
              		 	 <li > <a href="am_jiahua.php">假话列表</a></li>
                 		 <li > <a href="addjiahua.php">发布假话</a></li>
					</ul>
				</li>
				
				   <li class="has-sub ">
					<a href="am_wenda.php">
					<i class="icon-folder-close"></i> 
					<span class="title">问答管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addwenda.php">发布问答</a></li>
						<li><a href="am_wenda.php">问答列表</a></li>
					</ul>
				</li>
			    <li class="has-sub">
					<a href="am_mingan.php">
					<i class="icon-comments"></i> 
					<span class="title">监控预警管理</span>
					<span class="arrow"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="am_news2.php?type=文件共享">
					<i class="icon-folder-close"></i> 
					<span class="title">文件共享管理</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="addnews2.php?type=文件共享">添加文件共享</a></li>
						<li><a href="am_news2.php?type=文件共享">文件共享列表</a></li>
					</ul>
				</li>
				<?php }?>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>