<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title>
	后台管理登陆
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" /><meta name="description" /><meta name="author" /><link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="../assets/css/metro.css" rel="stylesheet" /><link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet" /><link href="../assets/css/style.css" rel="stylesheet" /><link href="../assets/css/style_responsive.css" rel="stylesheet" /><link id="style_color" href="../assets/css/style_default.css" rel="stylesheet" /><link rel="stylesheet" type="text/css" href="../assets/uniform/css/uniform.default.css" /></head>
<body class="login">
    <!-- BEGIN LOGO -->
    <div class="logo" style="margin-top: 130px;">
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        <form class="form-vertical login-form" action="action.php?type=login" method="post">
            <div class="logo" style="margin-top: 0px; margin-bottom: 30px;">
                <img src="/assets/img/login_bg.png" alt="方舟财经" />
            </div>
            <div class="alert alert-error hide">
                <button class="close" data-dismiss="alert"></button>
                <span>请输入用户名和密码!</span>
            </div>
            <div class="control-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">用户名</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="icon-user"></i>
                        <input class="m-wrap placeholder-no-fix" type="text" placeholder="用户名" name="username" />
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label visible-ie8 visible-ie9">密  码</label>
                <div class="controls">
                    <div class="input-icon left">
                        <i class="icon-lock"></i>
                        <input class="m-wrap placeholder-no-fix" type="password" placeholder="密  码" name="password" />
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <label class="checkbox">
                    <input type="checkbox" name="remember" value="1" />
                    记住密码
                </label>
                <button type="submit" class="btn green pull-right">
                    登录 <i class="m-icon-swapright m-icon-white"></i>
                </button>
            </div>
        </form>
        <!-- END LOGIN FORM -->

    </div>
    <!-- END LOGIN -->
    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        2014 &copy;<?=$_SERVER['HTTP_HOST']?>.
    </div>
    <!-- END COPYRIGHT -->
    <!-- BEGIN JAVASCRIPTS -->
    <script src="../assets/js/jquery-1.8.3.min.js"></script>
    <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/uniform/jquery.uniform.min.js"></script>
    <script src="../assets/js/jquery.blockui.js"></script>
    <script type="text/javascript" src="../assets/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../assets/js/app.js"></script>
    <script>
        jQuery(document).ready(function () {
            App.initLogin();
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
</html>
