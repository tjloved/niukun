<?php

//1.记录中奖信息 2.记录中奖号码 3.记录中奖时间 4.记录抽奖次数 5.记录中奖ip
include("sys/conn2.php");
include("sys/mysql.class.php");

$winRecords = $res->fn_sql("select * from win_record where wr_record != 5
and wr_record != 11 limit 10");

if($_GET[fid]){
    $fid=$_GET[fid];
}else{
    $q_f=$res->fn_select("select * from fangjianlist $sqlxs");
    $fid=$q_f[fid];
}
$winLists=$res->fn_select("select * from win_list where fid='$fid' ");

$prize_arr = array( 
    '0'     =>  array('id'=>1,'prize'=>$winLists['wl_name1'],'v'=>$winLists['wl_gl1']), 
    '1'     =>  array('id'=>2,'prize'=>$winLists['wl_name2'],'v'=>$winLists['wl_gl2']), 
    '2'     =>  array('id'=>3,'prize'=>$winLists['wl_name3'],'v'=>$winLists['wl_gl3']), 
    '3'     =>  array('id'=>4,'prize'=>$winLists['wl_name4'],'v'=>$winLists['wl_gl4']), 
    '4'     =>  array('id'=>5,'prize'=>$winLists['wl_name5'],'v'=>$winLists['wl_gl5']),
    '5'     =>  array('id'=>6,'prize'=>$winLists['wl_name6'],'v'=>$winLists['wl_gl6']),
    '6'     =>  array('id'=>7,'prize'=>$winLists['wl_name7'],'v'=>$winLists['wl_gl7']),
    '7'     =>  array('id'=>8,'prize'=>$winLists['wl_name8'],'v'=>$winLists['wl_gl8']),
    '8'     =>  array('id'=>9,'prize'=>$winLists['wl_name9'],'v'=>$winLists['wl_gl9']),
    '9'     =>  array('id'=>10,'prize'=>$winLists['wl_name10'],'v'=>$winLists['wl_gl10']),
    '10'    =>  array('id'=>11,'prize'=>$winLists['wl_name11'],'v'=>$winLists['wl_gl11']),
    '11'    =>  array('id'=>12,'prize'=>$winLists['wl_name12'],'v'=>$winLists['wl_gl12'])
);

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <script src="choujiang/jquery-1.js"></script>
    <script src="choujiang/layer.js"></script><link style="" id="layui_layer_skinlayercss" href="choujiang/layer.css" rel="stylesheet">
    <script type="text/javascript" src="choujiang/scroll.js"></script>
    <script type="text/javascript" src="choujiang/md5.js"></script>
    <link href="choujiang/choujiang.css" rel="stylesheet">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.list_lcj:even').addClass('lieven');
        })
        $(function () {
            $("div.list_lcj").myScroll({
                speed: 80,
                rowHeight: 30
            });
        });
    </script>
</head>
<body>
    <div id="divChouJiangPage" style="display:block">
        <div class="dowebok">
            <div class="rotary">
                <img class="hand" src="choujiang/z.png" alt="">
            </div>
            <div class="list">
                <h4>中奖名单</h4>
                <div class="list_lcj lieven">
                    <ul style="margin-top: -24px;">
                        <?php while($winRecord = mysql_fetch_array($winRecords)){ ?>
                        <li>
                            <span><?php echo substr_replace($winRecord['wr_phone'],'****',3,4); ?></span>
                            <span><?php echo $prize_arr[$winRecord['wr_record']-1]['prize']; ?></span>
                        </li>
                        <?php } ?>            
                    </ul>
                </div>

                <div class="wenxin">
                    <p>
                        温馨提醒：
                    </p>
                    <p>
                        <em>每个手机号每天只限抽取3次，间隔一小时，中奖随机</em>
                    </p>
                    <p>
                        <em>奖品发放时间为第二个工作日中午12点前</em>
                    </p>
                    <p>
                        <em>中奖后发送手机号和中奖截图给高级助理</em>
                    </p>
                    <p>
                        <em>请保持手机畅通，方便工作人员核实信息发放奖品</em>
                    </p>
                </div>
            </div>
        </div>
        <div class="tzck" style="display: none;">
            <ul>
                <form id="myForm" method="post" autocomplete="off">
                    <span class="tzck_close">X</span>
                    <h1>为领奖时方便，请输入手机号码</h1>
                    <li><input name="phone" class="phone" placeholder="请输入您的手机号码" id="usertel" type="text"></li>
                    <li>
                        <input name="code" class="code" placeholder="输入验证码" id="validate" type="text">
                        <input name="button" value="获取验证码" class="button" id="sendmsg_btn" onclick="sendmsg()" type="button">
                    </li>
                    <li><a id="sub" class="zc-btn" href="javascript:void(0);" onclick="writeUserInfo()">开始抽奖</a></li>
                    <li><p>请正确填写，方便工作人员与您联系兑奖</p>
                    <input id="txtStatus" value="0" type="hidden">
                    <input id="txtOddsValue" value="0" type="hidden">
                    </li>
                </form>
            </ul>
        </div>
    </div>
    <div class="zjl" style="display:none" id="divChouJiangSuccess">
        <div class="zjla_title">
            <h1 id="hChouJiangTitle">恭喜您</h1>
        </div>
        <div class="zjla_conter">
            <span id="hjiangpingname">
                <h1>获得佳能单反相机一部！</h1>
            </span>
            <p>中奖后发送<em>手机号和中奖截图给高级助理</em></p>
            <p>请保持手机畅通，方便工作人员核实信息发放奖品</p>

            <span style="display:none" id="spShowStatus">0</span>
            <div id="choujiangshare" style="">

                <ul id="">
                    <li><a>分享到：</a></li>
                    <li>
                        <a href="javascript:void(0);" tag="qqmb" title="分享至腾讯微博" target="_blank">
                            <img alt="" src="choujiang/qqmb.gif">

                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" tag="sina" title="分享至新浪微博" target="_blank">
                            <img alt="" src="choujiang/sinaminiblog.gif">

                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" tag="qzone" title="分享至QQ空间" target="_blank">
                            <img alt="" src="choujiang/qzone.gif">

                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" tag="qqim" title="分享至QQ好友" target="_blank">
                            <img alt="" src="choujiang/qqim.gif">

                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('.tzck_close').click(function () {
                $('.tzck').hide();//关闭注册
            });
            var $hand = $('.hand');

            $hand.click(function () {
                var status = $("#txtStatus").val();

                if (status == 0) {
                    $('.tzck').show();//点击显示注册
                }
                else {
                    $.post('action.php?type=choujiang', {}, function (data) {
                        if(data.status == 'success'){
                            chouJiang(parseInt(data.rid));
                        }else{
                            alert(data.info);
                        }
                    }, 'json');
                    $("#txtStatus").val(0);
                }
            });


            function chouJiang(data) {
                switch (data) {
                    case 1:
                        rotateFunc(1, 16, '恭喜您抽中话费50元');
                        break;
                    case 2:
                        rotateFunc(2, 47, '恭喜您抽中体验一周黄金马甲');
                        break;
                    case 3:
                        rotateFunc(3, 76, '恭喜您抽中话费20元');
                        break;
                    case 4:
                        rotateFunc(4, 106, '恭喜您抽中苹果6s手机一部');
                        break;
                    case 5:
                        rotateFunc(5, 135, '好遗憾，未中奖。');
                        break;
                    case 6:
                        rotateFunc(6, 164, '恭喜您抽中话费20元');
                        break;
                    case 7:
                        rotateFunc(7, 193, '恭喜您抽中马尔代夫两日游');
                        break;
                    case 8:
                        rotateFunc(8, 223, '恭喜您抽中科沃斯扫地机');
                        break;
                    case 9:
                        rotateFunc(9, 252, '恭喜您抽中话费50元');
                        break;
                    case 10:
                        rotateFunc(10, 284, '恭喜您抽中佳能单反相机一部');
                        break;
                    case 11:
                        rotateFunc(11, 314, '好遗憾，未中奖。');
                        break;
                    case 12:
                        rotateFunc(12, 345, '恭喜您抽中马尔代夫两日游');
                        break;
                }
            }
            function rotateFunc(awards, angle, text) {
                $hand.stopRotate();
                $hand.rotate({
                    angle: 0,
                    duration: 5000,
                    animateTo: angle + 1440,
                    callback: function () {
                        $("#divChouJiangSuccess").show();
                        $("#divChouJiangPage").hide();
                        if (awards == 5 || awards == 11) {
                            var str = "<h1>分享后，再来一次！</h1>";
                            $("#hjiangpingname").html(str);
                            $("#hChouJiangTitle").html("很遗憾");
                            $("#spShowStatus").html("0");
                        }
                        else {
                            var str = "<h1>" + text + "</h1><p>中奖后发送<em>手机号和中奖截图给高级助理</em></p><p>请保持手机畅通，方便工作人员核实信息发放奖品</p>";
                            $("#hjiangpingname").html(str);
                            $("#hChouJiangTitle").html("恭喜你");
                            $("#spShowStatus").html("1");
                        }
                    }
                });
            };
        });
    </script>
    <script src="choujiang/jquery.js"></script>
    <script type="text/javascript">
    var mailreg = /^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.){1,4}[a-z]{2,3}$/;
    var passreg = /^[a-zA-Z0-9_]{6,16}$/;
    var telreg = /^1[3|4|5|7|8][0-9]\d{8}$/;
    var nickreg = /[^\u4e00-\u9fa50-9a-zA-Z]/ig;
    var verreg = /^[0-9]{6}$/;
    function ShowError(str) {
        $('#ErrTip2').text(str).show();
        layer.alert(str);
    }
    var SmsCheckCode;
    function sendmsg() {
        var tel = $("#usertel");
        var v = tel.val();
        if (v == "") {
            ShowError('手机号码不能为空！');
            return false;
        }else if (!new RegExp(telreg).test(v)) {
            ShowError('手机号码格式不正确（6~16位）！');
            $('#usertel').focus();
            return false;
        }

        $.post('duanxin/demo.php', {telephone:v}, function (data) {
            if(data.starts == 'success'){
                timeCount=setInterval("waitMsg()",1000);
                alert(data.info);
            }
        }, 'json');
    }
    WAIT = 60;
    function waitMsg() {
        $('#sendmsg_btn').val(WAIT + '秒后重新发送');
        $('#sendmsg_btn').attr('disabled', 'disabled');
        WAIT--;
        if (WAIT == 0) {
            clearInterval(timeCount);
            WAIT = 60;
            $('#sendmsg_btn').val('获取验证码');
            $('#sendmsg_btn').removeAttr('disabled');
        }
    }
    function writeUserInfo() {
        var tel = $("#usertel").val();
        var verifyCode = $("#validate").val();
        $.post('action.php?type=jilvsj', {telephone:tel,verifyCode:verifyCode },function(rep){
            if (rep.status == 0) {
                hasClick = false;
                layer.alert(rep.msg);
            } else {
                $('.tzck').hide();
                //$("#txtStatus").val(rep.status);
                //$("#txtOddsValue").val(rep.msg);
                $('.tzck').hide();
                $("#txtStatus").val("ok");
                $("#txtOddsValue").val("begin choujiang");
            }
        }, 'json');
    }

    /*分享*/
    $("#choujiangshare a").click(function () {
        //alert()
        var str = $("#spShowStatus").html();
        var ftitle = (str == "0"?"马尔代夫双人游等着你哟！": $("#hjiangpingname").html()) + '！ 牛叉网官网直播室,现货投资,原油沥青,采金_牛叉网官网';
        var tag = $(this).attr("tag");
        var tagUrl = "";
        if (tag == "sina") {
            /*新浪微博*/
            tagUrl = "http://service.weibo.com/share/share.php";
            tagUrl += "?pic=http://www.niuchaa.com/fenxiang/images/fenxiang04.jpg";
            tagUrl += "&title=" + encodeURIComponent(ftitle);
        }
        else if (tag == "weixin") {
            /*微信*/
            tagUrl = "";
        }
        else if (tag == "qzone") {
            /*QQ空间*/
            tagUrl = "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey";
            tagUrl += "?summary=";
            tagUrl += "&pics=http://www.niuchaa.com/fenxiang/images/fenxiang04.jpg";
            tagUrl += "&title=" + encodeURIComponent(ftitle);
            tagUrl += "&desc=" + encodeURIComponent(ftitle);
        }
        else if (tag == "qqmb") {
            /*腾讯微博*/
            tagUrl = "http://share.v.t.qq.com/index.php";
            tagUrl += "?c=share&a=index";
            tagUrl += "&pic=http://www.niuchaa.com/fenxiang/images/fenxiang04.jpg";
            tagUrl += "&title=" + encodeURIComponent(ftitle);
        }
        else if (tag == "weixinp") {
            /*微信朋友圈*/
        }
        else if (tag == "qqim") {
            /*QQ好友*/
            tagUrl = "http://connect.qq.com/widget/shareqq/index.html";
            tagUrl += "?summary=";
            tagUrl += "&pics=http://www.niuchaa.com/fenxiang/images/fenxiang04.jpg";
            tagUrl += "&title=" + encodeURIComponent(ftitle);
            tagUrl += "&desc=" + encodeURIComponent(ftitle);
        }
        var furl = 'http://vip.niuchaa.com'
        tagUrl += "&url=" + encodeURIComponent(furl);
        $(this).attr("href", tagUrl);
    });
    </script>
</body>
</html>
