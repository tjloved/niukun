;$(function () {
	var icons,facesStr,colorStr,FAYAN_LIMIT,uploadThumb;
	icons = [
		{key:0, value:'/鼓掌', type:1},
		{key:1, value:'/跳', type:1},
		{key:2, value:'/kiss', type:1},
		{key:3, value:'/来电', type:1},
		{key:4, value:'/贱笑', type:1},
		{key:5, value:'/陶醉', type:1},
		{key:6, value:'/兴奋', type:1},
		{key:7, value:'/鄙视', type:1},
		{key:8, value:'/得意', type:1},
		{key:9, value:'/偷笑', type:1},
		{key:10, value:'/挖鼻孔', type:1},
		{key:11, value:'/衰', type:1},
		{key:12, value:'/流汗', type:1},
		{key:13, value:'/伤心', type:1},
		{key:14, value:'/鬼脸', type:1},
		{key:15, value:'/狂笑', type:1},
		{key:16, value:'/发呆', type:1},
		{key:17, value:'/害羞', type:1},
		{key:18, value:'/可怜', type:1},
		{key:19, value:'/气愤', type:1},
		{key:20, value:'/惊吓', type:1},
		{key:21, value:'/困了', type:1},
		{key:22, value:'/再见', type:1},
		{key:23, value:'/感动', type:1},
		{key:24, value:'/晕', type:1},
		{key:25, value:'/可爱', type:1},
		{key:26, value:'/潜水', type:1},
		{key:27, value:'/强', type:1},
		{key:28, value:'/囧', type:1},
		{key:29, value:'/窃笑', type:1},
		{key:30, value:'/疑问', type:1},
		{key:31, value:'/装逼', type:1},
		{key:32, value:'/抱歉', type:1},
		{key:33, value:'/鼻血', type:1},
		{key:34, value:'/睡觉', type:1},
		{key:35, value:'/委屈', type:1},
		{key:36, value:'/笑哈哈', type:1},
		{key:37, value:'/贱贱地笑', type:1},
		{key:38, value:'/被电', type:1},
		{key:39, value:'/转发', type:1},
		{key:40, value:'/求关注', type:1},
		{key:41, value:'/路过这儿', type:1},
		{key:42, value:'/好激动', type:1},
		{key:43, value:'/招财', type:1},
		{key:44, value:'/加油啦', type:1},
		{key:45, value:'/转转', type:1},
		{key:46, value:'/围观', type:1},
		{key:47, value:'/推撞', type:1},
		{key:48, value:'/来嘛', type:1},
		{key:49, value:'/啦啦啦', type:1},
		{key:50, value:'/切克闹', type:1},
		{key:51, value:'/给力', type:1},
		{key:52, value:'/威武', type:1},
		{key:53, value:'/流血', type:1},
		{key:54, value:'/顶一个', type:2},
		{key:55, value:'/赞一个', type:2},
		{key:56, value:'/掌声', type:2},
		{key:57, value:'/鲜花', type:2},
		{key:58, value:'/发红包', type:2},
		{key:59, value:'/抢红包', type:2}
	];
	FAYAN_LIMIT=5;/*游客发言时间*/
	facesStr = '';
	colorStr = '';
	$.each(icons, function (index, value) {
		var imgStr;
		imgStr = '<img data-value="'+value.value+'" src="./images/face/'+value.key+'.gif" alt="'+value.value+'">';
		if(1 == value.type){
			facesStr += imgStr;
		}
		if(2 == value.type){
			colorStr += imgStr;
		}
	});

	$('#main .main_r_c_p_func .faces .icons').html(facesStr);
	$('#main .main_r_c_p_func .color .icons').html(colorStr);
	$('#facesBtn, #colorBtn').parent('.fline').hover(function () {
		$(this).find('.icons').removeClass('hide');
	}, function () {
		$(this).find('.icons').addClass('hide');
	});
	$('#facesBtn, #colorBtn').parent().find('.icons img').click(function () {
		var $this, $textareaMessage, message;
		$this = $(this);
		$textareaMessage = $('textarea[name=content]');
		message = $textareaMessage.val();
		message += $this.data('value');
		$textareaMessage.val(message);
		$('#icons .color').addClass('hidden');
	});

	if($('#downScroll').data('bool')){
		$('#main .main_r_c_box').animate( { scrollTop: $('#main .main_r_c_box')[0].scrollHeight } ,1000 );
	}
	if(1 == ADMINID || 2 == ADMINID){
		setInterval('$.ajaxFun()', 1000);
	}

	$('.myfancybox').fancybox();


	swfobject.embedSWF("../mp3/sound.swf", "sound", "1", "1", "9.0.0", "../mp3/expressInstall.swf", {}, {wmode:'transparent'}, {});

	$.extend({
		initFrame: function () {
			var mainHeight, mainWidth, rightWidth, videoHeight, titleHeight, mainFormWidth, mainFormButtonWidth, mainChatTitleHeight, mainChatPanelHeight, mainBoxHeight;
			mainHeight = $(window).height() - $('#top').outerHeight(true);
			$('#main').height(mainHeight);
			windowWidth = $(window).width();
			windowWidth = windowWidth>1200?windowWidth:1200;
			$('#main').width(windowWidth);
			rightWidth = windowWidth - $('#main .main_left').outerWidth(true);
			rightHalfWidth = rightWidth/2;
			$('#main .main_r_video, #main .main_r_chat').width(rightHalfWidth);
			videoHeight = rightHalfWidth * 0.65;
			titleHeight = $('#main .main_r_v_title').outerHeight(true);
			$('#main .main_r_v_flash').height(videoHeight - titleHeight);
			$('#main .main_r_v_image').height(mainHeight - videoHeight);
			mainFormWidth = $('#main .main_r_c_p_form').outerWidth(true);
			mainFormButtonWidth = $('#main .main_r_c_p_form .button').outerWidth(true);
			$('#main .main_r_c_p_form .input').width(mainFormWidth - mainFormButtonWidth -2);
			mainChatTitleHeight = $('#main .main_r_c_title').outerHeight(true);
			mainChatPanelHeight = $('#main .main_r_c_panel').outerHeight(true);
			$('#main .main_r_c_box').height(mainHeight - mainChatTitleHeight - mainChatPanelHeight -55);
			//控制在线
			mainBoxHeight = $('#main .main_l_box').outerHeight(true);
			$('#main .main_l_line').height(mainHeight - mainBoxHeight);

		},
		onlineAct: function (obj) {
			var $obj;
			$obj = $(obj);
			
			if($obj.parent().is('.main_l_l_b_height')){
				$(obj).parent().removeClass('main_l_l_b_height');
			}else{
				$(obj).parent().addClass('main_l_l_b_height')
			}
		},
		addMsg: function () {
			var content, data, tomid, toname;
			//游客发言时间  没有生效
			if(ADMINID==14 && FAYAN_LIMIT>0 && FAYAN_LIMIT!=5){
				$("#ts").click();
				return false;
			}
			content = $('textarea[name=content]').val();
			content = content.replace(/<script.*?>.*?<\/script>/ig, '发现敏感词');  
			if(!content) return false;
			tomid = $('input[name=tomid]').val();
			toname = $('input[name=toname]').val();
			data = {
				content:content,
				username:USERNAME,
				mid:MID,
				tomid:tomid,
				tousername:toname
			}

			$.post('action.php?type=addliaotian', data, function(data) {
					if(data == 'jinyan'){
						alert('对不起，您当前游客身份不允许发言，请先注册会员，免费领取马甲和老师解答，发言不受限制！');
						return;
					}
					var loginObj, zoumadeng;
					zoumadeng = $('#runingLigt').data('bool') ? 1 : 0;
					sayObj = {
						'type': 'say',
						'uid': MID,
						'umz': USERNAME,
						'tid': tomid,
						'tmz': toname,
						'aid': ADMINID,
						'zmd': zoumadeng,
						'rid': FID,
						'content': content
					}
					ws.send(JSON.stringify(sayObj));
			}, 'json');
			$('textarea[name=content]').val('');
		},
		sayTo: function (obj) {
			var uname, uid;
			uname = $(obj).parents('li').data('uname');
			uid = $(obj).parents('li').data('uid');
			$('#toUname').text(uname);
			$('input[name=tomid]').val(uid);
			$('input[name=toname]').val(uname);
			$('#toall').show();
		},
		pbThis: function pb(obj){
			if(!window.confirm("确认屏蔽此人？"))
			{
			 return false;
			}
			  if(ADMINID==1 || ADMINID==2 ){
				 $.ajax({
					 url: "action.php?type=pingbi",  
					 type: "POST",
					 async: true,
					 data:{mid:$(obj).parents('li').data('uid')},
					 error: function(){  
					 },  
					 success: function(data){//如果调用php成功  
						if(data=="true"){ 
							 alert("屏蔽成功,暗箱操作,用户发言只有自己可见！");
						}else{
							  alert("屏蔽失败");
						}
					 }
				 }); 
				}else{
				  notice('您不具有此权限');
				}
			},
		fThisip: function fip(obj){
			if(!window.confirm("确认封掉此人IP？"))
			{
			 return false;
			}
			  if(ADMINID==1 || ADMINID==2 ){
				 $.ajax({
					 url: "action.php?type=ipban",  
					 type: "POST",
					 async: true,
					 data:{mid:$(obj).parents('li').data('uid')},
					 error: function(){  
					 },  
					 success: function(data){//如果调用php成功  
						if(data=="true"){ 
							var dataObj = {
								'type': 'force',
								'uid': MID,
								'rid': FID
							};
							 alert("封IP成功,强制刷新用户页面");
							ws.send(JSON.stringify(dataObj));
						}else{
							  alert("屏蔽失败");
						}
					 }
				 });  
					}else{
				  notice('您不具有此权限');
				}
			},
		sayAll: function () {
			$('#toUname').text('所有人');
			$('input[name=tomid]').val('all');
			$('input[name=toname]').val('所有人');
			$('#toall').hide();
		},
		lookBig: function (obj) {
			var imgsrc;
			imgsrc = $(obj).attr('src');
			$('#look').removeClass('hide').find('.imgshow').html('<img width="100%" height="100%" src="'+imgsrc+'" alt="" />');
		},
		closePanel: function (obj) {
			$(obj).parent().addClass('hide');
		},
		playMp3: function (music) {
			var sound = swfobject.getObjectById("sound");
			if (sound) {
				sound.SetVariable("f", music);
				sound.GotoFrame(1);
			}
		},
		loginFunc: function () {
			var success=true;
			$("#login_form input").each(function(){
				if(!$(this).val()){
					errormessage(this,$(this).attr('placeholder'));
					success=false;
					return false;
				}
				if($(this).hasClass('error')){
					$(this).blur();
					success = false;
					return false;
				}
			});
			if(!success){
				return false;
			}
			$('#login_form .ErrBox').html('登录中...');
			$('#login_form .ErrBox').show();
			var username=$("#login_form input[name='account']").val();
			var password=$("#login_form input[name='password']").val();
			var validate=$("#login_form input[name='vcode']").val();//修改
			$.post('action.php?type=check_account', {username:username,password:password,validate:validate}, function (data) {
				if(data=='success'){
					window.location.reload();
				}else if(data=='noPassword'){
					$('#login_form .ErrBox').html('密码错误...');
				}else if(data=='noValidate'){
					$('#login_form .ErrBox').html('验证码错误..');
				}else{
					$('#login_form .ErrBox').html('登录失败...');
				}
			});
		},
		ajaxFun: function () {
			$.get('action.php?type=sh_all_list', {'fid': FID}, function (data) {
				$('#main .main_r_c_box').html(data);
			});
		},

		
		fabuall: function(obj) {
			var id, $this, $plist, username, tomid, tousername, adminid, content;
			$this = $(obj);
			$plist = $this.parents('.main_r_c_b_list');
			id = $plist.data('id');
			username = $plist.data('umz');
			tomid = $plist.data('tomid');
			tomid = tomid != 0 && tomid ?tomid:'all';
			tousername = $plist.data('tmz');
			adminid = $plist.data('aid');
			content = $plist.find('.text').html();
			
			if(id){
				$.post('action.php?type=fabuall', {lid:id}, function (data) {
					if(data == 'true'){
						var sayObj;
							sayObj = {
							'type': 'shenhe',
							'uid': MID,
							'lid': id,
							'umz': username,
							'tid': tomid,
							'tmz': tousername,
							'aid': adminid,
							'zmd': 0,
							'rid': FID,
							'content': content
						}
						ws.send(JSON.stringify(sayObj));
					}
				});
			}
		},

		delall: function(obj){
			var id, $this;
			$this = $(obj);
			id = $this.parents('.main_r_c_b_list').data('id');
			$.post('action.php?type=delall', {lid:id}, function (data) {
				if(data == 'true'){
					$this.parents('.liaotian').remove();
					var sayObj;
						sayObj = {
							'type': 'chexiao',
							'lid': id,
							'rid': FID
						}
						ws.send(JSON.stringify(sayObj));
					alert('删除成功');
				}
			});
		},
		pubClear: function() {
			$('#main .main_r_c_box').html('');
		},
		selectIt: function (obj){
			if($(obj).data('bool')){
				$(obj).removeClass('active');
				$(obj).data('bool', 0);
			}else{
				$(obj).addClass('active');
				$(obj).data('bool', 1);
			}
		},

		zcjgShow: function () {
			$('#zcjg').show();
		}
		
	});
	$.initFrame();
	$(window).resize(function () {
		$.initFrame();
	});

	$('#theme img').click(function () {
		var $this;
		$this = $(this);
		$('body').css('backgroundImage', 'url('+$this.data('bg')+')');
	});

	$('#main .main_l_l_body, #main .main_r_c_box').niceScroll();
	uploadThumb = new plupload.Uploader({
		browse_button: 'uploadImg',
		url: 'sys/upload_xs.php',
		multi_selection: false,
		multipart_params: {'mid':MID}
	});
	uploadThumb.init();
	uploadThumb.bind('FilesAdded', function(upfile, files) {
		upfile.start();
	});
	uploadThumb.bind('FileUploaded', function(upfile, files, res) {
		var data =  JSON.parse(res.response);
		console.log(data);
		var content, data, tomid, toname;
		//游客发言时间
		if(ADMINID==14 && FAYAN_LIMIT>0 && FAYAN_LIMIT!=2){
			$("#ts").click();
			return false;
		}
		content = data.content;
		if(!content) return false;
		tomid = $('input[name=tomid]').val();
		toname = $('input[name=toname]').val();
		data = {
			content:content,
			username:USERNAME,
			mid:MID,
			tomid:tomid,
			tousername:toname
		}

		var loginObj, zoumadeng;
		zoumadeng = 0;
		sayObj = {
			'type': 'say',
			'uid': MID,
			'umz': USERNAME,
			'tid': tomid,
			'tmz': toname,
			'aid': ADMINID,
			'zmd': zoumadeng,
			'rid': FID,
			'content': content
		}
		ws.send(JSON.stringify(sayObj));	
	});
	uploadThumb.bind('Error', function(upfile, err) {
		alert(err.code +'???'+ err.message);
	});
});
