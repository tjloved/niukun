if (typeof console == "undefined") {    this.console = { log: function (msg) {  } };}
WEB_SOCKET_SWF_LOCATION = "//cdn.bootcss.com/web-socket-js/1.0.0/WebSocketMain.swf";
WEB_SOCKET_DEBUG = false;
var ws, WEB_CONFIG;
WEB_CONFIG = {
	'URL' : 'ws://115.28.186.52:7272'
}
function iconReplace(str){
	var icon;
	icon = {
		0:'/鼓掌', 
		1: '/跳', 
		2: '/kiss',
		3: '/来电', 
		4: '/贱笑', 
		5: '/陶醉', 
		6: '/兴奋',
		7: '/鄙视',
		8: '/得意',
		9: '/偷笑', 
		10: '/挖鼻孔',
		11: '/衰',
		12: '/流汗',
		13: '/伤心', 
		14: '/鬼脸',
		15: '/狂笑',
		16: '/发呆',
		17: '/害羞',
		18: '/可怜',
		19: '/气愤',
		20: '/惊吓',
		21: '/困了',
		22: '/再见',
		23: '/感动',
		24: '/晕',
		25: '/可爱',
		26: '/潜水',
		27: '/强',
		28: '/囧',
		29: '/窃笑',
		30: '/疑问',
		31: '/装逼',
		32: '/抱歉',
		33: '/鼻血',
		34: '/睡觉',
		35: '/委屈',
		36: '/笑哈哈',
		37: '/贱贱地笑',
		38: '/被电',
		39: '/转发',
		40: '/求关注',
		41: '/路过这儿',
		42: '/好激动',
		43: '/招财',
		44: '/加油啦',
		45: '/转转',
		46: '/围观',
		47: '/推撞',
		48: '/来嘛',
		49: '/啦啦啦',
		50: '/切克闹',
		51: '/给力',
		52: '/威武',
		53: '/流血',
		54: '/顶一个', 
		55: '/赞一个',
		56: '/掌声',
		57: '/鲜花',
		58: '/发红包',
		59: '/抢红包'
	}
	$.each(icon, function (index, value) {
		str = str.replace(new RegExp(value, 'g'), '<img align="absmiddle" src="/images/face/'+index+'.gif" />');
	});
	return str;
}