function connect() {
	ws = new WebSocket(WEB_CONFIG.URL);
	ws.onopen = function () {
		var loginObj;
		loginObj = {
			'type': 'login',
			'umz': USERNAME,
			'uid': MID,
			'rid': FID,
			'aid': ADMINID
		}
		ws.send(JSON.stringify(loginObj));
	};
	ws.onmessage = function(e){
		var messageData;
		messageData = JSON.parse(e.data);
		switch(messageData.type){
			case 'login':
				var loginStr, clientCount, selfStr;
				loginStr = '';
				if(messageData.clients || typeof messageData.clients != 'undefined'){
					$.each(messageData.clients,function (index, value) {
						loginStr += '<li class="main_l_l_b_list main_l_l_b_height" data-uname="'+value.umz+'" data-uid="'+value.uid+'">';
						loginStr += '<a class="main_l_l_b_l_item" href="javascript:;" onclick="$.onlineAct(this);">';
						loginStr += '<span class="name">'+value.umz+'</span><span class="icon">';
						loginStr += '<img src="images/level/User'+value.aid+'.png"></span></a>';
						loginStr += '<div class="main_l_l_b_l_action">';
						loginStr += '<a class="say" href="javascript:;" onclick="$.sayTo(this)">';
						loginStr += '<span class="icon"></span><span class="word">对TA说</span></a>';
						if(1 == ADMINID || 2 == ADMINID){
							loginStr += '<a class="pb" href="javascript:;" onclick="$.pbThis(this);">';
							loginStr += '<span class="icon"></span><span class="word">屏蔽</span></a>';
							loginStr += '<a class="fip" href="javascript:;" onclick="$.fThisip(this);">';
							loginStr += '<span class="icon"></span><span class="word">封IP</span></a>';
						}
						loginStr += '</div></li>';
					});
					$('#main .main_l_l_body').append(loginStr);
				}
				
				clientCount = parseInt(messageData.count);

				selfStr = '';
				selfStr += '<li class="main_l_l_b_list main_l_l_b_height" data-uname="'+messageData.umz+'" data-uid="'+messageData.uid+'">';
				selfStr += '<a class="main_l_l_b_l_item" href="javascript:;" onclick="$.onlineAct(this);">';
				selfStr += '<span class="name">'+messageData.umz+'</span><span class="icon">';
				selfStr += '<img src="images/level/User'+messageData.aid+'.png"></span></a>';
				selfStr += '<div class="main_l_l_b_l_action">';
				selfStr += '<a class="say" href="javascript:;" onclick="$.sayTo(this)">';
				selfStr += '<span class="icon"></span><span class="word">对TA说</span></a>';
				if(1 == ADMINID || 2 == ADMINID){
					selfStr += '<a class="pb" href="javascript:;" onclick="$.pbThis(this);">';
					selfStr += '<span class="icon"></span><span class="word">屏蔽</span></a>';
					selfStr += '<a class="fip" href="javascript:;" onclick="$.fThisip(this);">';
					selfStr += '<span class="icon"></span><span class="word">封IP</span></a>';
					$('#totalCount').text(clientCount);
				}else{
					var pzcount, systList;
						pzcount = $('#totalCount').data('pzcount');
						systList = $('#main .main_l_l_b_list').length;
						$('#totalCount').text(pzcount+systList+clientCount);
				}
				selfStr += '</div></li>';
				$('#main .main_l_l_body').append(selfStr);
		//这里是进入房间 
				logStr="";
				logStr += '<li class="delu">'+messageData.umz+'加入房间</li>';
				$('#main .main_r_c_box').append(logStr);
			break;
			case 'logout':
				var logoutStr;//这里是退出房间
				logoutStr="";
				logoutStr += '<li class="tucu" data-uname="'+messageData.umz+'">'+messageData.umz+'已退出</li>';
				$('#main .main_r_c_box').append(logoutStr);
				$('#main .main_l_l_b_list[data-uid='+messageData.uid+']').remove();
				$('#totalCount').text(parseInt($('#totalCount').text()) );
			break;
			case 'say'://发言开始				
				//撤销
				if(messageData.lid){
					$('#main .main_r_c_b_list[id='+messageData.lid+']').remove();
				}
				
				if(messageData.tid == MID){
					$.playMp3('mp3/msg.mp3');
				}
				var contentStrXs = iconReplace(messageData.content);
				if(ADMINID == 1 || ADMINID == 2){
					sendContent = '<img src="'+messageData.picurl+'" alt="img" style="max-width:500px; max-height:600px" />';
				}
				var talkToStr, contentStr, style;
				style = '';
				talkToStr = '';
				if('all' != messageData.tid){
					talkToStr = '<span class="tsay">对 '+messageData.tmz+' 说</span>';
				}
				contentStr = '';
				contentStr += '<div class="main_r_c_b_list clearfix">';
				contentStr += '<div class="imgs" style="float:left;"><img src="images/level/User'+messageData.aid+'.png"></div>';
				contentStr += '<div style="float:left; width:95%;"><div style="width:100%; height:30px;">';
				contentStr += '<span class="from">'+messageData.umz+'</span>';
				contentStr += '<span class="time">'+messageData.time+'</span>';
				contentStr += talkToStr;
				if(1==messageData.aid || 2 == messageData.aid){
					style = 'style="color:red"';
				}
				contentStr += '</div><span class="qibao"><span class="text" '+style+'>'+contentStrXs+'</span></span></div><div class="clearfix"></div></div>';
				$('#main .main_r_c_box').append(contentStr);
				
				if(messageData.zmd){
					$('.quanping').html('<marquee loop="1" onmouseover="marquee_show()"><p>'+iconReplace(messageData.content)+'</p></marquee>');
					$('.quanping').show();
					$('.quanping marquee').trigger('start');
				}

				if($('#downScroll').data('bool')){
					$('#main .main_r_c_box').animate( { scrollTop: $('#main .main_r_c_box')[0].scrollHeight } ,1000 );
				}
			break;
			case 'handan':
				$.playMp3('mp3/5103.mp3');
				var handanStr;
				handanStr = '';
				handanStr += '<div class="main_r_c_b_list clearfix"><div class="handan">';
				handanStr += '<strong class="title">交易提示</strong>';
				handanStr += '<strong class="date">'+messageData.time+'</strong>';
				handanStr += '<strong class="wrap">'+messageData.did+' 价位：'+messageData.kcjia+'</span>';
				if(ADMINID <= 13){
					handanStr += '<a href="tixing.php?status=0" class="myfancybox fancybox.iframe">详情点击查看</a>';
				}
				handanStr += '</div></div>';
				$('#main .main_r_c_box').append(handanStr);
				if($('#downScroll').data('bool')){
					$('#main .main_r_c_box').animate( { scrollTop: $('#main .main_r_c_box')[0].scrollHeight } ,1000 );
				}
			break;
			case 'pingcang':
				$.playMp3('mp3/5103.mp3');
				var pingcangStr;
				pingcangStr = '';
				pingcangStr += '<div class="main_r_c_b_list clearfix"><div class="handan">';
				pingcangStr += '<strong class="title">交易提示</strong>';
				pingcangStr += '<strong class="date">'+messageData.time+'</strong>';
				pingcangStr += '<strong class="wrap">单号：'+messageData.did+'已平仓</span>';
				if(ADMINID <= 13){
					handanStr += '<a href="tixing.php?status=0" class="myfancybox fancybox.iframe">详情点击查看</a>';
				}
				pingcangStr += '</div></div>';
				$('#main .main_r_c_box').append(pingcangStr);
				if($('#downScroll').data('bool')){
					$('#main .main_r_c_box').animate( { scrollTop: $('#main .main_r_c_box')[0].scrollHeight } ,1000 );
				}
			break;

			case 'chexiao':
				$('#main .main_r_c_b_list[id='+messageData.lid+']').remove();
			break;

			case 'force'://强制刷新页面
				if(MID == messageData.uid){
					window.location.href='./index.php';
				}else if(!messageData.uid){
					window.location.href='./index.php';
				}
			break;
		}
	};
	ws.onclose = function() {
		console.log("连接关闭，定时重连");
		connect();
	};
	ws.onerror = function() {
		console.log("出现错误");
	};
}
