function connect() {
	ws = new WebSocket(WEB_CONFIG.URL);
	ws.onopen = function () {};
	ws.onmessage = function(e){};
	ws.onclose = function() {
		console.log("连接关闭，定时重连");
		connect();
	};
	ws.onerror = function() {
		console.log("出现错误");
	};
}