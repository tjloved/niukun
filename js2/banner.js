$(function () {
    var $cccil, $cccb, indexBanner, bannerTiming;
    $mainBS = $('.slider');
    $mainBSI = $('.slider .item');
    $mainBSH = $('.slider .href');
    $mainBSIL = $('.slider .item .line');
    $mainBSAL = $('.slider .actionleft');
    $mainBSAR = $('.slider .actionright');
    $.extend({
        'init' : function () {
            var width;
            width = $mainBSI.width();
            $mainBSI.css({'margin-left' : - width/2});
            $mainBSIL.eq(0).addClass('hover');
            $mainBSH.eq(0).css({'display': 'block', 'z-index': 1, 'opacity': 1});
        }
    });
    $.init();
    
    indexBanner = 0;
    var bannerPlus = function () {
        indexBanner++;
        if(indexBanner > 2){
            indexBanner = 0;
        }
        $mainBSIL.removeClass('hover');
        $mainBSIL.eq(indexBanner).addClass('hover');
        $mainBSH.css({'display': 'none', 'z-index': 0, 'opacity': 0});
        $mainBSH.eq(indexBanner).css({'display': 'block', 'z-index': 1}).stop().animate({'opacity': 1}, 1000);
        
    }
    var bannerMinus = function () {
        indexBanner--;
        if(indexBanner < 0){
            indexBanner = 3;
        }
        $mainBSIL.removeClass('hover');
        $mainBSIL.eq(indexBanner).addClass('hover');
        $mainBSH.css({'display': 'none', 'z-index': 0, 'opacity': 0});
        $mainBSH.eq(indexBanner).css({'display': 'block', 'z-index': 1}).stop().animate({'opacity': 1}, 1000);
    }
    bannerTiming = setInterval(bannerPlus, 3000);
    $mainBSIL.mouseover(function () {
        clearInterval(bannerTiming);
        var indexData = $(this).index();
        $mainBSIL.removeClass('hover');
        $mainBSIL.eq(indexData).addClass('hover');
        $mainBSH.css({'display': 'none', 'z-index': 0, 'opacity': 0});
        $mainBSH.eq(indexData).css({'display': 'block', 'z-index': 1}).stop().animate({'opacity': 1}, 1000);
        indexBanner = indexData;
    });
    $mainBS.mouseover(function () {
        clearInterval(bannerTiming);
    }).mouseout(function () {
        bannerTiming = setInterval(bannerPlus, 5000);
    });
    $mainBSAL.click(function () {
        bannerMinus();
    });
    $mainBSAR.click(function () {
        bannerPlus();
    });
});