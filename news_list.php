
<?php 
include_once 'sys/conn.php';
include_once 'sys/mysql.class.php';
?>
<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/common.min.css" rel="stylesheet" />
    <script src="js/jquery-1.8.2.min.js"></script>
    <link href="css/pagination.css" rel="stylesheet" />
       
    <style type="text/css">
        .list-content-wrap
        {
            width: 70%;
            margin: 0 auto;
        }

        .list-header
        {
            width: 100%;
        }

            .list-header li
            {
                float: left;
                font-weight: bold;
                padding: 3px 0 2px;
            }

                .list-header li span
                {
                    display: block;
                    padding-left: 6px;
                }

            .list-header .l-h-title
            {
                width: 80%;
                color: #333;
            }

            .list-header th
            {
                text-align: left;
            }

            .list-header .l-h-title span
            {
                padding-left: 8px;
            }

            .list-header .l-h-time
            {
                width: 18%;
            }

                .list-header .l-h-time span
                {
                    width: 18%;
                    padding-left: 10px;
                }

        .list-items
        {
            width: 100%;
            overflow: hidden;
            height: auto;
        }

            .list-items tr
            {
                border-bottom: 1px dashed #ccc;
            }

            .list-items td
            {
                padding: 8px 0 4px;
            }

            .list-items li span
            {
                display: block;
                padding-left: 6px;
            }

            .list-items .l-i-title
            {
                width: 80%;
            }

            .list-items .l-i-time
            {
                width: 18%;
            }

                .list-items .l-i-time span
                {
                    color: #868686;
                    padding-left: 10px;
                }
        .list-tbl
        {
            border-collapse: collapse; table-layout: fixed; width:100%;
        }
        .pager {
            margin-top: 10px;
            font-size: 80%;
        }
		
    </style>
</head>
<body>
    <div class="list-content-wrap">
        <div id="content-container">
	
            <div class="list-items" id="list-container">
                <table id="list-tbl" class="list-tbl">
				 <tr class="list-header l-header">
                        <th class="l-h-title"><span>标题</span></th>
                        <th class="l-h-time"><span>发布时间</span></th>
                  </tr>
               
    <?php 

if($_GET[page]){
	$page=$_GET[page];
}else{
	$page=1;
}
$qian=($page-1)*20;


	$q_news=$res->fn_sql("select * from newslist where typename='$_GET[typename]' order by nid desc limit $qian,20");
	while($news=mysql_fetch_array($q_news)){
	
	?> 
                <tr class="">
				
				<td class="l-i-title"><span>
                <a href="article.php?nid=<?=$news[nid]?>"><?=$news[title]?></a></span></td>
				<td class="l-i-time"><span><?=date('Y-m-d H:i:s',$news[time])?></span></td>
				
				</tr>

       <?php } ?>      
          
                </table>
            </div>
            <div id="pager" class="pager">
     <?php
$num=$res->fn_num("select * from newslist where typename='$_GET[typename]'");
$ye=(int)($num/20+1);
?>
<?php if($page==1){?>
            <span class="current prev">上页</span>
<?php }else {?>
   
     <a href="news_list.php?typename=<?=$_GET[typename]?>&page=<?=($page-1)?>" class="prev">上页</a>

<?php }?>

<?php if($page==$ye){?>
<span class="current next">下页</span>
<?php }else{?>
  <a href="news_list.php?typename=<?=$_GET[typename]?>&page=<?=($page+1)?>" class="next">下页</a>

<?php } ?>
          

           </div>
        </div>
    </div>

</div> 
</body>
</html>