<?php
include_once 'sys/conn.php';
include_once 'sys/mysql.class.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>喊单提醒</title>
    <link href="css/common.min.css" rel="stylesheet" />
    <link href="css/pagination.css" rel="stylesheet" />
    <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
    <style type="text/css">
 *{ color: #333; }
 .list-content-wrap { width: 98%; margin: 0 auto; overflow: hidden; }
 .list-tab { border-collapse: collapse; border: 1px solid #d4d4d4; table-layout: fixed; }
 .list-header { width: 100%; }
 .list-header th { font-weight: bold; padding: 6px 0 3px 3px; overflow: hidden; text-align: left; border: 1px solid #d4d4d4; }
 .list-header .l-h-no { width: 45px; }
 .list-header .l-h-type { width: 65px; }
 .list-header .l-h-position { width: 35px; }
 .list-header .l-h-pro { width: 65px; }
 .list-header .l-h-price { width: 85px; }
 .list-header .l-h-profit { width: 70px; }
 .list-header .l-h-remark { width: 90px; }
 .list-header .l-h-analysts { width: 50px; }
 .list-header .l-h-praise { width: 50px; text-align: center !important; }
 .list-header .l-h-time { width: 80px; }
 .list-tab td { padding: 5px 0 3px 3px; overflow: hidden; border: 1px solid #CCC; cursor: default;   white-space: normal; text-overflow: ellipsis; }
 .l-i-no { width: 45px; }
 .l-i-type { width: 65px; }
 .l-i-type span { color: #ff0606; }
 .l-i-position { width: 35px; }
 .l-i-pro, .l-i-pro-content { width: 65px; overflow: hidden; }
 .l-i-price { width: 85px; }
 .l-i-profit { width: 70px; }
 .l-i-remark { width: 100px; }
 .l-i-remark .remark { width: 97px; height: 23px; overflow: hidden; }
 .l-i-analysts { width: 50px; }
 .l-i-praise { width: 50px; }
 .l-i-praise .on { display: block; width: 18px; height: 22px; background-position: -281px -232px;
 float: left; }
 .l-i-praise .off { display: block; width: 21px; height: 22px; background-position: -252px -232px;
 float: left; }
 .l-i-praise .praise-num { width: 23px; float: left; height: 20px; text-align: right; padding: 4px 2px 0 0; color: #979797; }
 .l-i-time { width: 80px; }
 .list-items .l-i-time span { color: #868686; }
 .create-wrap { width: 98%; margin: 8px auto 0; border-top: 1px dashed #CCC; }
 .create-wrap h3 { margin: 0; padding: 6px 0 4px 5px; }
 .call-bill-wrap { width: 800px; background-color: #f6f6f6; padding: 6px 5px 4px; overflow: hidden; position: relative; }
 .call-bill-wrap .btn-wrap { position: absolute; width: 110px; right: 0; bottom: 8px; }
 .call-bill-wrap .ifsend { margin:0 0 8px; }
 .call-bill-wrap .ifsend input {  margin:0; }
 .call-bill { width: auto; }
 .call-bill .list-title1 { width: 55px; text-align: right; }
 .call-bill .list-title2 { width: 120px; text-align: right; }
 .call-bill .list-title3 { width: 115px; text-align: right; }
 .call-bill .list-item1 { width: 110px; text-align: left; }
 .call-bill .list-item2 { width: 110px; text-align: left; }
 .call-bill .list-item3 { width: 100px; text-align: left; }
 .call-bill .price-box { width: 80px; border: 1px solid #d3d3d3; padding: 5px 3px 4px 5px; }
 .call-bill .position-box { width: 65px; border: 0; outline: none; padding-left: 5px; }
 .call-bill select { margin: 0 !important; border: 1px solid #d3d3d3; height: 28px; border-radius: 2px; }
 .call-bill td { padding: 2px 0; }
 .call-bill .sel1 { width: 90px; }
 .open-stock { padding: 7px 12px 4px; font-weight: 700; font-size: 14px; cursor: pointer; margin-left: 0px; }
 .l-text { position: relative; border: 1px solid #d3d3d3; height: 20px; line-height: 20px; width: 88px; background: white; }
 .l-trigger, .l-trigger-hover, .l-trigger-pressed { cursor: pointer; position: absolute; top: 1px; right: 1px; width: 16px; height: 16px; overflow: hidden; }
 .l-trigger { background: #eeeeee url('http://img3.jiaoyi18.com/ligerui/controls/bg-trigger.gif') repeat-x left; }
 .l-trigger-hover { background: #fedc75 url('http://img3.jiaoyi18.com/ligerui/controls/bg-trigger-over.gif') repeat-x left; }
 .l-trigger-pressed { background: #fedc75 url('http://img3.jiaoyi18.com/ligerui/controls/bg-trigger-pressed.gif') repeat-x left; }
 .l-spinner-up .l-spinner-icon, .l-spinner-down .l-spinner-icon { overflow: hidden; position: absolute; left: 0px; top: 50%; margin-top: -3px; width: 100%; height: 6px; background: no-repeat center top; }
 .l-spinner-down-over, .l-spinner-up-over { background: #fedc75 url('http://img3.jiaoyi18.com/ligerui/controls/bg-trigger-over.gif') repeat-x left; }
 .l-spinner-up .l-spinner-icon { background-image: url(http://img3.jiaoyi18.com/ligerui/icon/icon-up.gif); }
 .l-spinner-down .l-spinner-icon { background-image: url(http://img3.jiaoyi18.com/ligerui/icon/icon-down.gif); }
 .l-spinner-up, .l-spinner-down { width: 100%; height: 50%; overflow: hidden; display: block; position: absolute; left: 0px; top: 0px; }
 .l-spinner-down { top: 50%; }
 .l-spinner-split { overflow: hidden; width: 13px; height: 1px; position: absolute; left: 1px; top: 50%; z-index: 10; background: #fff; }
 .ui-widget-header { border: 0px solid #4297d7; background:none; color: #ffffff; font-weight: bold; }
 .ui-daterangepicker-arrows input.ui-rangepicker-input { width: 100px;   padding-left: 10px; height: 20px; line-height: 20px; }
 .datapicker-wrap input { border: 1px solid #037DC6; outline: none; }
 .ui-daterangepickercontain { margin-top:15px; }
 .ui-daterangepicker .ranges {    padding: 0px 0px 40px 0;    }
 .ui-datepicker .ui-datepicker-header { background:#4C99C7; }
 .ui-corner-all {  border-bottom-right-radius:0px; border-bottom-left-radius:0px;  }
 .ui-timepicker-div .ui-corner-all { border-bottom-right-radius:5px; border-bottom-left-radius:5px;  }
 button.ui-corner-all { border-bottom-right-radius:5px; border-bottom-left-radius:5px; }
 .ui-datepicker table { background:#454545; padding:0px; color:#EFEFEF; }
 .ui-datepicker-other-month { background:#ffffff; }
 .ui-state-disabled, .ui-widget-content .ui-state-disabled { opacity: 1; filter: Alpha(Opacity=100); background-image: none; }
 .ui-datepicker td { border: 1px solid #CBCBCB; }
 .ui-state-default, .ui-widget-content .ui-state-default { border: 0px solid #c5dbec; background:#E8E8E8; color:#585858; }
 .ui-datepicker td { padding: 0px; }
 .ui-state-highlight, .ui-widget-content .ui-state-highlight { color:#D6FFFF; background:#4790BB; }
 .ui-datepicker td span ,.ui-datepicker td a{ text-align: center; }
 .ui-datepicker-other-month .ui-state-default{ color:#939393; background:#ffffff; font-weight:initial; }
 .ui-state-hover .ui-icon,.ui-widget-content .ui-state-hover, .ui-state-hover { border: 0px;   }
 .ui-daterangepicker-arrows input.ui-rangepicker-input { width: 90px;      margin: 0 2px 0 0px;     }
 .ui-daterangepicker-arrows { padding: 2px; width: 110px; }
 .ui-daterangepicker { background:#454545; }
 .ui-datepicker { width: 17em; padding:0px; }
 .ui-daterangepicker .range-start, .ui-daterangepicker .range-end {  margin-left: 0px;}
 .ui-datepicker table { margin: 0; }
 .ui-widget-header .ui-icon{ background-image:url(http://zhibo.niukun.net/jiantou.png); }
 .ui-icon-circle-triangle-w { background-position:5px 3px; }
 .ui-icon-circle-triangle-e { background-position:-10px 3px; }
 .ui-widget span,.ui-widget a{  font-family: 微软雅黑; }
 .ui-datepicker td a { height:26px; line-height:26px; padding:0px  .2em; }
 .ui-datepicker th { padding: .3em .3em; }
 .ui-daterangepicker button.btnDone { font-weight:initial;font-size:12px;padding:2px 10px; background:#C7C5C6; }
 .ui-datepicker-other-month { color: #939393; background: #ffffff; font-weight: initial; text-align:center; }
 .ui-datepicker .ui-datepicker-prev-hover { left: 0px; }
 .ui-datepicker .ui-datepicker-prev { left: 0px; }
 .ui-datepicker .ui-datepicker-prev-hover, .ui-datepicker .ui-datepicker-next-hover { top: 3px; }
 .ui-datepicker .ui-datepicker-prev, .ui-datepicker .ui-datepicker-next { top: 3px; }
 .ui-datepicker .ui-datepicker-next-hover { right: 0px; }
 .ui-datepicker .ui-datepicker-next { right: 0px; }
 .ui-datepicker-title span, .ui-datepicker-calendar span{ color:#EFEFEF; }
 .ui-timepicker-div { padding-left:10px; }
 .ui-timepicker-div dl dd { margin: 0px 10px 10px 55px; }
 #ui-datepicker-div { background:#454545; }
 .ui-timepicker-div dl, .ui-timepicker-div dt, .ui-timepicker-div dd { color:#EFEFEF; font-weight:600; }
 .ui-widget { font-family: 微软雅黑; }
 .ui-widget-content {  background: #141414; border:0px solid #141414;   }
 .ui-slider-horizontal { height: .5em; }
 .ui-slider .ui-slider-handle {     width: 10px; height: 18px; }
 .ui-timepicker-div .ui-state-default { background:url(http://img3.jiaoyi18.com/timePick.png) no-repeat -5px -14px; }
 .ui-slider-horizontal .ui-slider-handle { top: -.4em;     }
 .ui-timepicker-div .ui_tpicker_hour, .ui-timepicker-div .ui_tpicker_minute, .ui-timepicker-div .ui_tpicker_second {margin-top:8px; }
 #ui-datepicker-div .ui-datepicker-buttonpane .ui-datepicker-current { background:#B1E0FE; }
 #ui-datepicker-div .ui-datepicker-buttonpane .ui-datepicker-close { background:#E8E8E8;  opacity: .8; filter: Alpha(Opacity=80); }
    </style>
</head>
<body>
    <div class="list-content-wrap">
 <div class="nav-tabs call-bill-switch">
<?php 
if($_GET[status]=='0'){
?>
		<a href="javascript:void(0);" class="item1 active blue">即时喊单</a>
		<a href="tixing.php?status=1" class="item2">历史喊单</a>
<?php }else {?>
		<a href="tixing.php?status=0" class="item1 ">即时喊单</a>
		<a href="javascript:void(0);" class="item2 active blue">历史喊单</a>
<?php }?>
		</div>
		<?php if($_SESSION[leixing]=='教师'){?>
<div class="nav-tabs" style="width: 80px;position: relative;top: -34px;left: 180px;color: #fff;background-color: #f00;"><a href="admin_hm2/addhandan.php?status=1" style="color:#fff">发布喊单</a></div>
<?php }?>
 <div id="content-container">
 <div>
 <table class="list-tab">
   <tbody><tr class="list-header l-header">
  <th class="l-h-no"><span>单号</span></th>
  <th class="l-h-time"><span>开仓时间</span></th>
  <th class="l-h-type"><span>类型</span></th>
  <th class="l-h-position"><span>仓位</span></th>
  <th class="l-h-pro"><span>商品</span></th>
  <th class="l-h-price"><span>开仓价</span></th>
  <th class="l-h-price"><span>止损价</span></th>
  <th class="l-h-price"><span>止盈价</span></th>
  <th class="l-h-time"><span>平仓时间</span></th>
  <th class="l-h-price"><span>平仓价</span></th>
  <th class="l-h-profit"><span>获利点数</span></th>
  <th class="l-h-remark"><span>备注</span></th>
  <th class="l-h-analysts"><span>分析师</span></th>
    	<?php if($_SESSION[leixing]=='教师'){?>
						 <th class="l-h-analysts"><span>操作</span></th>
						<?php } ?>
  </tr>
			<?php
				if($_GET[page]){
					$page=$_GET[page];
				}else{
					$page=1;
				}
				$qian=($page-1)*20;
				$q_handan =$res->fn_sql("select t1.* ,t2.username from handanlist t1 left join userlist t2 on t1.fenxishi=t2.mid where status ='$_GET[status]' order by hid desc limit $qian,20");
				while($handan =mysql_fetch_array($q_handan)){
			
			?>
 <tr data-value="3560" class="">
 <td class="l-i-no"><span><?=$handan[did]?></span></td>
 <td class="l-i-time opentime"><span><?=date('m-d H:i',$handan[kctime])?></span></td>
 <td class="l-i-type"><span><?=$handan[leixing]?></span></td>
 <td class="l-i-position"><span><?=$handan[cangwei]?></span></td>
 <td class="l-i-pro"><div class="l-i-pro-content"><span><?=$handan[shangpin]?></span><div></div></div></td>
 <td class="l-i-price opening-price"><span><?=$handan[kcjia]?></span></td><td class="l-i-price"><span><?=$handan[zsjia]?></span></td>
 <td class="l-i-price"><span><?=$handan[zyjia]?></span></td>
 <td class="l-i-time closingtime"><span><?=date('m-d H:i',$handan[pctime])?></span></td>				 				<td class="l-i-price closing-price"><span><?=$handan[pcjia]?></span></td>
 <td class="l-i-profit"><span><?=$handan[hldian]?></span></td>
 <td class="l-i-remark"><div class="remark"><?=$handan[beizhu]?></div></td>
 <td class="l-i-analysts"><span><?=$handan[username]?></span></td>
				<?php if($_SESSION[leixing]=='教师'){?>
				 <td class="l-i-analysts"><span><a href="admin_hm2/addhandan.php?hid=<?=$handan[hid]?>">修改</a></span></td>
				<?php } ?>	
 </tr>
  <?php } ?>   
 </tbody>
 </table>
 <div id="pager1" class="pager">
 <?php
$num=$res->fn_num("select * from handanlist where status ='$_GET[status]'");
$ye=(int)($num/20+1);
?>
<?php if($page==1){?>
 <span class="current prev">上页</span>
<?php }else {?>
   
 <a href="tixing.php?status=<?=$_GET[status]?>&page=<?=($page-1)?>" class="prev">上页</a>
<?php }?>
<?php if($page==$ye){?>
<span class="current next">下页</span>
<?php }else{?>
  <a href="tixing.php?status=<?=$_GET[status]?>&page=<?=($page+1)?>" class="next">下页</a>
<?php } ?>
 </div>
 </div>
 </div>
    </div>
</body>
</html>
