<?php
include_once '../sys/conn.php';
include_once '../sys/mysql.class.php';
$now = getdate();
$month=$now[year].'-'.$now[mon];
$jiaoshis=$res->fn_rows("select t1.*,t2.count from zhuanjialist t1  left join (select jid, count(*) count from votelist where month='$month' group by jid) t2 on t1.jid=t2.jid where t1.fid='$_GET[fid]' order by t2.count desc");
$today=time()-$now[hours]*3600-$now[minutes]*60-$now[seconds];
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
	<meta name="keyword" content=""/>
	<title><?php $now = getdate(); echo $now[mon];?>月份讲师榜单</title>
	<script type="text/javascript" src="../js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="js/malertbox.js"></script>
	<style>
		*{margin:0;padding:0;}
		html{height:100%; }
		body{background:#fff;font-family:microsoft yahei,Helvetica,arial,sans-serif;font-size:14px;color:#888;}
		.grap{width:930px;margin:0 auto;}
		.grap h1{text-align:center;color:#ff0;font-size:36px;background:url(images/tbg.gif) center no-repeat;}
		li i{font-style:normal;}
		.grap h1 i{font-size:14px;font-style:normal;font-weight:normal;margin-left:10px;color:#E8C2C6;}
		.vote{clear:both;padding:25px;}
		.vote li{overflow:hidden;zoom:1;padding-top:5px;border-bottom:1px solid #ddd;list-style-type:none;padding:20px 0;}
		.right a.zan{float:right;width:90px; background:url(images/vote.gif) no-repeat;font-size:0px;overflow:hidden;text-indent:-50px;m}
		.right a.zan:hover{background:url(images/voon.gif) no-repeat;width:90px;height:90px;}
		.right a.zan.voted{background:url(images/voted.gif) no-repeat;width:90px;height:90px;}
		.right a.cai{float:right;width:90px; background:url(images/vote2.gif) no-repeat;font-size:0px;overflow:hidden;text-indent:-50px;}
		.right a.cai:hover{background:url(images/voon2.gif) no-repeat;}
		.vote li p{float:left;}
		.vote .pic{width:80px; height:80px;border:1px solid #e1e1e1;margin:0 2px; float:left; }
		.pic .img{width:80px; height:80px;}
		.v_name{height:35px;line-height:35px;font-size:18px;color:#333;margin-left: 17px;}
		.v_name i{float:right;padding-right:10px;font-style:normal;color:#B20000;}
		.content {height:auto;color:#7B0000; font-size:14px; font-weight:400;margin-top:3px;margin-left:120px;}
		.percent_container{width:208px;height:5px;background:#eee;position:relative;margin-top:3px;border-radius: 5px;}
		.percent_line{position:absolute;left:0;top:0;height:5px;background:#FA7143;display:block;border-radius: 5px;}
		.w33{width:33%;}
		.w20{width:20%;}
		.w10{width:10%;}
		.left .v_text{margin-top:8px;height:27px;line-height:27px;}
		.left .v_text span{margin-right:25px;}
		.malertbox{border:#ccc 1px solid;}
		.left{width:73%;float:left;margin-left:5px;margin-top: 5px;}
		.right{width:25%;float:right;margin-right:5px;margin-top: 5px;}
	</style>
</head>
<script>
	var sum = <?php echo $res->fn_num("select * from votelist where month='$month' and type=1");?>;
	$(function(){
		calpercent();
	});
	function calpercent(){
		$('ul li').each(function(){
			var count = parseInt($(this).find('.count').text());
			var amount = parseInt($(this).find('.amount').text());
			var percent= sum ? Math.round(amount/sum*10000)/100: 1;
			percent = percent+'%';
			$(this).find('.percent').html(percent);
			$(this).find('.percent_line').css('width',percent);
		});
	}
	function vote(t,type){
		mid="<?=$_SESSION[mid]?>";
		if(!mid){
			malertbox("请先登录，然后进行投票操作");
			return false;
		}
		$.post('vote_action.php',{'mid':mid,'jid':t,'type':type},function(data){
			if( data == 'success'){
				alert("投票成功");
				window.location.reload();
			}else if(data=='chaoshi'){
				if(type==1){
					alert("您已经投过票了，请等一个小时后再进行投票");
				}else{
					alert("您已经投过票了，请等3个小时后再进行投票");
				}
			}else if(data=='youke'){
				malertbox("请先登录，然后进行投票操作");
				return false;
			}
		});
	}
</script>
<body>
	<div class="grap">
		<h1><?php $now = getdate(); echo $now[mon];?>月份讲师榜单</h1>
		<a style="width:130px;margin:20px auto;display:block;font-size:16px;color:#f00;" href="vote_history.php?fid=<?=$_GET[fid]?>">《查看历史排名》</a>
		<div class="vote">
			<ul id='vote'>
				<?php foreach($jiaoshis as $jiaoshi): ?>
					<li id="t6">
						<div class="left" style="width:100%;">
							<div style="display:block;float:left;width:25%;"><img src="<?=$jiaoshi[pic]?>" style="width:200px;"></div>
							<div style="display:block;float:left;width:70%;">
								<span style="font-weight:bold;font-size:18px;"><?=$jiaoshi[jname]?></span>
								<hr>
								<br>
								<span><?=$jiaoshi['content']?></span>
							</div>
						</div>
						<div class="left" style="width:100%">
							<div style="margin-left:25%;color: red;font-size: 18px;">
								<span>今日获赞：
									<i class="count"><?php echo $res->fn_num("select * from votelist where jid='$jiaoshi[jid]' and time>='$today' and type=1");?></i>
								</span>&nbsp;
								<span>本月累计：
									<i class="amount"><?php echo $res->fn_num("select * from votelist where jid='$jiaoshi[jid]' and month='$month' and type=1");?>
									</i>
								</span>&nbsp;
								<span>今日获踩：
									<i class=""><?php echo $res->fn_num("select * from votelist where jid='$jiaoshi[jid]' and time>='$today' and type=0");?></i>
								</span>&nbsp;
								<span>本月累计：
									<i class=""><?php echo $res->fn_num("select * from votelist where jid='$jiaoshi[jid]' and month='$month' and type=0");?></i>
								</span>
							</div>
							<!-- 点赞和点菜 -->
							<div style="overflow:hidden;zoom:1;margin-left:25%;">
								<div style="float:left">
									<a href="javascript:vote('<?=$jiaoshi[jid]?>','1')" class="zan" style="margin-top:20px;display:inline-block;width:110px;height:35px;border:#ccc solid 1px;text-align:center;line-height:25px;background:#eee url('../images/zan.png') no-repeat  center"></a>
									<a href="javascript:vote('<?=$jiaoshi[jid]?>','0')" class="cai" style="margin-top:20px;display:inline-block;width:110px;height:35px;border:#ccc solid 1px;text-align:center;line-height:25px;background:#eee url('../images/cai.png') no-repeat center;"></a>
								</div>
								<!-- 进度条 -->
								<div style="float:left">
									<div class="percent_container"  style="margin-left:100px;position:relative;top:35px;">
										<span class="percent_line" ></span>
									</div>
									<p style="display:inline;margin-top:20px;margin-left:30px;"><i class="percent"></i></p>
								</div>
								
							</div>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</body>
</html>
