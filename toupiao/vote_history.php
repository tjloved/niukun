<?php 
include_once '../sys/conn.php';
include_once '../sys/mysql.class.php';
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
<meta name="keyword" content=""/>
<title>讲师历史排名</title>

<style>
*{margin:0;padding:0;}
body{background:#fff;font-family:microsoft yahei,Helvetica,arial,sans-serif;font-size:14px;color:#444;}
.grap{width:800px;margin:0 auto;/*height:600px;*/margin-bottom: 20px}
.grap h1{text-align:center;color:#ff0;font-size:36px;background:url(images/tbg.gif) center no-repeat;}
.grap h1 i{font-size:14px;font-style:normal;font-weight:normal;margin-left:10px;color:#E8C2C6;}
.vote{clear:both;padding:25px;border:2px solid #B20000;border-radius:15px;}

.v_text{margin-top:8px;height:27px;line-height:27px;}
.v_text span{margin-right:25px;}
table{border-collapse:collapse;width:100%;margin:auto;}
.bangt{font-size:16px;padding-left:20px;}
td{height:30px;border:1px #B20000 solid;padding:5px 10px;}
td i{display:inline-block;width:20px;height:20px;border:#fff 1px solid;border-radius:10px;text-align:center; font-style:normal;background-color:#aaa;color:#fff;}
td.p1 i{background-color:#E04F10;}
td.p2 i{background-color:#CAD31D;}
td.p3 i{background-color:#80C42D;}
</style>

</head>
<body>
<div class="grap">
<h1>讲师历史排名</h1><a style="position:absolute;left:20px;top:20px;text-decoration:none;font-size:16px;color:#f00;" href="index.php?fid=<?=$_GET[fid]?>"><<返回投票</a>
<table  class="vote"> 
<tr><th width="45%"></th><th></th></tr>
<?php
$now = getdate();
$m=$now[mon]-1;
if($m==0){
	$m=12;
	$y=$now[year]-1;
}else{
	$y=$now[year];
}
$month=$y.'-'.$m;


$jiaoshis=$res->fn_rows("select t1.*,t2.count from zhuanjialist t1  left join (select jid, count(*) count from votelist where month='$month' and type=1 group by jid) t2 on t1.jid=t2.jid where t1.fid='$_GET[fid]' order by t2.count desc");
$sum=$res->fn_num("select *  from votelist t1 left join zhuanjialist t2 on t1.jid =t2.jid where t2.fid='$_GET[fid]'  and t1.month='$month' and type=1");

foreach($jiaoshis as $key =>$jiaoshi){
	
?>
<?php if($key=='0'){?>
	<tr>
	<td rowspan="<?=count($jiaoshis)?>" class="bangt"><?=$y?>年<?=$m?>月份讲师榜单</td>
	<td  class="p1"><i>1</i><?=$jiaoshi[jname]?> （共<?=$jiaoshi[count]?>票，<?=round($jiaoshi[count]/$sum*100)?> %）</td>
	</tr>
<?php } else{?>

<tr>
<td  class="p<?=$key+1?>"><i><?=$key+1?></i> <?=$jiaoshi[jname]?> （共<?=$jiaoshi[count]?>票，<?=round($jiaoshi[count]/$sum*100)?> %）</td>
</tr>
<?php } } ?>
</table>

</div>
</body>
</html>