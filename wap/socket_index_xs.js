function connect() {
	ws = new WebSocket(WEB_CONFIG.URL);
	ws.onopen = function () {
		var loginObj;
		loginObj = {
			'type': 'login',
			'umz': USERNAME,
			'uid': MID,
			'rid': FID,
			'aid': ADMINID
		}
		ws.send(JSON.stringify(loginObj));
	};
	ws.onmessage = function(e){
		var messageData;
		messageData = JSON.parse(e.data);
		switch(messageData.type){
			case 'login':
				var loginStr, clientCount, selfStr;
				loginStr = '';
				if(messageData.clients || typeof messageData.clients != 'undefined'){
					$.each(messageData.clients,function (index, value) {
						loginStr += '<li onclick="UBase_Click(this)" adminid="'+value.aid+'" uname="'+value.umz+'" uid="'+value.uid+'">';
							loginStr += '<a href="#">'+value.umz+'<span class="u_l"></span></a>';
							loginStr += '<span><img src="images/level/User'+value.aid+'.png"></span>';
							loginStr += '<p>';
								loginStr += '<a onclick="sayTo(\''+value.uid+'\',\''+value.umz+'\')" class="talkto" href="javascript:void(0)">对TA说</a>';
								if(1 == ADMINID || 2 == ADMINID){
								loginStr += '<a onclick="pingbi(\''+value.uid+'\')" class="pingbi" href="javascript:void(0)">屏蔽</a>';
								loginStr += '<a onclick="ipban(\''+value.uid+'\')" class="ipban" href="javascript:void(0)">封IP</a>';
								}
							loginStr += '</p>';
						loginStr += '</li>';
					});
					$('#userList').append(loginStr);
				}
				
				clientCount = parseInt(messageData.count);
				selfStr = '';
				selfStr += '<li onclick="UBase_Click(this)" adminid="'+messageData.aid+'" uname="'+messageData.umz+'" uid="'+messageData.uid+'" class="on">';
					selfStr += '<a href="#">'+messageData.umz+'<span class="u_l"></span></a>';
					selfStr += '<span><img src="images/level/User'+messageData.aid+'.png"></span>';
					selfStr += '<p>';
						selfStr += '<a onclick="sayTo(\''+messageData.uid+'\',\''+messageData.umz+'\')" class="talkto" href="javascript:void(0)">对TA说</a>';
						if(1 == ADMINID || 2 == ADMINID){
						selfStr += '<a onclick="pingbi(\''+messageData.uid+'\')" class="pingbi" href="javascript:void(0)">屏蔽</a>';
						selfStr += '<a onclick="ipban(\''+messageData.uid+'\')" class="ipban" href="javascript:void(0)">封IP</a>';
						$('#manageCount').text(clientCount);
						}else{
						var renshu, systList;
						renshu = parseInt($('#allCount').parents('.renshu').attr('rel'));
						systList = parseInt($('#systList li').length);
						$('#allCount').text(renshu+systList+clientCount);
						}
					selfStr += '</p>';
				selfStr += '</li>';
				
				$.each($('#userList li'), function (index, value) {
					if($(value).attr('uid') == MID){
						$(this).remove();
					}
				});
				
				$('#userList').append(selfStr);
			break;
			case 'logout':
				$('#userList li[uid='+messageData.uid+']').remove();
				$('#allCount').text(parseInt($('#allCount').text()) - 1);
			break;

			case 'say'://发言开始
				
				if(messageData.lid){
					$('#liaotianlist').find('.liaotian[id='+messageData.lid+']').remove();
				}
				
				if(messageData.tid == MID){
					play('mp3/msg.mp3');
				}
				var contentStrXs = iconReplace(messageData.content);

				if(ADMINID == 1 || ADMINID == 2){
					contentStrXs = '<span style="color:#f00">'+ contentStrXs +'</span>';
					sendContent = '<img src="'+messageData.picurl+'" alt="img" style="max-width:500px; max-height:600px" />';
				}

				var talkToStr, contentStr;
				talkToStr = '';
				if('all' != messageData.tid){
					talkToStr = ' <a href="#">对 '+messageData.tmz+' 说</a> ';
				}
				contentStr = '';
				contentStr += '<div class="liaotian" id="'+messageData.lid+'">';
				contentStr += '<div class="liaotian_left fl"><img src="images/level/User'+messageData.aid+'.png"></div>';
				contentStr += '<div class="liaotian_right fl">';
				contentStr += '<span class="userbase"><a href="#">'+messageData.umz+'</a>'+talkToStr+'<a href="#">['+messageData.time+']</a></span>';
				contentStr += '<p>'+contentStrXs+'</p></div></div>';
				$('#liaotianlist').append(contentStr);
				if(messageData.zmd){
					$('.quanping').html('<marquee loop="1" onmouseover="marquee_show()"><p>'+iconReplace(messageData.content)+'</p></marquee>');
					$('.quanping').show();
					$('.quanping marquee').trigger('start');
				}
				if($('.bar_gundong').attr('checked')){
				   $('#liaotianlist').animate( { scrollTop: $('#liaotianlist')[0].scrollHeight } ,1000 );
				}
			break;
			case 'handan':
				play('mp3/5103.mp3');
				var handanStr;
				handanStr = '';
				handanStr += '<div class="liaotian"><div class="liaotian_right fl m2"><span class="userbase">';
				handanStr += '<a uname="交易提示" href="javascript:void(0)">交易提示</a>';
				handanStr += '<a href="javascript:void(0)">['+messageData.time+']</a></span>';
				handanStr += '<p onclick="$(\'#hdtx\').click()">'+messageData.did+' 价位：'+messageData.kcjia+' 详情点击查看</p>';
				handanStr += '</div></div>';
				$('#liaotianlist').append(handanStr);
			break;
			case 'pingcang':
				play('mp3/5103.mp3');
				var pingcangStr;
				pingcangStr = '';
				pingcangStr += '<div class="liaotian "><div class="liaotian_right fl m2"><span class="userbase">';
					pingcangStr += '<a uname="交易提示" href="javascript:void(0)">交易提示</a>';
					pingcangStr += '<a href="javascript:void(0)">['+messageData.time+']</a></span>';
					pingcangStr += '<p onclick="$(\'#hdtx\').click()">单号：'+messageData.did+' 已平仓</p>';
				pingcangStr += '</div></div>';
				$('#liaotianlist').append(pingcangStr);
			break;

			case 'chexiao':
				console.log(messageData);
				$('#liaotianlist .liaotian[id='+messageData.lid+']').remove();
			break;
		}
	};
	ws.onclose = function() {
		console.log("连接关闭，定时重连");
		connect();
	};
	ws.onerror = function() {
		console.log("出现错误");
	};
}

function sayTo(uid, umz){	
	$('#send_talkto').html('<option value="all">对所有人说</option><option value="'+uid+'" selected="selected">'+umz+'</option>');
}


function fabuall(obj) {
	var id, $this, username, tomid, tousername, adminid, content;
	$this = $(obj);
	id = $this.parents('.liaotian').attr('id');
	username = $this.parents('.liaotian').attr('umz');
	tomid = $this.parents('.liaotian').attr('tomid');
	tomid = tomid != 0 && tomid ?tomid:'all';
	tousername = $this.parents('.liaotian').attr('tmz');
	adminid = $this.parents('.liaotian').attr('aid');
	content = $this.parents('.liaotian').find('.liaotiancontent').html();
	

	if(id){
		$.post('action.php?type=fabuall', {lid:id}, function (data) {
			if(data == 'true'){
				$this.parent('.xs_action').html('<i>已审核</i><a href="javascript:;">删除</a>');

				var sayObj;
					sayObj = {
					'type': 'shenhe',
					'uid': MID,
					'lid': id,
					'umz': username,
					'tid': tomid,
					'tmz': tousername,
					'aid': adminid,
					'zmd': 0,
					'rid': FID,
					'content': content
				}
				ws.send(JSON.stringify(sayObj));
			}
		});
	}
}

function delall(obj){
	var id, $this;
	$this = $(obj);
	id = $this.parents('.liaotian').attr('id');
	$.post('action.php?type=delall', {lid:id}, function (data) {

		if(data == 'true'){
			$this.parents('.liaotian').remove();

			var sayObj;
				sayObj = {
					'type': 'chexiao',
					'lid': id,
					'rid': FID
				}
				ws.send(JSON.stringify(sayObj));
			alert('删除成功');
		}
	
	});

}