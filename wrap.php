<div class="wrap" style="display:none">
<!--弹出窗口-->
<div class="tanchuchuang">
	<div class="close"><a href="#"><img src="images/guanbi.jpg" /></a></div>
	<div class="wenzi">投票功能暂只对会员开放，请先注册会员或登入会员账号，为喜欢的老师投上一票</div>
	<div class="anniu">
    	<div class="zhuceanniu fl"><a href="#"><img src="images/zhuce.png" /></a></div>
        <div class="dengluanniu fl"><a href="#"><img src="images/denglu.png" /></a></div>
    </div>
</div>

<!--登陆弹出框-->
<div id="login_form">
    <h2>会员登陆</h2> 
    <div class="ErrBox"></div>
    <div class="form_line">
        <label>账号：</label>
        <input type="text" name="account">
    </div>
    <div class="form_line">
        <label>密码：</label>
        <input type="password" name="password">
    </div>
    <div class="form_line">
        <label>验证：</label>
        <input type="text" name="vcode">
        <img src="sys/pic.php" onclick="this.src='sys/pic.php'+'?time='+Date.parse(new Date())"/>
    </div>
    <div>
        <button type="button" onclick="$.loginFunc()" class="login_btn">立即登陆</button>
    </div>
</div>

<!--手机登陆弹出框-->
<div id="loginf" style="height:auto;width:auto;">
    <div style="height:32px;width:100%;">
        <div class="ErrBox" style="width:100%;box-sizing:border-box;"></div>
    </div>
    <span style="display:block;" class="zhanghao">
        <input type="text" name="account" style="width:100%;box-sizing:border-box;" placeholder="请输入账号" />
    </span>
    <span style="display:block;" class="zhanghao">
        <input type="password" placeholder="请输入密 码" style="width:100%;box-sizing:border-box;" name="password"/>
    </span>
    <span style="display:block;position:relative;" class="zhanghao">
        <input type="text" placeholder="请输入验证码" style="width:100%;box-sizing:border-box;" name="validate"/>
        <img src="sys/pic.php" style="position:absolute;right:0;height: 100%;top:0;" onclick="this.src='sys/pic.php'+'?time='+Date.parse(new Date())"/>
    </span>
    <div class="form_submit" style="width:100%;">
        <img style="width:100%;display:block;margin-top:5px;" src="images/lijidenglu.png" />
    </div>
</div>



<!--注册弹出框-->
<div id="register_form" style="height:auto;width:auto;">
  <h1 class="login_title" style="color:#fff;font-size: 18px;padding-left:10px;background: url(images/dl_bj.png) no-repeat 100%;line-height:52px;width:430px;margin-bottom: 24px;">请您注册</h1>
  <div style="width:100%;">
    <div class="ErrBox" style="width:100%;box-sizing:border-box;margin-top:-20px;"></div>
  </div>
  <div style="border:#ccc solid 1px;border-radius: 5px;width:298px;margin:auto;margin-bottom: 20px;" class="zhanghao">
    <span style="float:left;width:35px;text-align: center;"><img src="images/zhanghu.png" alt=""></span>
    <input type="text" style="width:250px;padding-left:10px;box-sizing:border-box;" placeholder="账 号:6-15位字母或数字组合" name="account"/>
  </div>
  <div style="border:#ccc solid 1px;border-radius: 5px;width:298px;margin:auto;margin-bottom: 20px;" class="zhanghao">
    <span style="float:left"><img src="images/pass.png" alt=""></span>
    <input type="password" style="width:250px;padding-left:10px;box-sizing:border-box;" placeholder="密 码：6-15位" name="password"/>
  </div>
  <div style="border:#ccc solid 1px;border-radius: 5px;width:298px;margin:auto;margin-bottom: 20px;" class="zhanghao">
    <span style="float:left;width:35px;text-align: center;"><img src="images/pass.png" alt=""></span>
    <input type="password" style="width:250px;padding-left:10px;box-sizing:border-box;" placeholder="请输入确认密码" name="rpassword"/>
  </div>

  <div style="border:#ccc solid 1px;border-radius: 5px;width:298px;margin:auto;margin-bottom: 20px;" class="zhanghao">
    <span style="float:left;width:35px;text-align: center;"><img src="images/dl_phone.png" alt=""></span>
    <input type="text" placeholder="请输入手机号" style="width:250px;padding-left:10px;box-sizing:border-box;" name="telephone"/>
  </div>

  <div style="border:#ccc solid 1px;border-radius: 5px;width:298px;margin:auto;margin-bottom: 20px;" class="zhanghao">
    <span style="float:left;width:35px;text-align: center;"><img src="images/dl_qq.png" alt=""></span>
    <input type="text" placeholder="请输入qq号" style="width:250px;padding-left:10px;box-sizing:border-box;" name="qq"/>
  </div>
  <div style="border:#ccc solid 1px;border-radius: 5px;width:298px;margin:auto;margin-bottom: 20px;position:relative;" class="zhanghao">
    <span style="float:left;width:35px;text-align: center;"><img src="images/yanzheng.png" alt=""></span>
    <input type="text" placeholder="请输入验证码" style="width:250px;padding-left:10px;box-sizing:border-box;" name="valiphone"/>
    <input type="button" style="position:absolute;right:0;height: 100%;top:0;padding:0;width:120px;background:#EF572E;border-radius:3px;border:none;color:#fff" id="sendmsg_btn" value="获取验证码" onclick="sendmsg()"/>
  </div>
  <script src="/js/register.js" type="text/javascript" ></script>
  <div class="form_submit" style="width:288px;margin:auto;">
    <img style="display:block;margin-top:5px;" src="images/lijizhuce.png" />
  </div>
  <p style="text-align:center;height:20px;width:100%;" >已有账号？直接<a href="#login_form" id="going" style="color:red;" >登录</a></p>
</div>



<!--名师榜-->
<div id="mingshi">
<iframe src="toupiao/index.php?fid=<?=$_GET[fid]?>" style="width:100%;height:100%; border:0px; overflow-y:auto" ></iframe>
</div>

<!--产品介绍弹窗-->
<div id="product">
<div class="tupian1">
	<?=$zoushang2[content]?>
</div>
</div>

<!--专家团队-->
<div id="zhuanjia">
<div class="laoshizong">
   <?php
$q_zhuanjia=$res->fn_sql("select * from zhuanjialist t1 left join  fangjianlist t2 on t1.fid=t2.fid where  t1.fid='$_GET[fid]' order by t1.jid desc");
while($r =mysql_fetch_array($q_zhuanjia)){
?>
	<div class="laoshi1 ">
	<table style="width:98%;color:#666666;font-size:14px;line-height:25px;">
    	<tr>
        	<td><img src="<?=$r[pic]?>" width="150px"  style="margin:10px 20px;"></td>
            <td>
			<div><?=$r[jname]?></div>
            <div>
			<?=$r[content]?>
            </div>
            </td>
        </tr>
    </table>
   </div>
 <?php } ?>
   </div>
</div>


<!--课程表-->
<div id="kechengbiao">
	<div class="kecheng">
    	<div class="biaoti"><img src="images/kecheng.png" /></div>
        <div class="biaoge">
        	<span><img src="images/829.png" /></span>
            <span class="kecheng1">
            <table style="width:790px;text-align:center;margin-left:20px;border:0px solid black;border-spacing:0px;height:35px;line-height:35px;">
                <tr style="font-size:18px;color:#fff;background:#23abff;  ">
                	<td>直播时间</td>
                    <td>星期一</td>
                    <td>星期二</td>
                    <td>星期三</td>
                    <td>星期四</td>
                    <td>星期五</td>
                </tr>
<?php 
$jiaoshis=$res->fn_rows("select * from jiaoshilist");
$kechengs=$res->fn_rows("select * from kechenglist where fid='$_SESSION[fid]' order by orderid asc");
foreach($kechengs as $kecheng ){
?>
                <tr style="font-size:18px;color:#fff; color:#4884b3; " class="ke">
                	<td><?=$kecheng[starttime]?>~<?=$kecheng[endtime]?></td>
                    <td><?=$kecheng[zhouyi]?></td>
                 <td><?=$kecheng[zhouer]?></td>
<td><?=$kecheng[zhousan]?></td>
<td><?=$kecheng[zhousi]?></td>
<td><?=$kecheng[zhouwu]?></td>
                </tr>
         <?php } ?>
            </table>
            </span>
        </div>
    </div>
</div>

<!--公司简介-->
<div id="gongsi">
	<div class="gongsimain">
    	<?=$zoushang3[content]?>
    </div>
</div>



<!--操作建议-->
<div id="caozuojianyi">
	<div class="content">
    	<?=$zoushang1[content]?>
    </div>
</div>
<!--决赛评审-->
 <?php $zuoshang5=$res->fn_select("select * from newslist where position='左上5' and fid='$_GET[fid]'"); ?>
<div id="jsps1">
	<div class="gongsimain">
    	<?php echo $zuoshang5['content']; ?>
    </div>
</div>
<!--比赛流程-->
 <?php  $zuoshang4=$res->fn_select("select * from newslist where position='左上4' and fid='$_GET[fid]'"); ?>
<div id="bslcbs">
	<div class="content">
    	<?php echo $zuoshang4['content']; ?>
    </div>
</div>
<!--奖项设置-->
<?php  $zuoshang3=$res->fn_select("select * from newslist where position='左上3' and fid='$_GET[fid]'"); ?>
<div id="jxszcon">
	<div class="content2">
    	<?php echo $zuoshang3['content']; ?>
    </div>
</div>

<!--游客说话间隔提示-->
<a href="#tishi" id="ts" ></a>
<div id="tishi" >
<h2>提示信息</h2>
<div class="tishi_content">
<p>游客发言时间间隔为10秒，还有<t>--</t>秒！</p>
<p>请联系QQ客服，免费领取马甲和老师解答，发言不受限制！</p>
 <ul>
<?php 
  $qq_rs=$res->fn_sql("select * from newslist where typename='客服QQ' and fid=$_GET[fid] limit 6 ");
    while($qqlinks=mysql_fetch_array($qq_rs)){
  ?>
    <li><a href="http://wpa.qq.com/msgrd?v=3&uin=<?=$qqlinks[description]?>&site=qq&menu=yes" target="_blank" ><img src="images/QQ.jpg" title="请加QQ：<?=$qqlinks[description]?>"/></a></li>
<?php } ?>
  </ul>
</div>
</div>

<div id="theme">
    <img data-bg="./images/bg/w1.jpg" src="./images/bg/small/w1x.jpg">
    <img data-bg="./images/bg/w2.jpg" src="./images/bg/small/w2x.jpg">
    <img data-bg="./images/bg/w3.jpg" src="./images/bg/small/w3x.jpg">
    <img data-bg="./images/bg/w4.jpg" src="./images/bg/small/w4x.jpg">
    <img data-bg="./images/bg/w5.jpg" src="./images/bg/small/w5x.jpg">
    <img data-bg="./images/bg/w6.jpg" src="./images/bg/small/w6x.jpg">
    <img data-bg="./images/bg/w7.jpg" src="./images/bg/small/w7x.jpg">
    <img data-bg="./images/bg/w8.jpg" src="./images/bg/small/w8x.jpg">
    <img data-bg="./images/bg/w9.jpg" src="./images/bg/small/w9x.jpg">
    <img data-bg="./images/bg/w10.jpg" src="./images/bg/small/w10x.jpg">
    <img data-bg="./images/bg/w11.jpg" src="./images/bg/small/w11x.jpg">
    <img data-bg="./images/bg/w12.jpg" src="./images/bg/small/w12x.jpg">
    <img data-bg="./images/bg/w13.jpg" src="./images/bg/small/w13x.jpg">
    <img data-bg="./images/bg/w14.jpg" src="./images/bg/small/w14x.jpg">
    <img data-bg="./images/bg/w15.jpg" src="./images/bg/small/w15x.jpg">
    <img data-bg="./images/bg/w16.jpg" src="./images/bg/small/w16x.jpg">
    <img data-bg="./images/bg/w17.jpg" src="./images/bg/small/w17x.jpg">
    <img data-bg="./images/bg/w18.jpg" src="./images/bg/small/w18x.jpg">
    <img data-bg="./images/bg/w19.jpg" src="./images/bg/small/w19x.jpg">
    <img data-bg="./images/bg/w20.jpg" src="./images/bg/small/w20x.jpg">
    <img data-bg="./images/bg/w21.jpg" src="./images/bg/small/w21x.jpg">
    <img data-bg="./images/bg/w22.jpg" src="./images/bg/small/w22x.jpg">
    <img data-bg="./images/bg/w23.jpg" src="./images/bg/small/w23x.jpg">
    <img data-bg="./images/bg/w24.jpg" src="./images/bg/small/w24x.jpg">
</div>
</div><!--/.wrap-->

<!--海报-->
<div class="haibao1" style="display:none"></div>
<div class="haibao" style="display:block">
	<div class="haibaotu"><img src="images/DefaultFace_<?=$_GET[fid]?>.png" id="welcome"/>
	<style>
        .haibao1 {background: transparent;position:absolute;top: 0;left: 0;text-align:center;width:100%;height:90%;min-height:600px;z-index: 99;}
	.haibao {width:800px;z-index: 99999;} 
	.haibao ul{list-style:none;position:relative;/*bottom:40px;left:20px;*/width:100%;}
	.haibao ul{width:99%;float:left; background:#000000; padding-left:1%; padding-top:5px;padding-bottom:5px; opacity:0.9;}
	.haibao ul li{float:left;margin-right:5px; margin-bottom:0px; margin-top:5px;}
	.haibao ul li span { color:#f00; float:left; line-height:22px; font-size:12px; margin-right:5px;}
</style>
	 <ul>
         <?php 
  $qq_rs=$res->fn_sql("select * from newslist where typename='客服QQ' and fid=$_GET[fid]");
    while($qqlinks=mysql_fetch_array($qq_rs)){
  ?>
        	<li><span><?=$qqlinks[title]?></span><a href="http://wpa.qq.com/msgrd?v=3&uin=<?=$qqlinks[description]?>&site=qq&menu=yes" target="_blank" ><img src="images/QQ.jpg" title="请加QQ：<?=$qqlinks[description]?>"/></a></li>
          
       <?php } ?>
        </ul>
		</div>
    <div class="chacha1"><a href="#"><img src="images/chacha2.png" onclick="haibaohide()"/></a></div>
</div>
<div class="quanping" >
<marquee loop='1' onmouseover="marquee_show()"><p></p></marquee>
</div>
<script>
function marquee_show(){
	if($('.quanping marquee p').offset().left==-$('.quanping marquee p').width()){
			$('.quanping').hide()
	}
}

</script>