﻿# Host: localhost  (Version: 5.5.40)
# Date: 2016-10-17 10:39:13
# Generator: MySQL-Front 5.3  (Build 4.120)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "admin_group"
#

DROP TABLE IF EXISTS `admin_group`;
CREATE TABLE `admin_group` (
  `adminid` int(10) NOT NULL,
  `adminname` varchar(200) DEFAULT NULL,
  `fayan` tinyint(1) DEFAULT '1',
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "admin_group"
#

/*!40000 ALTER TABLE `admin_group` DISABLE KEYS */;
INSERT INTO `admin_group` VALUES (1,'管理员',1,1),(2,'讲师',1,1),(3,'财主',1,1),(4,'至尊会员',1,1),(5,'钻石VIP',1,1),(6,'铂金会员',1,1),(7,'黄金会员',1,1),(8,'白银会员',1,1),(9,'【七段】寂灭',1,0),(10,'【八段】无边',1,0),(11,'【九段】羽化',1,0),(12,'会员',1,1),(14,'游客',1,1),(13,'注册用户',1,1),(16,'禁止用户',0,0);
/*!40000 ALTER TABLE `admin_group` ENABLE KEYS */;

#
# Structure for table "adminlist"
#

DROP TABLE IF EXISTS `adminlist`;
CREATE TABLE `adminlist` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `quanxian` varchar(1000) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

#
# Data for table "adminlist"
#

/*!40000 ALTER TABLE `adminlist` DISABLE KEYS */;
INSERT INTO `adminlist` VALUES (1,'admin','admin','超级管理员',0),(32,'fangjian','fangjian','房间管理员',16728),(33,'dailishang','dailishang','代理商',16728);
/*!40000 ALTER TABLE `adminlist` ENABLE KEYS */;

#
# Structure for table "configlist"
#

DROP TABLE IF EXISTS `configlist`;
CREATE TABLE `configlist` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `wzbt` varchar(500) DEFAULT NULL,
  `wzgjz` varchar(500) DEFAULT NULL,
  `wzms` varchar(500) DEFAULT NULL,
  `zxrs` int(11) DEFAULT '0',
  `yyh` text,
  `yyh2` text,
  `pvalue` varchar(20) DEFAULT NULL,
  `sfsh` tinyint(1) DEFAULT '0',
  `ltjlts` int(11) DEFAULT NULL,
  `fyjg` int(11) DEFAULT NULL,
  `nrzs` int(11) DEFAULT NULL,
  `ykdl` tinyint(1) DEFAULT '1',
  `zcsh` tinyint(1) DEFAULT '1',
  `dxyz` tinyint(1) DEFAULT '1',
  `zdsl` tinyint(1) DEFAULT '1',
  `dlts` tinyint(1) DEFAULT '1',
  `sjxz` int(11) DEFAULT NULL,
  `videoadd` varchar(255) DEFAULT NULL,
  `zcjg` int(11) DEFAULT NULL,
  `pbzd` varchar(500) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

#
# Data for table "configlist"
#

/*!40000 ALTER TABLE `configlist` DISABLE KEYS */;
INSERT INTO `configlist` VALUES (1,'牛昆财经直播室-喊单直播系统','牛昆财经直播室-喊单直播系统             ','牛昆财经直播室-喊单直播系统',50,'20458238','','234567',1,1,0,0,0,0,0,0,0,1,'http://www.iqiyi.com',0,'你妈',16728);
/*!40000 ALTER TABLE `configlist` ENABLE KEYS */;

#
# Structure for table "duanxin"
#

DROP TABLE IF EXISTS `duanxin`;
CREATE TABLE `duanxin` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(500) NOT NULL,
  `key` varchar(80) NOT NULL,
  `tpl_id` varchar(10) NOT NULL,
  PRIMARY KEY (`did`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "duanxin"
#

/*!40000 ALTER TABLE `duanxin` DISABLE KEYS */;
INSERT INTO `duanxin` VALUES (1,'http://v.juhe.cn/sms/send?','624375294ed96418bafb86f62374a7b6','11976');
/*!40000 ALTER TABLE `duanxin` ENABLE KEYS */;

#
# Structure for table "fangjianlist"
#

DROP TABLE IF EXISTS `fangjianlist`;
CREATE TABLE `fangjianlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fidvip` int(5) DEFAULT NULL COMMENT 'VIP房间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

#
# Data for table "fangjianlist"
#

/*!40000 ALTER TABLE `fangjianlist` DISABLE KEYS */;
INSERT INTO `fangjianlist` VALUES (19,16728,'最专业的金融信息服务平台','',NULL);
/*!40000 ALTER TABLE `fangjianlist` ENABLE KEYS */;

#
# Structure for table "fenleilist"
#

DROP TABLE IF EXISTS `fenleilist`;
CREATE TABLE `fenleilist` (
  `flid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`flid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Data for table "fenleilist"
#

/*!40000 ALTER TABLE `fenleilist` DISABLE KEYS */;
INSERT INTO `fenleilist` VALUES (9,'现货示例后台可修改');
/*!40000 ALTER TABLE `fenleilist` ENABLE KEYS */;

#
# Structure for table "fenxishilist"
#

DROP TABLE IF EXISTS `fenxishilist`;
CREATE TABLE `fenxishilist` (
  `fxid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`fxid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Data for table "fenxishilist"
#

/*!40000 ALTER TABLE `fenxishilist` DISABLE KEYS */;
INSERT INTO `fenxishilist` VALUES (6,'admin','admin',1);
/*!40000 ALTER TABLE `fenxishilist` ENABLE KEYS */;

#
# Structure for table "handanlist"
#

DROP TABLE IF EXISTS `handanlist`;
CREATE TABLE `handanlist` (
  `hid` int(11) NOT NULL AUTO_INCREMENT,
  `did` varchar(200) DEFAULT NULL,
  `kctime` varchar(200) DEFAULT NULL,
  `leixing` varchar(200) DEFAULT NULL,
  `cangwei` varchar(200) DEFAULT NULL,
  `cwleixing` varchar(600) DEFAULT NULL,
  `shangpin` varchar(200) DEFAULT NULL,
  `kcjia` varchar(200) DEFAULT NULL,
  `zsjia` varchar(200) DEFAULT NULL,
  `zyjia` varchar(200) DEFAULT NULL,
  `pctime` varchar(200) DEFAULT NULL,
  `pcjia` varchar(200) DEFAULT NULL,
  `hldian` varchar(200) DEFAULT NULL,
  `beizhu` varchar(600) DEFAULT NULL,
  `fenxishi` varchar(200) DEFAULT NULL,
  `zan` varchar(200) DEFAULT NULL,
  `pubdate` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`hid`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

#
# Data for table "handanlist"
#

/*!40000 ALTER TABLE `handanlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `handanlist` ENABLE KEYS */;

#
# Structure for table "ipbanlist"
#

DROP TABLE IF EXISTS `ipbanlist`;
CREATE TABLE `ipbanlist` (
  `iid` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`iid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "ipbanlist"
#

/*!40000 ALTER TABLE `ipbanlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipbanlist` ENABLE KEYS */;

#
# Structure for table "jiahua_jiange"
#

DROP TABLE IF EXISTS `jiahua_jiange`;
CREATE TABLE `jiahua_jiange` (
  `jgid` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`jgid`),
  UNIQUE KEY `jiange` (`jgid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "jiahua_jiange"
#

/*!40000 ALTER TABLE `jiahua_jiange` DISABLE KEYS */;
/*!40000 ALTER TABLE `jiahua_jiange` ENABLE KEYS */;

#
# Structure for table "jiahualist"
#

DROP TABLE IF EXISTS `jiahualist`;
CREATE TABLE `jiahualist` (
  `jhid` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `jhtype` varchar(200) DEFAULT NULL,
  `jiange` int(11) DEFAULT NULL,
  `fabu` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`jhid`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

#
# Data for table "jiahualist"
#

/*!40000 ALTER TABLE `jiahualist` DISABLE KEYS */;
INSERT INTO `jiahualist` VALUES (12,'想要开户的朋友。可以联系上方老师助理QQ、跟着我们老师做单。激活账户领取马甲。技术教材供你学习。详情联系上方QQ，竭诚为您服务。','C类',0,0),(13,'想加入我们中银财富中心直播室，想成为直播室会员的朋友，请直接联系直播室上方的QQ咨询，中银直播室竭诚为您服务！','A类',0,0),(15,'你看到的不是广告，是机会；我们给的不是建议，是利润 稳健才是盈利的王道。想开户的朋友，请直接添加上方客服好友咨询，中银在线直播室竭诚为您服务！','A类',0,0),(16,'/鲜花','A类',0,0),(17,'/顶一个','A类',0,0);
/*!40000 ALTER TABLE `jiahualist` ENABLE KEYS */;

#
# Structure for table "jiaoshilist"
#

DROP TABLE IF EXISTS `jiaoshilist`;
CREATE TABLE `jiaoshilist` (
  `jid` int(11) NOT NULL AUTO_INCREMENT,
  `jname` varchar(200) DEFAULT NULL,
  `qq` int(11) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`jid`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

#
# Data for table "jiaoshilist"
#

/*!40000 ALTER TABLE `jiaoshilist` DISABLE KEYS */;
INSERT INTO `jiaoshilist` VALUES (60,'孔明老师',0,87001),(62,'金言老师',0,87001),(63,'神胡老师',0,87001),(64,'谷风老师',0,87001),(65,'米道老师',0,87001),(66,'蜂王老师',0,87001),(67,'无极老师',0,87001);
/*!40000 ALTER TABLE `jiaoshilist` ENABLE KEYS */;

#
# Structure for table "kechenglist"
#

DROP TABLE IF EXISTS `kechenglist`;
CREATE TABLE `kechenglist` (
  `kid` int(11) NOT NULL AUTO_INCREMENT,
  `starttime` varchar(200) DEFAULT NULL,
  `endtime` varchar(200) DEFAULT NULL,
  `zhouyi` varchar(200) DEFAULT NULL,
  `zhouer` varchar(200) DEFAULT NULL,
  `zhousan` varchar(200) DEFAULT NULL,
  `zhousi` varchar(200) DEFAULT NULL,
  `zhouwu` varchar(200) DEFAULT NULL,
  `zhouliu` varchar(200) DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`kid`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

#
# Data for table "kechenglist"
#

/*!40000 ALTER TABLE `kechenglist` DISABLE KEYS */;
INSERT INTO `kechenglist` VALUES (47,'09:00','10:30','孔明老师','孔明老师','孔明老师','孔明老师','孔明老师','神胡老师',0,87001),(48,'10:30','12:00','金言老师','金言老师','金言老师','金言老师','金言老师','神胡老师',1,87001),(49,'12:00','13:30','神胡老师','神胡老师','神胡老师','神胡老师','神胡老师','神胡老师',2,87001),(50,'13:30','14:30','谷风老师','谷风老师','谷风老师','谷风老师','谷风老师','神胡老师',3,87001),(51,'14:30','16:00','孔明老师','孔明老师','孔明老师','孔明老师','孔明老师','神胡老师',4,87001),(52,'16:00','17:30','金言老师','金言老师','金言老师','金言老师','金言老师','神胡老师',5,87001);
/*!40000 ALTER TABLE `kechenglist` ENABLE KEYS */;

#
# Structure for table "kuaixunlist"
#

DROP TABLE IF EXISTS `kuaixunlist`;
CREATE TABLE `kuaixunlist` (
  `kid` int(11) NOT NULL AUTO_INCREMENT,
  `bianhao` varchar(200) DEFAULT NULL,
  `pinzhong` varchar(200) DEFAULT NULL,
  `fangxiang` varchar(200) DEFAULT NULL,
  `jiancangjia` varchar(200) DEFAULT NULL,
  `zhisunjia` varchar(200) DEFAULT NULL,
  `mubiaojia` varchar(200) DEFAULT NULL,
  `jiancangshijian` varchar(200) DEFAULT NULL,
  `fenxishi` varchar(200) DEFAULT NULL,
  `zhuangtai` varchar(200) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`kid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Data for table "kuaixunlist"
#

/*!40000 ALTER TABLE `kuaixunlist` DISABLE KEYS */;
INSERT INTO `kuaixunlist` VALUES (1,'kx20141022164359','1dd','多','123','','123','0','','平仓',0),(3,'kx20141022164359','1dd','','123','200','123','1413969156','11大四','平仓',0),(4,'kx20141103171605','品牌A','多','111111','111','111111111','17:14:15','小张','1234',0);
/*!40000 ALTER TABLE `kuaixunlist` ENABLE KEYS */;

#
# Structure for table "liaotianlist"
#

DROP TABLE IF EXISTS `liaotianlist`;
CREATE TABLE `liaotianlist` (
  `lid` int(10) NOT NULL AUTO_INCREMENT,
  `mid` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `tomid` int(10) NOT NULL,
  `tousername` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `time` int(10) NOT NULL,
  `shstatus` tinyint(1) NOT NULL DEFAULT '0',
  `shtime` int(10) NOT NULL,
  `siliao` tinyint(1) NOT NULL DEFAULT '0',
  `fid` int(11) NOT NULL,
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

#
# Data for table "liaotianlist"
#

/*!40000 ALTER TABLE `liaotianlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `liaotianlist` ENABLE KEYS */;

#
# Structure for table "minganlist"
#

DROP TABLE IF EXISTS `minganlist`;
CREATE TABLE `minganlist` (
  `lid` int(10) NOT NULL AUTO_INCREMENT,
  `mid` int(10) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `tomid` int(10) DEFAULT NULL,
  `tousername` varchar(200) DEFAULT NULL,
  `content` text,
  `time` int(10) DEFAULT NULL,
  `shstatus` tinyint(1) DEFAULT '0',
  `shtime` int(10) DEFAULT NULL,
  `siliao` tinyint(1) DEFAULT '0',
  `fid` int(11) DEFAULT NULL,
  `yidu` tinyint(1) DEFAULT '0',
  `xianshi` tinyint(1) DEFAULT '1',
  `llid` int(11) DEFAULT NULL,
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Data for table "minganlist"
#

/*!40000 ALTER TABLE `minganlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `minganlist` ENABLE KEYS */;

#
# Structure for table "newslist"
#

DROP TABLE IF EXISTS `newslist`;
CREATE TABLE `newslist` (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(66) DEFAULT NULL,
  `title2` varchar(66) DEFAULT NULL,
  `typename` varchar(55) DEFAULT NULL,
  `litpic` varchar(55) DEFAULT NULL,
  `content` text,
  `content2` text,
  `time` int(11) DEFAULT NULL,
  `tuijian` varchar(55) DEFAULT NULL,
  `chicun` varchar(555) DEFAULT NULL,
  `description` varchar(555) DEFAULT NULL,
  `description2` varchar(666) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `position` varchar(200) DEFAULT NULL,
  UNIQUE KEY `nid` (`nid`)
) ENGINE=MyISAM AUTO_INCREMENT=294 DEFAULT CHARSET=utf8;

#
# Data for table "newslist"
#

/*!40000 ALTER TABLE `newslist` DISABLE KEYS */;
INSERT INTO `newslist` VALUES (30,'版权声明 ','','0','','<p><span style=\"font-size: 14px;\">版权声明</span></p>','',1467251590,'操作建议','','','',16728,'右下3'),(31,'公告图片','','0','','<p>公告图片名称錅錅稀奇时数顶替</p>','',1467422909,'公告','','','',16728,'右下1'),(284,'在线客服',NULL,'客服QQ',NULL,'',NULL,1467520799,NULL,NULL,'139966056',NULL,16728,''),(285,'客服1对1',NULL,'客服QQ',NULL,'',NULL,1467520089,NULL,NULL,'909966056',NULL,16728,''),(286,'东北亚大宗商品交易中心软件',NULL,'文件共享',NULL,'<p style=\"text-align: center;\"><a href=\"http://xuanhuijinrong.com/Public/Home/images/soft1.png\" target=\"_self\"><img src=\"/ueditor/php/upload/image/20160702/1467449203762264.png\" title=\"1467449203762264.png\" alt=\"QQ图片20160702163342.png\" width=\"699\" height=\"251\" style=\"width: 699px; height: 251px;\"/></a></p>',NULL,1467480772,NULL,NULL,'',NULL,16728,''),(35,'立即开户','','0','','<p>专业的开户指导</p>','',1467536442,'','','http://wpa.qq.com/msgrd?v=3&uin=15896098989&site=qq&menu=yes','',16728,'左上2'),(287,'全球再次进入货币战争 贬值潮是否会再临',NULL,'市场评论',NULL,'<p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;background-color: rgb(204, 232, 207)\"><strong><span style=\"font-family: &#39;等线 Light&#39;;color: rgb(192, 80, 77);font-size: 21px\"><span style=\"font-family: SimHei;font-size: 16px;line-height: 1.5\">一、消息面</span></span></strong></p><p style=\"margin-top: 0px;margin-bottom: 20px;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;background-color: rgb(204, 232, 207)\"><span style=\"font-family: SimHei;color: rgb(0, 112, 192);line-height: 1.5\">1、全球再次进入货币战争 &nbsp;贬值潮是否会再临</span></p><p style=\"margin-top: 0px;margin-bottom: 20px;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;background-color: rgb(204, 232, 207)\"><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">国际现货黄金周五亚市盘初上涨突破</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">1330</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">美元</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">/</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">盎司关口，并刷新</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">6</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">月</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">27</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">日以来高点，因英国脱欧公投引发的不确定性可能促使全球货币政策进一步宽松。然而，更值得关注的是白银，周四大涨</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">2.8%</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">后周五更涨</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">3%</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">破</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">19</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">美元</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">/</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">盎司关口，沪贵银连续突破</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">4000</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">点和</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">4100</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">点高位，刷新</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">2014</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">年</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">9</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">月</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">10</span><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">日来高点。</span></p><p style=\"margin-top: 0px;margin-bottom: 20px;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;background-color: rgb(204, 232, 207)\"><span style=\"font-family: SimHei;color: rgb(0, 112, 192);line-height: 1.5\">2</span><span style=\"font-family: SimHei;color: rgb(0, 112, 192);line-height: 1.5\">、石油供需之争 美国沙特齐唱空头</span></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><span style=\"font-family: SimHei;color: rgb(73, 73, 73);line-height: 1.5\">美国能源部长与沙特能源部长法力赫周五达成共识，认为油市仍然供过于求。法力赫认为油市将在2016年底达到平衡，美国能源部长则认为油市将在2017年实现平衡。法力赫称沙特的石油政策主要跟随市场而不是份额转变，欧佩克将对短期市场需求做出回应，沙特将维持闲置产能不变。</span></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><br/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><br/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><strong><span style=\"font-family: SimHei;color: rgb(192, 80, 77);line-height: 1.5\">二、技术面</span></strong></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: center;background-color: rgb(204, 232, 207)\"><img width=\"604\" height=\"320\" src=\"/ueditor/php/upload/image/20160704/1467595230499297.png\" style=\"border: none\"/></p><p style=\"margin-top: 0px;margin-bottom: 20px;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: center;background-color: rgb(204, 232, 207)\"><span style=\"font-family: SimHei;line-height: 1.5\">中鑫油在</span><span style=\"font-family: SimHei;line-height: 1.5\">60</span><span style=\"font-family: SimHei;line-height: 1.5\">分钟线处于一个圆弧顶反转形态，而今日已走出较大级别反转，目前将在短暂反弹后继续下行，预计想测试前期</span><span style=\"font-family: SimHei;line-height: 1.5\">2353</span><span style=\"font-family: SimHei;line-height: 1.5\">低点。</span><img width=\"604\" height=\"329\" src=\"/ueditor/php/upload/image/20160704/1467595232303331.png\" style=\"border: none\"/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><span style=\"font-family: SimHei;line-height: 1.5\">沪贵银</span><span style=\"font-family: SimHei;line-height: 1.5\">60</span><span style=\"font-family: SimHei;line-height: 1.5\">分钟线的多头非常强势，上行趋势明显，现不建议轻易入场。白银上方短期内没有前期高点的压力位，从点位压力层面难以判断高点位置，以布林轨道技术显示将有短期修复，但可能是回调或者盘整，不建议继续做多。</span></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: center;background-color: rgb(204, 232, 207)\"><img width=\"604\" height=\"329\" src=\"/ueditor/php/upload/image/20160704/1467595233833740.png\" style=\"border: none\"/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><span style=\"font-family: SimHei;line-height: 1.5\">沪贵铜</span><span style=\"font-family: SimHei;line-height: 1.5\">60</span><span style=\"font-family: SimHei;line-height: 1.5\">分钟线的顶部呈现双重顶（</span><span style=\"font-family: SimHei;line-height: 1.5\">M</span><span style=\"font-family: SimHei;line-height: 1.5\">头），并且多次突破前期高位，现需要一个大级别的回调来重聚市场动力。建议空单入场。</span></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><br/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><strong><span style=\"font-family: SimHei;color: rgb(192, 80, 77);line-height: 1.5\">三、我们的观点</span></strong></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><strong><span style=\"font-family: SimHei;color: rgb(192, 80, 77);line-height: 1.5\">国际市场由于货币贬值压力而导致全球大宗商品承压，今日应特别关注白银的回调以及铜的下跌趋势。</span></strong></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><strong><span style=\"font-family: SimHei;color: rgb(192, 80, 77);line-height: 1.5\">四、今日关注</span></strong></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: center;background-color: rgb(204, 232, 207)\"><img width=\"604\" height=\"383\" src=\"/ueditor/php/upload/image/20160704/1467595235113763.png\" style=\"border: none\"/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: center;background-color: rgb(204, 232, 207)\"><img width=\"604\" height=\"289\" src=\"/ueditor/php/upload/image/20160704/1467595237127559.png\" style=\"border: none\"/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: center;background-color: rgb(204, 232, 207)\"><img width=\"604\" height=\"386\" src=\"/ueditor/php/upload/image/20160704/1467595239559111.png\" style=\"border: none\"/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><br/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><br/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><br/></p><p style=\"margin-top: 0px;margin-bottom: 20px;margin-left: 0;padding: 0px;color: rgb(102, 102, 102);line-height: 25px;text-indent: 2em;font-family: 微软雅黑;white-space: normal;text-align: justify;background-color: rgb(204, 232, 207)\"><strong><span style=\"font-family: SimHei;color: rgb(192, 80, 77);line-height: 1.5\">市场有风险，投资需谨慎，本文观点仅做参考！</span></strong></p><p><br/></p>',NULL,1467595250,NULL,NULL,'',NULL,16728,''),(56,'滚动公告','','0','','<p>牛昆财经直播室-最专业的金融信息服务平台-牛昆财经直播室财经直播室-最专业的金融信息服务平台</p>','',1467515651,'','','','',16728,'中上1'),(277,'官方网站',NULL,'0',NULL,'',NULL,1467423013,NULL,NULL,'http://www.niukun.net',NULL,16728,'左上1'),(279,'在线客服错单解套',NULL,'客服QQ',NULL,'',NULL,1467520135,NULL,NULL,'139966056',NULL,16728,''),(60,'产品介绍','','0','','<p><img src=\"/ueditor/php/upload/image/20150330/1427691797129004.jpg\" title=\"1427691797129004.jpg\" alt=\"产品介绍.jpg\"/></p>','',1467422980,'','','','',16728,'右下4'),(61,'公司简介','','0','','<p>南京牛昆财经直播室-喊单直播系统是以PHP、MySql、Socket、H264技术研发而成的多功能、极致专业的喊单直播系统软件！适合白银直播室、黄金直播室、原油喊单直播室、股票喊单直播室、邮币卡喊单直播室、农产品喊单直播室使用！软件价格22800元！购买电话：15896098989.</p>','',1467250909,'','','南京牛昆网络','',16728,'右下5'),(140,'操作建议','','0','','','',1467250767,'','','','',16728,'右下2'),(142,'客服热线','','0','','<p><span style=\"color: rgb(255, 255, 255); font-family: 微软雅黑; font-size: 12px; background-color: rgb(0, 0, 0);\">15896098989</span></p>','',1467422998,'','','','',16728,'右上2'),(280,'新华上海行交易中心软件',NULL,'文件共享',NULL,'<p style=\"text-align: center;\"><a href=\"http://software.xuanhui168.com/analyze_dz.rar\" target=\"_self\"><img src=\"/ueditor/php/upload/image/20160702/1467448466760543.png\" width=\"700\" height=\"294\" style=\"width: 700px; height: 294px;\"/></a><a href=\"http://software.xuanhui168.com/analyze_boyi.rar\" target=\"_self\"><img src=\"/ueditor/php/upload/image/20160702/1467448511118526.png\" width=\"699\" height=\"264\" style=\"width: 699px; height: 264px;\"/></a></p><p style=\"text-align: center;\"><a href=\"http://software.xuanhui168.com/xihuaE_PC.rar\" target=\"_self\"><img src=\"/ueditor/php/upload/image/20160702/1467448564517285.png\" width=\"699\" height=\"243\" style=\"width: 699px; height: 243px;\"/></a></p><p style=\"text-align: center;\"><a href=\"http://software.xuanhui168.com/xihuaE_phone.rar\" target=\"_self\"><img src=\"/ueditor/php/upload/image/20160702/1467448590438838.png\" width=\"696\" height=\"250\" style=\"width: 696px; height: 250px;\"/></a></p>',NULL,1467480746,NULL,NULL,'',NULL,16728,''),(1,' 置顶公告',NULL,'0',NULL,'<p>这里是置顶公告的内容，在后台单页面管理--单页列表--置顶公告 标题内编辑修改</p>',NULL,1467449907,NULL,NULL,'',NULL,16728,'中上2'),(292,'回放',NULL,'精彩回放',NULL,NULL,NULL,1476610096,NULL,NULL,'http://player.video.qiyi.com/2713facccde926c344c3035f6c85f4aa/0/199/v_19rr9mml28.swf-albumId=550074700-tvId=550074700-isPurchase=0-cnId=6',NULL,16728,NULL),(293,'精彩精彩',NULL,'精彩回放',NULL,NULL,NULL,1476610108,NULL,NULL,'http://player.video.qiyi.com/2713facccde926c344c3035f6c85f4aa/0/199/v_19rr9mml28.swf-albumId=550074700-tvId=550074700-isPurchase=0-cnId=6',NULL,16728,NULL),(288,'你好',NULL,'精彩回放',NULL,'http://www.iqiyi.com',NULL,1476610068,NULL,NULL,'http://player.video.qiyi.com/2713facccde926c344c3035f6c85f4aa/0/199/v_19rr9mml28.swf-albumId=550074700-tvId=550074700-isPurchase=0-cnId=6',NULL,16728,''),(2,'第二条',NULL,'精彩回放',NULL,'',NULL,1476610077,NULL,NULL,'http://player.video.qiyi.com/2713facccde926c344c3035f6c85f4aa/0/199/v_19rr9mml28.swf-albumId=550074700-tvId=550074700-isPurchase=0-cnId=6',NULL,NULL,NULL),(289,'精彩回放三',NULL,'精彩回放',NULL,NULL,NULL,1476610077,NULL,NULL,'http://player.video.qiyi.com/2713facccde926c344c3035f6c85f4aa/0/199/v_19rr9mml28.swf-albumId=550074700-tvId=550074700-isPurchase=0-cnId=6',NULL,16728,NULL),(290,'精彩回放四',NULL,'精彩回放',NULL,NULL,NULL,1476610085,NULL,NULL,'http://player.video.qiyi.com/2713facccde926c344c3035f6c85f4aa/0/199/v_19rr9mml28.swf-albumId=550074700-tvId=550074700-isPurchase=0-cnId=6',NULL,16728,NULL),(291,'精彩回放五',NULL,'精彩回放',NULL,NULL,NULL,1476610061,NULL,NULL,'http://player.video.qiyi.com/2713facccde926c344c3035f6c85f4aa/0/199/v_19rr9mml28.swf-albumId=550074700-tvId=550074700-isPurchase=0-cnId=6',NULL,16728,NULL),(283,'在线客服转户开户',NULL,'客服QQ',NULL,'',NULL,1467520169,NULL,NULL,'139966056',NULL,16728,''),(186,'现货原油空单策略','','市场评论','','<p>【中银在线财富中心】原油经过大幅度的暴跌之后短线存在反弹需求，操作上可以考虑现价做多，止损347.6，目标354.78，其次是360.72.现价350.48.如有操作，请严格控制仓位（100万可下20手左右以此类推）。---以上观点仅供参考</p>','',1436247137,'','','','',16728,'');
/*!40000 ALTER TABLE `newslist` ENABLE KEYS */;

#
# Structure for table "paylist"
#

DROP TABLE IF EXISTS `paylist`;
CREATE TABLE `paylist` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='余额';

#
# Data for table "paylist"
#


#
# Structure for table "pinpailist"
#

DROP TABLE IF EXISTS `pinpailist`;
CREATE TABLE `pinpailist` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(200) DEFAULT NULL,
  `flid` int(11) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

#
# Data for table "pinpailist"
#

/*!40000 ALTER TABLE `pinpailist` DISABLE KEYS */;
INSERT INTO `pinpailist` VALUES (38,'沪贵银',9,NULL),(39,'沪贵铜',9,NULL),(40,'沪贵油',9,NULL);
/*!40000 ALTER TABLE `pinpailist` ENABLE KEYS */;

#
# Structure for table "shipanlist"
#

DROP TABLE IF EXISTS `shipanlist`;
CREATE TABLE `shipanlist` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `pname` varchar(200) DEFAULT NULL,
  `cszc` varchar(200) DEFAULT NULL,
  `zrzc` varchar(200) DEFAULT NULL,
  `zxzc` varchar(200) DEFAULT NULL,
  `drsy` varchar(200) DEFAULT NULL,
  `ljsy` varchar(200) DEFAULT NULL,
  `zgsy` varchar(200) DEFAULT NULL,
  `ccgp` varchar(200) DEFAULT NULL,
  `qq` int(11) DEFAULT NULL,
  `displayorder` int(11) DEFAULT NULL,
  `rsypm` int(11) DEFAULT NULL,
  `zrsy` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

#
# Data for table "shipanlist"
#

/*!40000 ALTER TABLE `shipanlist` DISABLE KEYS */;
INSERT INTO `shipanlist` VALUES (8,10,'枫','','671280','671280','3254','42350','','',139966056,3,32,'4352'),(9,1,'De.e','','2357956','2357956','8460','179800','','',909966056,4,5,'4050');
/*!40000 ALTER TABLE `shipanlist` ENABLE KEYS */;

#
# Structure for table "temp_liaotianlist"
#

DROP TABLE IF EXISTS `temp_liaotianlist`;
CREATE TABLE `temp_liaotianlist` (
  `lid` int(10) NOT NULL AUTO_INCREMENT,
  `mid` int(10) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  `shstatus` tinyint(1) DEFAULT '0',
  `shtime` int(10) DEFAULT NULL,
  `jiange` int(10) DEFAULT NULL,
  `longtime` int(10) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

#
# Data for table "temp_liaotianlist"
#

/*!40000 ALTER TABLE `temp_liaotianlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `temp_liaotianlist` ENABLE KEYS */;

#
# Structure for table "toupiao"
#

DROP TABLE IF EXISTS `toupiao`;
CREATE TABLE `toupiao` (
  `mid` int(10) NOT NULL,
  `opinion` int(10) DEFAULT NULL,
  `dateline` int(10) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "toupiao"
#

/*!40000 ALTER TABLE `toupiao` DISABLE KEYS */;
INSERT INTO `toupiao` VALUES (0,1,1410415875);
/*!40000 ALTER TABLE `toupiao` ENABLE KEYS */;

#
# Structure for table "toupiao_count"
#

DROP TABLE IF EXISTS `toupiao_count`;
CREATE TABLE `toupiao_count` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `tcount` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `op1` int(11) DEFAULT NULL,
  `op2` int(11) DEFAULT NULL,
  `op3` int(11) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Data for table "toupiao_count"
#

/*!40000 ALTER TABLE `toupiao_count` DISABLE KEYS */;
INSERT INTO `toupiao_count` VALUES (4,0,'恭喜发财',65,59,147,909966056),(5,0,'赞助奖品',12,14,2222,909966056),(6,NULL,'白银',2,2,2,909966056),(7,NULL,'白银趋势',200,101,150,16728);
/*!40000 ALTER TABLE `toupiao_count` ENABLE KEYS */;

#
# Structure for table "userlist"
#

DROP TABLE IF EXISTS `userlist`;
CREATE TABLE `userlist` (
  `mid` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `nickname` varchar(200) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `adminid` int(10) DEFAULT NULL,
  `fangxiang` varchar(500) DEFAULT NULL,
  `fenxifa` varchar(200) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT '0',
  `email` varchar(200) DEFAULT NULL,
  `telephone` varchar(200) DEFAULT NULL,
  `qq` varchar(200) DEFAULT NULL,
  `logintime` int(10) DEFAULT NULL,
  `login_count` int(11) DEFAULT '1',
  `dateline` int(10) DEFAULT NULL,
  `updatetime` int(10) DEFAULT '0',
  `regtime` int(11) NOT NULL DEFAULT '0',
  `votetime` int(11) DEFAULT NULL,
  `zantime` int(11) DEFAULT NULL,
  `caitime` int(11) DEFAULT NULL,
  `flowers` int(10) DEFAULT NULL,
  `tuijianmid` int(11) DEFAULT NULL,
  `jiaren` tinyint(1) DEFAULT '0' COMMENT '0:注册会员;1:离线假人;2:在线假人',
  `fabu` tinyint(1) DEFAULT '0',
  `fid` int(11) DEFAULT NULL,
  `ip` varchar(200) DEFAULT NULL,
  `beizhu` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

#
# Data for table "userlist"
#

/*!40000 ALTER TABLE `userlist` DISABLE KEYS */;
INSERT INTO `userlist` VALUES (6,'牛昆',NULL,'123456',1,NULL,NULL,0,NULL,NULL,NULL,NULL,596,1474429885,0,0,NULL,NULL,NULL,NULL,NULL,0,0,16728,'127.0.0.1',NULL);
/*!40000 ALTER TABLE `userlist` ENABLE KEYS */;

#
# Structure for table "vipkechenglist"
#

DROP TABLE IF EXISTS `vipkechenglist`;
CREATE TABLE `vipkechenglist` (
  `vkid` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(200) NOT NULL,
  `starttime` varchar(200) NOT NULL,
  `endtime` varchar(200) NOT NULL,
  `content` varchar(200) NOT NULL,
  `jiaoshi` varchar(200) NOT NULL,
  `feiyong` varchar(200) NOT NULL,
  `qq` varchar(200) NOT NULL,
  `orderid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  PRIMARY KEY (`vkid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "vipkechenglist"
#

/*!40000 ALTER TABLE `vipkechenglist` DISABLE KEYS */;
INSERT INTO `vipkechenglist` VALUES (3,'11.23','9:30','11:30','股市分析','王老师','100','139966056',0,1);
/*!40000 ALTER TABLE `vipkechenglist` ENABLE KEYS */;

#
# Structure for table "votelist"
#

DROP TABLE IF EXISTS `votelist`;
CREATE TABLE `votelist` (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `jid` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `month` varchar(200) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`vid`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

#
# Data for table "votelist"
#

/*!40000 ALTER TABLE `votelist` DISABLE KEYS */;
INSERT INTO `votelist` VALUES (1,2,18,1450018904,'2015-12',1),(2,2,20,1450018906,'2015-12',0),(3,7,1,1467220820,'2016-6',1),(4,46,5,1467423068,'2016-7',1),(5,46,4,1467423072,'2016-7',0),(6,76,7,1467439032,'2016-7',1),(7,46,5,1467467972,'2016-7',1),(8,46,5,1467468004,'2016-7',0),(9,76,5,1467468048,'2016-7',1),(10,76,5,1467468053,'2016-7',0),(11,46,4,1467471818,'2016-7',1),(12,238,5,1467515283,'2016-7',1),(13,238,4,1467515290,'2016-7',0),(14,245,5,1467516644,'2016-7',1),(15,245,4,1467516651,'2016-7',0),(16,24,5,1467532921,'2016-7',1),(17,24,5,1467532923,'2016-7',0),(18,4,9,1467558520,'2016-7',1),(19,4,6,1467558536,'2016-7',0),(20,94,6,1467597607,'2016-7',1),(21,94,9,1467597632,'2016-7',0);
/*!40000 ALTER TABLE `votelist` ENABLE KEYS */;

#
# Structure for table "wendalist"
#

DROP TABLE IF EXISTS `wendalist`;
CREATE TABLE `wendalist` (
  `wid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `question` text,
  `answer` text,
  `time` int(11) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`wid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

#
# Data for table "wendalist"
#

/*!40000 ALTER TABLE `wendalist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wendalist` ENABLE KEYS */;

#
# Structure for table "win_list"
#

DROP TABLE IF EXISTS `win_list`;
CREATE TABLE `win_list` (
  `wl_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '房间id',
  `wl_name1` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name2` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name3` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name4` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name5` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name6` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name7` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name8` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name9` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name10` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name11` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_name12` varchar(255) NOT NULL DEFAULT '' COMMENT '名称',
  `wl_gl1` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl2` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl3` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl4` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl5` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl6` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl7` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl8` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl9` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl10` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl11` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  `wl_gl12` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '概率',
  PRIMARY KEY (`wl_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=gbk COMMENT='奖品列表';

#
# Data for table "win_list"
#

/*!40000 ALTER TABLE `win_list` DISABLE KEYS */;
INSERT INTO `win_list` VALUES (1,16728,'恭喜您抽中话费50元','恭喜您抽中体验一周黄金马甲','恭喜您抽中话费20元','恭喜您抽中苹果6s手机一部','好遗憾，未中奖。','恭喜您抽中话费20元','恭喜您抽中马尔代夫两日游','恭喜您抽中科沃斯扫地机','恭喜您抽中话费50元','恭喜您抽中佳能单反相机一部','好遗憾，未中奖。','恭喜您抽中马尔代夫两日游',500,5000,1000,200,10000,1000,1,300,500,100,10000,1);
/*!40000 ALTER TABLE `win_list` ENABLE KEYS */;

#
# Structure for table "win_record"
#

DROP TABLE IF EXISTS `win_record`;
CREATE TABLE `win_record` (
  `wr_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wr_phone` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `wr_record` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '中奖记录',
  `wr_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '抽奖时间',
  `wr_ip` varchar(15) NOT NULL DEFAULT '' COMMENT '抽奖人ip',
  `fid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '房间ID',
  PRIMARY KEY (`wr_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=gbk COMMENT='获奖记录';

#
# Data for table "win_record"
#

/*!40000 ALTER TABLE `win_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `win_record` ENABLE KEYS */;

#
# Structure for table "zhuanjialist"
#

DROP TABLE IF EXISTS `zhuanjialist`;
CREATE TABLE `zhuanjialist` (
  `jid` int(11) NOT NULL AUTO_INCREMENT,
  `jname` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `content` text,
  `qq` varchar(200) DEFAULT NULL,
  `pic` varchar(200) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  PRIMARY KEY (`jid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

#
# Data for table "zhuanjialist"
#

/*!40000 ALTER TABLE `zhuanjialist` DISABLE KEYS */;
INSERT INTO `zhuanjialist` VALUES (4,'金言老师',NULL,'<p><br/></p><p style=\"line-height: 1.5em; margin-bottom: 10px; margin-top: 10px;\"><span style=\"font-size: 16px;\"><strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">简介：</span></strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">资深贵金属分析师，有着15年以上对现货市场的研究及实盘交易经验，曾担任某机构贵金属研究院院长，新浪，汇通，幸福黄金，中金，和讯等多家知名网站的路演特邀嘉宾，在业内享有较高的知名度，现为红杉财经资深分析师。见证了现货市场的沧海浮沉，对国际现货市场具有深刻的认识和独到的见解。</span></span></p><p style=\"line-height: 1.5em; margin-top: 10px; margin-bottom: 10px;\"><span style=\"font-size: 16px;\"><strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">操作风格：</span></strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">趋势是你永远的朋友。止损不是亏损，而是稳定盈利。善于把握市场每一个拐点，伺机而动，稳定盈利。</span></span></p>','2880204437','/up/1467360033.jpg',16728),(5,'米道老师',NULL,'<p style=\"line-height: 1.5em; margin-bottom: 5px;\"><span style=\"font-family: 黑体, SimHei; font-size: 16px;\">江恩理论、艾略特波浪理论的践行者</span></p><p style=\"line-height: 1.5em; margin-bottom: 5px;\"><span style=\"font-family: 黑体, SimHei; font-size: 16px;\"><strong>资历：</strong>20年左右实战操盘经验，熟悉股票、股指期货、商品期货、贵金属等投资，爱学习，刻苦钻研，有自己独特的交易系统。</span></p><p style=\"line-height: 1.5em; margin-bottom: 5px;\"><span style=\"font-family: 黑体, SimHei; font-size: 16px;\"> <strong>操作风格：</strong>趋势为王，果断建仓平仓，擅长博取波段和短线的暴利，长线中线短线相结合。</span></p><p style=\"line-height: 1.5em; margin-bottom: 5px;\"><span style=\"font-size: 16px;\"><strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">交易法宝：</span></strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">独创四步雪球复利战法。</span></span></p><p style=\"line-height: 1.5em; margin-bottom: 5px;\"><span style=\"font-family: 黑体, SimHei; font-size: 16px;\"> <strong>交易特色：</strong>安全保本永远是第一原则。严格止损和止盈，特别强调遵守纪律和控制仓位的风控管理。擅长研究数据，对数字非常敏感，在数据行情中能迅速反应。遵循趋势交易，只做大概率事件。</span></p><p style=\"line-height: 1.5em; margin-bottom: 5px;\"><span style=\"font-family: 黑体, SimHei; font-size: 16px;\"> <strong>经典战绩：</strong>平均年收益率在20%---100%以上。月收益率高的时候在100%-300%以上，成功率非常高，80--90%以上，轻仓滚雪球，操作风格非常稳健，特别有毅力，水滴石穿。<br/> <br/> </span></p>','2880204442','/up/1467360232.jpg',16728),(6,'孔明老师',NULL,'<p style=\"line-height: 1.5em; margin-bottom: 10px; margin-top: 10px;\"><span style=\"font-size: 16px;\"><strong><span style=\"font-family: 黑体, SimHei;\">简介：</span></strong><span style=\"font-family: 黑体, SimHei;\">金融硕士研究生，十年投资经验，曾任证券投资顾问、证券分析师等职务，现为国家高级黄金分析师。历经国内外市场洗礼，对于国际金融市场和宏观经济嗅觉敏锐，自创庖丁解盘战法，深谙时间窗理论,成功抓住白银,原油市场大底。擅长K线语言，均线理论，时间窗理论，有千万资金操盘经验，有成熟的风险控制理念，交易风格上趋势交易结合中短线操作，操盘准确率持续稳定在80%以上，投资风格：胆大心细，顺势而为。</span></span></p><p style=\"line-height: 1.5em; margin-bottom: 10px; margin-top: 10px;\"><strong style=\"line-height: 1.5em;\"><span style=\"font-family: 黑体, SimHei;\">交易理念：</span></strong><span style=\"line-height: 1.5em; font-family: 黑体, SimHei;\">投资心态第一，资金管理第二，操盘技术第三</span></p><p><br/></p>','2880204431','/up/1467360320.jpg',16728),(7,'蜂王老师',NULL,'<p style=\"margin-top:10px;margin-right:0;margin-bottom:10px;margin-left: 0;line-height:24px\"><strong><span style=\"font-family: 黑体\"></span></strong></p><p style=\"margin-top: 10px; margin-bottom: 10px; white-space: normal; line-height: 24px;\"><strong><span style=\"font-family: 黑体;\">个人介绍：</span></strong></p><p style=\"margin-top: 10px; margin-bottom: 10px; white-space: normal; line-height: 24px;\"><span style=\"font-family: 黑体;\">昵称蜂王老师。主修经济学，货币银行学，具有扎实的经济学理论功底，3年外汇、5年贵金属等领域的工作和研究，有丰富的实盘经验，上海文广电台特邀嘉宾。善于把握中短线的投资机会。针对不同的行情结合不同的技术指标分析，同时结合基本面分析，对于国际宏观经济金融领域有个人独到的见解， 熟悉各种交易策略流程及交易管理系统。在长期的实战过程中形成稳健的交易风格。个人目前为止在线蜂眼盯盘时段运用独创的蜂王针交易系统公开布局策略在2014年7，8月份实现30连赢的辉煌纪录，多年下来整体布局成功概率维持在80%左右。</span></p><p style=\"margin-top:10px;margin-right:0;margin-bottom:10px;margin-left: 0;line-height:24px\"><strong><span style=\"font-family: 黑体\">从业时间：</span></strong><span style=\"font-family: 黑体\">8</span><span style=\"font-family: 黑体\">年 &nbsp;</span></p><p style=\"margin-top:10px;margin-right:0;margin-bottom:10px;margin-left: 0;line-height:24px\"><strong><span style=\"font-family: 黑体\">交易格言：</span></strong><span style=\"font-family: 黑体\">宁可错过，不可做错，稳中求胜，严格止损。</span></p><p style=\"margin: 10px 0px; line-height: 24px;\"><br/></p>','2880204433','/up/1467360438.jpg',16728),(8,'谷风老师',NULL,'<p style=\"line-height: 1.5em; margin-bottom: 5px;\"><strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">简介:</span></strong></p><p style=\"line-height: 1.5em; margin-bottom: 5px;\"><span style=\"font-family: 黑体, SimHei; line-height: 1.5em;\">&nbsp; &nbsp;2010年开始从事外汇，期货，贵金属等一系列金融衍生品投资工作，侧重于期货交易及分析。2013年开始着重于贵金属领域，坚持两手都要抓，两手都要硬的分析理念，结合技术面和基本面共同分析，以独到的眼光研判大趋势，把握波段行情。痴迷于指标研究，以打造一套适用于各种行情的交易系统为目标，在多年的实战经验磨练出自己的交易系统，并成功研发了一系列适用于不同行情不同交易品种的指标。</span></p><p style=\"margin-bottom: 10px; line-height: 1.5em;\"><strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">投资格言：</span></strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">大道至简！</span></p><p><br/></p>','2880204416 ','/up/1467363449.jpg',16728),(9,'无极高老师',NULL,'<p><strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">简介：</span></strong></p><p><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">&nbsp; &nbsp;9年机构操盘经验，历职券商研究员，私募基金操盘手，系属投资大鳄索罗斯第三代传人。凭借独创的天魔系统，契机交易法，走势模型理论等研究体系，在业界内独立一派。</span></p><p><strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">交易理念：</span></strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">让交易更系统，让投资更简单。</span></p>','2880204403','/up/1467363615.jpg',16728),(10,'神胡老师',NULL,'<p><strong><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">简介：</span></strong></p><p><span style=\"font-size: 16px; font-family: 黑体, SimHei;\">&nbsp; &nbsp;从事金融行业二十年，对现货，期货以及证劵行业有资深的探求，擅长运用波浪理论及江恩法则来研究行情，对市场的把握经常出神入化，因此客户冠以神狐《胡》称名。《通过波浪理论出空间，通过时间周期找出市场转折点。基本面和政策面是来验证》。</span></p>','2880204444 ','/up/1467363659.jpg',16728);
/*!40000 ALTER TABLE `zhuanjialist` ENABLE KEYS */;
